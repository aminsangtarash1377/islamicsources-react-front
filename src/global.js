const {words, newWords} = require('./config');
const config = require('./config');

function sendWordsToBackForTranslate(word) {
    if(
        Object.keys(words).filter(val => val === word).length < 1 &&
        newWords.filter(val => val === word).length < 1 &&
        word !== ""
    ){
        config.newWords.push(word);
    }
    return word;
}

module.exports = {
    __ :function (word) {
        return words[0][word] ? words[0][word] : sendWordsToBackForTranslate(word);
    }
};