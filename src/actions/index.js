export const Authenticated = value => ({
    type:"AUTHENTICAT",
    value: value
});

export const UserDetailPush = value => ({
    type:"USERDETAILSPUSH",
    value: value
});

export const PrivateLoading = value => ({
    type:"STOPLOADING",
    value: value
});

export const Filter = text => ({
    type:"FILTER",
    text
});

export const Rtl = data => ({
    type:"RTL",
    default:false,
    data
});

export const Parent = data => ({
    type:"PARENT",
    default:0,
    data
});

export const SingleInBooksPageTitle = data => ({
    type:"SINGLEINBOOKSPAGETITLE",
    default:null,
    data
});

export const SingleInBooksPageSlug = data => ({
    type:"SINGLEINBOOKSPAGESLUG",
    default:null,
    data
});

export const SingleInArticlesPageSlug = data => ({
    type:"SINGLEINARTICLESPAGESLUG",
    default:null,
    data
});

export const SingleInArticlesPageTitle = data => ({
    type:"SINGLEINARTICLESPAGETITLE",
    default:null,
    data
});

export const LoadingForSidebar = data => ({
    type:"LOADINGFORSIDEBAR",
    default:null,
    data
});

export const SingleInJournalPageSlug = data => ({
    type:"SINGLEINJOURNALPAGESLUG",
    default:null,
    data
});

export const SingleInHomePageSlug = data => ({
    type:"SINGLEINHOMEPAGESLUG",
    default:null,
    data
});

export const SingleInHomePageTitle = data => ({
    type:"SINGLEINHOMEPAGETITLE",
    default:null,
    data
});

export const SingleInCategoriesPageTitle = data => ({
    type:"SINGLEINCATEGORIESPAGETITLE",
    default:null,
    data
});

export const SingleInCategoriesPageSlug = data => ({
    type:"SINGLEINCATEGORIESPAGESLUG",
    default:null,
    data
});

export const UrlImagePackage = data => ({
    type:"URLIMAGEPACKAGE",
    default:null,
    data
});

export const DataForSideBar = data => ({
    type:"DATAFORSIDEBAR",
    default:[],
    data
});

export const SingleInJournalPageTitle = data => ({
    type:"SINGLEINJOURNALPAGETITLE",
    default:null,
    data
});

export const CheckTypeSelected = d => ({
    type : "TYPE",
    checked : d.checked,
    name : d.name
});

export const CheckFormatSelected = format => ({
    type : "FORMAT",
    checked : format.checked,
    name : format.name
});

export const CheckCreatorSelected = Creator => ({
    type : "CREATOR",
    checked : Creator.checked,
    name : Creator.name
});

export const SearchFiltersMyLists = Lists => ({
    type : "LISTS",
    name : Lists.name,
    data: Lists.data,
    checked : Lists.checked
});







