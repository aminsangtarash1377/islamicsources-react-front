// export * from ""

export {default as Header} from './Modules/Header/Header';
export * from './Modules/CustomImage';
export {default as ResourceStatistics} from './Modules/ResourceStatistics/ResourceStatistics';
export {default as Search} from './Modules/Search/Search';
export {default as Login} from './Modules/Login/Login';
export * from './Modules/Search/Search';
export {default as SearchFilterMenu} from './Modules/SearchFilterMenu/SearchFilterMenu';
export {default as BookCollection} from './Modules/Home/BookCollection/BookCollection';
export {default as TabSection} from './Modules/TabSection/TabSection';
export {default as Book} from './Modules/PostTypes/Book/Book';
export {default as Article} from './Modules/PostTypes/Article/Article';
export {default as Pictures} from './Modules/Pictures/Pictures';
export {default as AuthorOfTheWeek} from './Modules/Home/AuthorOfTheWeek/AuthorOfTheWeek';
export {default as BookOfTheWeekFirst} from './Modules/Home/BookOfTheWeekFirst/BookOfTheWeekFirst';
export {default as SliderMainPage} from './Modules/Home/SliderMainPage/SliderMainPage';
export {default as BookOfTheWeekSecondSingle} from './Modules/PostTypes/BookOfTheWeekSecondSingle/BookOfTheWeekSecondSingle';
export {default as Media} from './Modules/Home/Media/Media';
export {default as Video} from './Modules/PostTypes/Video/Video';
export {default as FilterOption} from './Modules/FilterOption/FilterOption';
export {default as FilterSelectsMode} from './Modules/FilterSelectsMode/FilterSelectsMode';
export {default as NewSelectInput} from './Modules/newSelectInput/NewSelectInput';
export {default as Footer} from './Modules/Footer/Footer';
export {default as TabSectionSingle} from './Modules/TabSectionSingle/TabSectionSingle';
export {default as FilterSelectsModeComponent} from './Modules/FilterSelectsModeComponent/FilterSelectsModeComponent';
export {default as SidebarCategory} from './Modules/SidebarCategory/SidebarCategory';
export {default as Contact} from './Modules/Contact/Contact';
export {default as ProfileBox} from "../Components/Modules/ProfileBox/ProfileBox";
export {default as CardView} from "../Components/Modules/CardView/CardView";
export {default as MediaBox} from "../Components/Modules/MediaBox/MediaBox";
export {default as TabSectionForMedia} from "../Components/Modules/TabSectionForMedia/TabSectionForMedia";
export {default as ImageBoxInMediaPage} from "../Components/Modules/ImageBoxInMediaPage/ImageBoxInMediaPage";
export {default as SoundBoxInMediaPage} from "../Components/Modules/SoundBoxInMediapage/SoundBoxInMediapage";
export {default as SoundPlayer} from "../Components/Modules/SoundPlayer/SoundPlayer";

export {default as FancyInput} from './Modules/FancyInput/FancyInput';
export {default as TextAbout} from './Modules/TextAbout/TextAbout';
export {default as LocationAndContect} from './Modules/LocationAndContect/LocationAndContect';
export {default as TextPackagePage} from './Modules/TextPackagePage/TextPackagePage';
export {default as Subject} from './Modules/Subject/Subject';
export {default as BoxInformation} from './Modules/BoxInformation/BoxInformation';
export {default as SearchIn} from './Modules/SearchPage/SearchIn/SearchIn';
export {default as MyList} from './Modules/SearchPage/MyList/MyList';
export {default as ChangePasswordModal} from './Modules/ChangePasswordModal/ChangePasswordModal';
export {default as SubmitArticle} from './Modules/SubmitArticle/SubmitArticle';
export {default as ArticleHistoryPost} from './Modules/ArticleHistoryPost/ArticleHistoryPost';
export {default as PrivateRoute} from './Modules/PrivateRoute/PrivateRoute';
export {default as SpinLoading} from './Modules/SpinLoading/SpinLoading';
export {default as __} from '../global';




// Pages
export {default as Main} from './Pages/Main/Main';
export {default as Language} from './Pages/Language/Language';
export {default as SingleBook} from './Pages/SingleBook/SingleBook';
export {default as Books} from './Pages/Books/Books';
export {default as ArticlesSubject} from './Pages/Articles Subject/ArticlesSubject';
export {default as BooksSubject} from './Pages/Books Subject/BooksSubject';
export {default as ArticlesCategory} from './Pages/Articles Category/ArticlesCategory';
export {default as BooksCategory} from './Pages/Books Category/BooksCategory';
export {default as JournalsCategory} from './Pages/Journals Category/JournalsCategory';

export {default as About} from './Pages/About/About';
export {default as Persons} from './Pages/Persons/Persons';
export {default as SinglePersons} from './Pages/SinglePersons/SinglePersons';
export {default as Package} from './Pages/Package/Package';
export {default as Subjects} from './Pages/Subjects/Subjects';
export {default as InternalSubject} from './Pages/InternalSubject/InternalSubject';
export {default as SearchPage} from './Pages/SearchPage/SearchPage';
export {default as Publishers} from './Pages/Publishers/Publishers';
export {default as Centers} from './Pages/Center/Center';
export {default as PublisherInternal} from './Pages/Publishers Internal/PublisherInternal';
export {default as Profile} from './Pages/Profile/Profile';
export {default as SingleCategory} from './Pages/Single Category/SingleCategory';
export {default as ArticlePage} from './Pages/ArticlePage/ArticlePage';
export {default as Journal} from './Pages/Journal/Journal';
export {default as SingleJournal} from './Pages/SingleJournal/SingleJournal';
export {default as SingleArticle} from './Pages/SingleArticle/SingleArticle';
export {default as Categories} from './Pages/Categories/Categories';
export {default as InternalMedia} from './Pages/InternalMedia/InternalMedia';
export {default as MediaPage} from './Pages/MediaPage/MediaPage';
