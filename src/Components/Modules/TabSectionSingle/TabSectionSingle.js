import React from 'react';
import './TabSectionSingle.css';
import connect from "react-redux/es/connect/connect";
import {withRouter} from "react-router-dom";

function TabSectionSingle(props){
    return(
        <div className="d-flex w-100 my-4 justify-content-center align-items-center">
            <h3 className={`title-BookCollection mb-0 font-F-Gothic 
            ${props.RtlDir ? "ml-4" : "mr-4"}`}>
                {props.title}
            </h3>
            <hr className="TabSectionSingleHr"/>
        </div>
    )
}
const mapStateToProps = states => (
    {
        RtlDir: states.Rtl
    }
);
export default withRouter(connect(mapStateToProps)(TabSectionSingle));