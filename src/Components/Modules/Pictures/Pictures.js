import React from 'react';
import {Col, Row} from "antd";
import {Link} from "react-router-dom";
import './Pictures.css';


export default function Pictures(props) {

    const {data} = props;
    let dataPackage = data && [data.package_one, data.package_second];

    return (
        <Row
            type="flex"
            gutter={64}
            className="
              mt-4
              mt-md-5
              flex-lg-row
              flex-column
              justify-content-lg-between"
        >
            {
                dataPackage && dataPackage.map((x, i) => (
                    <Col
                        span={24}
                        lg={12}
                        className="mb-lg-0 mb-4 parentImage"
                        key={i}
                    >
                        <Link
                            to={`/Package/${x.post_name}`}
                            className="position-relative mh-100 d-flex  flex-column justify-content-center h-100 overflow-hidden"
                            style={{borderRadius : ".5rem"}}
                        >
                            <img
                                src={x.attachment}
                                className="w-100"
                                alt="logo in islamic sources"
                            />
                            <span
                                className="
                                    w-100
                                    TitleOnPicture
                                    font-F-Gothic
                                    position-absolute
                                    d-flex
                                    justify-content-center
                                    align-items-center
                                    "
                            >
                                {x.post_title}
                             </span>
                        </Link>
                    </Col>
                ))
            }
        </Row>
    );
}