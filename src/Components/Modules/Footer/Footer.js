import React, {useState} from 'react';
import {Col, Icon, message, Row} from "antd";
import Check from '../../../images/checkFooter.svg';
import Logo from '../../../images/LogoIslamicSources.png';
import {connect} from "react-redux";
import {Link, withRouter} from "react-router-dom";
import {CustomImage} from "../CustomImage";
import {SpinLoading} from "../../Module";
import {__} from "../../../global";
import axios from "axios";
import {defaultWPUrl, ISAccessUserToken, StripHTML} from "../../../config";
import "./Footer.css";


function Footer(props) {
    const [newsLetter, SetNewsLetter] = useState("");
    const [newsLetterLoading, SetNewsLetterLoading] = useState(false);

    function setColor(target, e, hover) {
        let paths = target.currentTarget.querySelectorAll("path");
        if (hover) {
            for (let i = 0; i < paths.length; i++) {
                paths[i].style.fill = e;
            }
        } else {
            for (let i = 0; i < paths.length; i++) {
                paths[i].style.fill = "#a8a8a8";
            }
        }
    }

    async function newsletterSubmit(target) {
        target.preventDefault();
        SetNewsLetterLoading(true);
        await axios.post(defaultWPUrl + "/users/post" ,{
                "post_title" : newsLetter,
                "term": "newsletters",
            },
            {headers : {Authorization : ISAccessUserToken}})
            .then(res => {
                if(res.status === 200){
                    message.success(__("Successfully Sent"))
                }
            })
            .catch(res => {
                if (res.response && res.response.data.message){
                    message.error(__(StripHTML(res.response.data.message)))
                }
            })
        SetNewsLetterLoading(false)
    }

    const {data,style} = props;
    return (
        <Row
            style={style}
            className="d-flex flex-column-reverse justify-content-center
                       flex-lg-row align-items-center m-h-18 Color-bg
                       justify-content-lg-start align-items-lg-start"
        >
            <Col span={20} lg={10} className="my-1 my-md-5">
                <Row className="w-100 d-flex flex-row justify-content-end">
                    <Col span={24} lg={24} className="d-flex flex-column">
                        <Row className="w-100 d-flex flex-row flex-md-column
                        justify-content-center align-items-center"
                        >
                            <Col span={8} lg={8}
                                 className="d-flex justify-content-start justify-content-md-center"
                            >
                                <img
                                    src={Logo}
                                    alt="عکس لوگو"
                                    className="w-100"
                                />
                            </Col>
                            <h5 className="AboutContact d-none d-md-block my-4">
                                <Link to="/about/">
                                    {__("About & contact")}
                                </Link>
                            </h5>
                            {data && data.footer_available_sections.social_section &&
                            <Col
                                span={16}
                                lg={12}
                                className="d-flex justify-content-between my-2"
                            >
                                {Object.values(data.socials).map((x, i) => (
                                    <a
                                        href={x.url}
                                        key={i}
                                    >
                                        <CustomImage
                                            onMouseOver={target => setColor(target, x.color, true)}
                                            onMouseLeave={target => setColor(target, x.color, false)}
                                            url={x.image}
                                            className="mx-2 cursor-p VirtualIconFooter"
                                        />
                                    </a>
                                ))}
                            </Col>
                            }
                        </Row>
                        <h5 className="AboutContact d-md-none text-center my-3">
                            <Link to="/about/">
                                {__("About & contact")}
                            </Link>
                        </h5>
                        <Row className="mt-2 d-flex justify-content-center">
                            <p className="m-0 constructor">
                                All rights reserved for islamic source Designed By Rah Studio
                            </p>
                        </Row>
                    </Col>
                </Row>
            </Col>
            <Col
                span={20}
                lg={14}
                className="
                    d-flex
                    flex-column
                    position-relative
                    my-3
                    my-md-5
                    pl-md-4
                    ColParentText
                "
            >
                <div
                    className="
                        w-100
                        d-flex
                        flex-column
                        flex-lg-row
                        h-100
                    "
                >
                    {data && data.footer_available_sections.apps_section &&
                    <Col span={24} lg={8} className="d-flex flex-column">
                        <div
                            className="
                                d-flex
                                justify-content-start
                                align-items-center
                                mb-3
                            "
                        >
                            <h2
                                className="
                                    AndroidApps
                                    font-F-Gothic
                                    mb-0
                                "
                            >
                                {__("Android Apps")}
                            </h2>
                            <hr
                                className="
                                    hrFooter
                                    mx-3
                                    d-flex
                                    d-lg-none
                                "
                            />
                            <h4
                                className="
                                    cursor-p
                                    font-F-Gothic
                                    MoreTop
                                    mb-0
                                    d-flex
                                    d-lg-none
                                "
                            >
                                {__("More")}
                                <Icon
                                    type={props.RtlDir ? "left" : "right"}
                                    className="
                                        d-flex
                                        justify-content-center
                                        align-items-center
                                        d-lg-none
                                    "
                                />
                            </h4>
                        </div>
                        <div
                            className="
                                d-flex
                                flex-lg-column
                                w-100
                                h-100
                                justify-content-between
                            "
                        >
                            {data && Object.values(data.apps).map((x, i) => (
                                <a
                                    href={x.url}
                                    key={i}
                                    className="
                                        d-flex
                                        flex-column
                                        align-items-center
                                        align-items-lg-start
                                    "
                                >
                                    <img
                                        className="ImgFooter"
                                        src={x.image === false ? "#" : x.image}
                                        alt="ImgFooter"
                                    />
                                    <h4
                                        className="
                                            titleFooterLogo
                                            mb-0
                                            font-F-Gothic
                                            mt-2
                                        "
                                    >
                                        {x.title}
                                    </h4>
                                </a>
                            ))
                            }
                        </div>
                    </Col>}
                    <Col
                        span={24}
                        lg={16}
                        className="
                            d-flex
                            flex-column
                            justify-content-between
                        "
                    >
                        <h4
                            className="
                                font-F-Gothic
                                TextFooter
                                mt-3
                                mt-md-0
                                mb-0
                            "
                        >
                            <div
                                dangerouslySetInnerHTML={{
                                    __html:
                                        data
                                        && data.footer_available_sections.seo_box_section && data.seoBox
                                }}
                            />
                        </h4>
                        {data && data.footer_available_sections.newsletter &&
                        <React.Fragment>
                            <h3
                                className="
                                font-F-Gothic
                                TitleInputFooter
                                my-2
                                my-md-0
                            "
                            >
                                {__("Input to newsletter")}:
                            </h3>
                            <SpinLoading
                                loading={newsLetterLoading}
                                mode="loading"
                            >
                                <form
                                    onSubmit={val => newsletterSubmit(val)}
                                    className="
                                w-100
                                d-flex
                                justify-content-between
                            "
                                >
                                    <input
                                        type="text"
                                        placeholder={__("Your Email")}
                                        className="
                                            font-F-Gothic
                                            InputFooter
                                        "
                                        onChange={value => SetNewsLetter(value.target.value)}
                                        value={newsLetter}
                                    />
                                    <button type="submit" className="d-none" />
                                    <CustomImage
                                        onClick={val => newsletterSubmit(val)}
                                        url={Check}
                                        className="buttonFooter cursor-p"
                                    />
                                </form>
                            </SpinLoading>
                        </React.Fragment>
                        }
                    </Col>
                </div>
            </Col>
        </Row>
    );
}

const mapStateToProps = states => (
    {
        RtlDir: states.Rtl
    }
);
export default withRouter(connect(mapStateToProps)(Footer));