import React from "react";
import {Avatar, Col, Spin} from "antd";
import {Scrollbars} from "react-custom-scrollbars";
import connect from "react-redux/es/connect/connect";
import {withRouter} from "react-router-dom";
import PropTypes from "prop-types";
import {loadingItem} from "../../../config";
import {SpinLoading} from "../../Module";
import "./BoxInformation.css";
import userImage from "../../../images/user image/userImage E.png"
import {__} from "../../../global";

BoxInformation.propTypes = {
    title: PropTypes.string,
    StartingTime: PropTypes.string,
    Logo: PropTypes.string.isRequired,
};

function BoxInformation(props) {
    const {StartingTime, Logo, data, loading} = props;
    return (
        <div
            className="
            w-100
            d-flex mt-md-3
            flex-column
            flex-lg-row
            BoxPublisher"
        >
            {!loading ?
                <Col
                    span={24}
                    lg={props.RtlDir ? {span: 5, offset: 1} : 5}
                    className="
                    d-flex
                    flex-row
                    flex-lg-column
                    justify-content-start
                    justify-content-lg-center
                    align-items-end
                    align-items-lg-center"
                >
                    {Logo === "SinglePerson" &&
                    <Avatar
                        size={180}
                        src={
                            data && data.featuredImage ?
                                data.featuredImage.sourceUrl
                                :
                                userImage
                        }
                        className="mt-lg-0 mt-4 mb-0 mx-lg-0 mx-3 size"
                    />
                    }
                    {Logo === "PublisherInternal" &&
                    <div
                        className="
                        parentImageBox
                        d-flex
                        justify-content-center
                        align-items-center"
                    >
                        <img
                            src={
                                data && data.featuredImage &&
                                data.featuredImage.mediaDetails &&
                                data.featuredImage.mediaDetails.sizes[1] &&
                                data.featuredImage.mediaDetails.sizes[1].sourceUrl
                            }
                            className="w-100 h-100"
                            alt="Writer and Publisher in Islamic Sources"
                        />
                    </div>
                    }
                    <div
                        className="
                        d-flex
                        flex-column
                        align-items-lg-center
                        align-items-start
                        my-lg-4
                        mx-3
                        mx-lg-0"
                    >
                        <h2
                            className="
                            titleBoxPublisher
                            d-block
                            d-lg-none
                            font-F-Gothic
                            mb-4"
                        >
                            {data && data.title}
                        </h2>
                        {
                            Logo === "SinglePerson" &&
                            <h5 className="job-SingleWriters font-F-Gothic mb-1">
                                {data && data.types &&
                                data.types.nodes.map((x) => x.name)}
                            </h5>
                        }
                        <div>
                            <div className="d-flex">
                                <h4 className="font-F-Gothic text-bold Launched my-2">
                                    {StartingTime}
                                </h4>
                                <p className="font-F-Gothic LaunchedCount mx-1 my-2">
                                    {data && data.centerfields &&
                                    data.centerfields.centerEstablished}
                                </p>
                            </div>
                            {
                                Logo === "SinglePerson" &&
                                <div
                                    className="
                                    d-flex
                                    flex-lg-column
                                    flex-row
                                    align-items-center
                                    justify-content-between"
                                    style={{minWidth: "10rem"}}
                                >
                                    <div className="d-flex">
                                        <h4 className="font-F-Gothic text-bold Launched my-2">
                                            {__("Born")}:
                                        </h4>
                                        <p className="font-F-Gothic LaunchedCount mx-1 my-2">
                                            {data && data.personfields &&
                                            data.personfields.personBorn}
                                        </p>
                                    </div>
                                    <div className="d-flex my-2">
                                        <h4 className="font-F-Gothic text-bold Launched my-lg-0 my-2">
                                            {__("Died")}:
                                        </h4>
                                        <p className="font-F-Gothic LaunchedCount mx-1 my-lg-0 my-2">
                                            {data && data.personfields &&
                                            data.personfields.personDied}
                                        </p>
                                    </div>
                                </div>
                            }
                        </div>
                    </div>
                </Col>
                :
                <Spin
                    spinning
                    className="
                    w-100
                    d-flex
                    justify-content-center
                    align-items-center"
                    style={{height: "20vh", maxWidth: "15vw"}}
                    indicator={
                        <img
                            className="w-25 h-auto"
                            src={loadingItem}
                            alt="loading"
                        />
                    }
                />
            }

            {
                <SpinLoading
                    loading={loading}
                    mode="withoutBox"
                >
                    <Col span={24} lg={props.RtlDir ? 18 : {span: 18, offset: 1}}>
                        <h2
                            className="
                                titleBoxPublisher
                                d-none
                                d-lg-block
                                font-F-Gothic"
                        >
                            {data && data.title}
                        </h2>
                        <Scrollbars className="PublisherScroll mt-3">
                            <div className="ParentTextPublisher">
                                <h4
                                    className="font-F-Gothic"
                                    dangerouslySetInnerHTML={{__html: data && data.content}}
                                />
                            </div>
                        </Scrollbars>
                    </Col>
                </SpinLoading>
            }
        </div>
    )
}

const mapStateToProps = states => (
    {
        RtlDir: states.Rtl
    }
);
export default withRouter(connect(mapStateToProps)(BoxInformation));