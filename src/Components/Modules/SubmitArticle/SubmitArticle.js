import React, {useState} from 'react';
import {useHistory} from 'react-router-dom';
import {TabSection, SpinLoading} from "../../Module";
import {Icon, message, Upload} from "antd";
import {defaultWPUrl, getBase64, ISAccessUserToken} from "../../../config";
import {__} from "../../../global";
import axios from "axios";
import PropTypes from "prop-types";

function SubmitArticle(props) {
    const [loading,SetLoading] = useState(false);
    const [loadingSubmit,SetLoadingSubmit] = useState(false);
    const [imageUrl,SetImageUrl] = useState("");
    const [submitArticleInformation] = useState({
        term : "sent",
        post_title : "",
        writer : "",
        subject : "",
        source : "",
        file : 0,
    });

    function update_fields(target) {
        submitArticleInformation[target.target.name] = target.target.value;
    }

    async function sendPost(val, values, history) {
        val.preventDefault();
        await SetLoadingSubmit(true);
        await axios.post(defaultWPUrl + '/users/post', values,
            {headers : { Authorization : ISAccessUserToken }})
            .then(async res => {
                if(res.status === 200){
                    message.success(__(props.message));
                    await SetLoadingSubmit(false);
                    history.push('/profile');
                }
            })
            .catch(async res => {
                if(res.response && res.response.data.message){
                    await SetLoadingSubmit(false);
                    await message.error(res.response.data.message);
                }
            });
    }

    function beforeUpload(file) {
        const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
        if (!isJpgOrPng) {
            message.error('You can only upload JPG/PNG file!');
        }
        const isLt2M = file.size / 1024 / 1024 < 2;
        if (!isLt2M) {
            message.error('Image must smaller than 2MB!');
        }
        return isJpgOrPng && isLt2M;
    }
    function handleChange (info) {
        if (info.file.status === 'uploading') {
            SetLoading(true);
            return;
        }
        if (info.file.status === 'done') {
            getBase64(info.file.originFileObj, imageUrl =>
                SetImageUrl(imageUrl),
                SetLoading(false),
            );
            if(info.file.response)
                submitArticleInformation['file'] =  info.file.response.data;
        }
    }

    const uploadButton = (
        <div>
            <Icon type={loading ? 'loading' : 'plus'} />
            <div className="ant-upload-text">{__('Upload')}</div>
        </div>
    );
    const history = useHistory();
    return (
        <div id="submit_post">
            <SpinLoading
                loading={loadingSubmit}
                mode="logo"
            >
                <form onSubmit={val => sendPost(val, submitArticleInformation, history)} className="mt-3 d-flex flex-column">
                    <TabSection
                        hr
                        more={false}
                        title={__('Submit Article')}
                        moreLink="#"
                    />
                    <div id="submitArticle" className="w-100 d-flex flex-column flex-lg-row justify-content-between mt-3">
                        <div className="SubmitArticle d-flex flex-column">
                            <input
                                type="text"
                                className="InputSubmitArticle mb-3 px-3"
                                placeholder={__("Title")}
                                name="post_title"
                                onChange={update_fields}
                            />
                            <input
                                type="text"
                                className="InputSubmitArticle mb-3 px-3"
                                placeholder={__("Writers")}
                                name="writer"
                                onChange={update_fields}
                            />
                            <input
                                type="text"
                                className="InputSubmitArticle mb-3 px-3"
                                placeholder={__("Subject")}
                                name="subject"
                                onChange={update_fields}
                            />
                            <input
                                type="text"
                                className="InputSubmitArticle mb-3 px-3"
                                placeholder={__("Source")}
                                name="source"
                                onChange={update_fields}
                            />
                        </div>
                        <div className="SubmitArticle d-flex flex-column align-items-center">
                            <Upload
                                style={{height : "7.5rem"}}
                                name="avatar"
                                listType="picture-card"
                                className="avatar-uploader mb-3 mb-lg-0"
                                showUploadList={false}
                                action={defaultWPUrl + '/upload'}
                                headers={{Authorization : ISAccessUserToken}}
                                data={val => ({file : val})}
                                beforeUpload={beforeUpload}
                                onChange={handleChange}
                            >
                                {imageUrl ? <img src={imageUrl} alt="avatar" style={{width: '100%'}}/> : uploadButton}
                            </Upload>
                            <button className="font-F-Gothic mt-2 mt-lg-3 SendButtonProfile">{__('Send')}</button>
                        </div>
                    </div>
                </form>
            </SpinLoading>
        </div>
    );
}

SubmitArticle.propTypes = {
    message : PropTypes.string.isRequired
};

export default SubmitArticle;