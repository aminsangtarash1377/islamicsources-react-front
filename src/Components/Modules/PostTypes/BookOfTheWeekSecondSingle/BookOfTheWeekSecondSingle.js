import React from 'react';
import './BookOfTheWeekSecondSingle.css';
import {Col, Icon} from "antd";
import {__} from "../../../../global";
import connect from "react-redux/es/connect/connect";
import {withRouter} from "react-router-dom";


function BookOfTheWeekSecondSingle(props) {

    const {data} = props;
    return (
        <Col
            span={24}
            className="
            p-4
            bg-BookOfTheWeekSecondSingle
            d-flex
            justify-content-around
            align-items-center
            font-F-Gothic"
            style={{direction:props.RtlDir?"rtl":"ltr"}}
        >
            <img
                src={data.image}
                className={props.RtlDir? "width ml-4" :"width mr-4"}
                alt={__("Book Of The Week")}
            />
            <div className="d-flex flex-column" style={{maxWidth: "17.5rem"}}>
                <h5 className="title-BookOfTheWeekSecondSingle D-letter">
                    {data.title}
                </h5>
                <p className="text-BookOfTheWeekSecondSingle">
                    {data.text}
                </p>
                <div className="d-flex align-items-center">
                        <span
                            className="bg-Points d-flex justify-content-center align-items-center"
                            style={{width: "2rem", height: "1rem"}}
                        >
                            {data.count}
                        </span>
                    <div className="bg-Points mx-2 d-flex justify-content-center align-items-center"
                         style={{width: "4rem", height: "1rem"}}
                    >
                        <Icon className="mx-1" type="eye" theme="filled"/>
                        <span className="mx-1">
                            {data.numberOfVisits}
                        </span>
                    </div>
                </div>
            </div>
        </Col>
    );
}

const mapStateToProps = states => (
    {
        RtlDir: states.Rtl,
    }
);
export default withRouter(connect(mapStateToProps)(BookOfTheWeekSecondSingle));