import React from 'react';
import './Video.css';
import {Link} from "react-router-dom";
import SvgPlay from "../../../../images/Play A.svg";


export default function Video(props) {
    const {data} = props;

    return (
        <Link to=""
              className="d-flex flex-column align-items-center mb-5 mb-lg-0"
              style={{minHeight:"15.5rem"}}
        >
            <div className="videoDiv d-flex align-items-center justify-content-center">
                <img
                    src={data.src}
                    className="w-100 position-relative"
                    alt="Book in islamic Sources"
                />
                <div
                    className="
                    BGPlayerSvg-Video
                    position-absolute
                    cursor-p
                    d-flex
                    justify-content-center
                    align-items-center"
                >
                    <img
                        src={SvgPlay}
                        className="PlayerSvg-Video"
                        alt="click"
                    />
                </div>
            </div>
            <p className="mt-4 TitleVideo font-F-Gothic text-center"
               style={{color: "#606060", fontSize: "15px"}}
            >
                {data.text}
            </p>
        </Link>
    );
}