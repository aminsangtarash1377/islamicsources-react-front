import React, {useEffect, useState} from 'react';
import {Popover} from "antd";
import {Link, withRouter} from "react-router-dom";
import {defaultRahUrl} from "../../../../config";
import defaultImage from '../../../../images/defaultBook.png';
import defaultUserImage from '../../../../images/user image/userImage E.png';
import axios from "axios";
import PropTypes from "prop-types";
import connect from "react-redux/es/connect/connect";
import {__} from "../../../../global";
import './Book.css';

Book.propTypes = {
    targetLocation: PropTypes.string.isRequired,
    popover: PropTypes.string.isRequired,
    className: PropTypes.string
};

function Book(props) {
    const {data, targetLocation, popover, className} = props;
    const [bookCount, setBookCount] = useState(null);

    function getBookCount() {
        axios.get(
            defaultRahUrl + `/publisher?slug=${data && data.slug}&books_count=true`
        )
            .then(result => {
                setBookCount(result.data)
            })
    }

    useEffect(() => {
        popover === "Person" && getBookCount();
    }, []);


    const content =
        (
            popover === "Book" ?
                <div
                    className={`d-flex flex-column align-items-start pupOverBook ${
                        props.RtlDir ? "pupOverBookRTL" : "pupOverBookLTR"}`
                    }
                    style={
                        {
                            lineHeight: "1.4rem",
                        }
                    }
                >
                    <span className="title-Book-hover">{data && data.title}</span>
                    <span className="content-Book-hover">
                        {targetLocation && targetLocation === "Journals"
                            ?
                            data.magazinefields &&
                            data.magazinefields.actConcessionaire &&
                            data.magazinefields.actConcessionaire.title &&
                            data.magazinefields.actConcessionaire.title[0]
                            :
                            data &&
                            data.bookfields &&
                            data.bookfields.actAuthor &&
                            data.bookfields.actAuthor[0] &&
                            data.bookfields.actAuthor[0].title
                        }
                    </span>
                    <span className="content-Book-hover font-F-Gothic">
                        {targetLocation && targetLocation === "Journals"
                            ?
                            data.magazinefields && data.magazinefields.actRelease
                            :
                            data && data.bookfields && data.bookfields.actPublishDate
                        }
                    </span>
                    <div
                        className={`
                          d-flex
                          justify-content-between 
                          align-items-center
                          ${props.RtlDir ? "pupOverBookRTL" : "pupOverBookLTR"}
                    `}>
                        <span
                            className={`
                                Points-Book-hover
                                ${props.RtlDir ? "ml-2" : ""}`
                            }
                        >
                            {targetLocation && targetLocation === "Journals"
                                ?
                                data && data.magazinefields && data.magazinefields.rate
                                    ?
                                    isNaN(Math.round(data && data.magazinefields && data.magazinefields.rate)) ?
                                        "-"
                                        :
                                        Math.round(data && data.magazinefields && data.magazinefields.rate)
                                    : "-"

                                :
                                data && data.bookfields && data.bookfields.rate
                                    ?
                                    isNaN(Math.round(data && data.bookfields && data.bookfields.rate)) ?
                                        "-"
                                        :
                                        Math.round(data && data.bookfields && data.bookfields.rate)
                                    : "-"
                            }
                        </span>
                        <span
                            className={`
                                contentLast-Book-hover 
                                ${props.RtlDir ? "ml-2" : ""}`
                            }
                        >
                            {targetLocation && targetLocation === "Journals"
                                ?
                                data.magazinefields && data.magazinefields.view
                                :
                                data && data.bookfields && data.bookfields.view
                            } {__("View")}
                        </span>
                    </div>
                </div>
                :
                popover === "Person" ?
                    <div
                        className={`d-flex flex-column align-items-start pupOverBook ${
                            props.RtlDir ? "pupOverBookRTL" : "pupOverBookLTR"}`}
                        style={{lineHeight: "1.4rem"}}
                    >
                        <span className="title-Book-hover">
                            {data && data.title}
                        </span>
                        <div
                            className={`
                                content-popoverPerson
                                d-flex
                                justify-content-start
                                align-items-center 
                                ${props.RtlDir ? "pupOverBookRTL" : "pupOverBookLTR"}`
                            }
                        >
                            <span className={props.RtlDir ? "ml-2" : ""}>
                                {bookCount}
                            </span>
                            <span className={props.RtlDir ? "ml-2" : ""}>
                                {__("Book")}
                            </span>
                        </div>
                    </div>
                    :
                    popover === "SinglePerson" ?
                        <div
                            className={`d-flex flex-column align-items-start pupOverBook ${
                                props.RtlDir ? "pupOverBookRTL" : "pupOverBookLTR"}`}
                            style={{lineHeight: "1.4rem"}}
                        >
                            <span className="title-Book-hover">{data.text}</span>
                            <span className="content-Book-hover">Soheil M Afnan</span>
                            <span className="content-Book-hover font-F-Gothic">1958</span>
                        </div>
                        :
                        popover === "Picture" ?
                            <div
                                className={`d-flex flex-column align-items-start 
                                ${props.RtlDir ? "pupOverBookRTL" : "pupOverBookLTR"}`}
                            >
                                <span className="title-Book-hover">{data && data.title}</span>
                            </div>
                            :
                            ""
        );


    return (
        <Link
            to={`/${targetLocation}/${data && data.slug}/`}
            className="ParentDivBook d-flex flex-column align-items-center mb-4 mb-lg-0"
        >
            <Popover
                content={content}
                placement={props.RtlDir ? "bottomRight" : "bottomLeft"}
                className={props.RtlDir ? "pupOverBookRTL" : "pupOverBookLTR"}
            >
                <div
                    className={
                        className === "Book"
                            ?
                            "BookDiv"
                            :
                            className === "Picture"
                                ?
                                "BookDiv-Picture"
                                :
                                "BookDiv"
                    }
                >
                    <img
                        src={
                            data && data.featuredImage ?
                                data.featuredImage.sourceUrl
                                :
                                (
                                    targetLocation === "Persons"
                                        ?
                                        defaultUserImage
                                        :
                                        defaultImage
                                )
                        }
                        className="h-100 w-auto"
                        alt="Book in islamic Sources"
                    />
                </div>
            </Popover>
            <p
                className="mt-3 mt-md-4 titleBook"
                style={{color: "#797979", font_size: "1rem"}}
            >
                {data && data.title}
            </p>
        </Link>
    );
}

const mapStateToProps = states => (
    {
        RtlDir: states.Rtl
    }
);
export default withRouter(connect(mapStateToProps)(Book));