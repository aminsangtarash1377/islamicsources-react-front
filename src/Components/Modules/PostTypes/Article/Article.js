import React from 'react';
import {__} from "../../../../global";
import './Article.css';
import {Col} from "antd";
import {Link} from "react-router-dom";



export default function Article(props) {
    const {link, title, slug, view, rate, author} = props;

    return (
        <div className="Article px-2">
            <Link to={`/${link}/${slug && slug}`} style={{minHeight: "4.2rem", width: "100%"}}
                  className="border mb-4 px-3 px-md-4 d-flex
                 justify-content-between align-items-center
                 After-Before-None"
            >
                <Col xs={15} md={19} lg={18}
                     className="font-F-Gothic d-flex flex-column
                     justify-content-start align-items-start"
                >
                    <span className="title-Article font-F-Gothic">
                    {__(title && title)}
                    </span>
                    <label className="label-Article font-F-Gothic">
                        {__(author && author)}
                    </label>
                </Col>
                <Col xs={9} md={5} lg={6}
                     className="font-F-Gothic d-flex align-items-center">
                    <span className="mx-2 points-Article"
                    >
                        {rate && isNaN(Math.round(rate)) ? "-" : Math.round(rate)}
                    </span>
                    <span className="count-Article">
                    {view && view + " "}{__("View")}
                    </span>
                </Col>
            </Link>
        </div>
    );
}
