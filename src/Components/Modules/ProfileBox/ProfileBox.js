import React, {useState} from 'react';
import {Avatar, Col, Icon, Input, message, Upload} from "antd";
import connect from "react-redux/es/connect/connect";
import {withRouter, useHistory} from "react-router-dom";
import {__} from "../../../global";
import axios from "axios";
import {defaultWPUrl, getBase64, ISAccessUserToken} from "../../../config";
import {UserDetailPush} from "../../../actions";
import {ChangePasswordModal, SpinLoading} from "../../Module";
import "./ProfileBox.css";


function ProfileBox(props) {
    const [change, SetChange] = useState(true);
    const [res, Setres] = useState(false);
    const [visible, setVisible] = useState(false);
    const [loading ,setLoading] = useState(false);
    const [uploadLoading ,setUploadLoading] = useState(false);
    const [imageUrl ,setImageUrl] = useState('');
    let [_tempData] = useState(false);


    let data = props.userDetails;

    _tempData = data;

    function updateFields(target) {
        _tempData[target.target.name] = target.target.value;
    }

    async function updateDetails(val, input, history){
        val.preventDefault();
        setLoading(true);
        await axios.post(
            defaultWPUrl + '/users/edit/current',
            {
                first_name : input.first_name === "" ? data.first_name : input.first_name,
                last_name : input.last_name === "" ? data.last_name : input.last_name,
                user_login : input.user_login === "" ? data.user_login : input.user_login,
                user_email : input.user_email === "" ? data.user_email : input.user_email,
                gender : input.gender === "" ? data.gender : input.gender,
                birth_date : input.birth_date === "" ? data.birth_date : input.birth_date,
                phone_number : input.phone_number === "" ? data.phone_number : input.phone_number,
                social_pages : input.social_pages === "" ? data.social_pages : input.social_pages,
                address : input.address === "" ? data.address : input.address,
                avatar : isNaN(parseInt(input.avatar)) ? null : input.avatar,
            },
            {headers : {Authorization : ISAccessUserToken}}
        )
            .then(async res => {
                if(res.status === 200){
                    if(!isNaN(parseInt(input['avatar']))){
                        input.avatar = await get_attachment_url(input.avatar);
                    }
                    input.display_name = input.first_name + " " + input.last_name;
                    await message.success(__('Your details successfully changed'));
                    await props.dispatch(UserDetailPush(input));
                    setLoading(false);
                    SetChange(!change);
                    history.push('/profile');
                }
            })
            .catch(async res => {
                setLoading(false);
                if(res.response && res.response.data.message){
                    message.error(__(res.response.data.message))
                }
            })
    }

    const uploadButton = (
        <div>
            <Icon type={uploadLoading ? 'loading' : 'plus'} style={{fontSize : "200%"}} />
            <div className="ant-upload-text">{__('Upload')}</div>
        </div>
    );
    function beforeUpload(file) {
        const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
        if (!isJpgOrPng) {
            message.error(__('You can only upload JPG/PNG file!'));
        }
        const isLt2M = file.size / 1024 / 1024 < 2;
        if (!isLt2M) {
            message.error(__('Image must smaller than 2MB!'));
        }
        return isJpgOrPng && isLt2M;
    }
    function handleChange(info) {
        if (info.file.status === 'uploading') {
            setUploadLoading(true);
            return;
        }
        if (info.file.status === 'done') {
            // Get this url from response in real world.
            getBase64(info.file.originFileObj, imageUrl => {
                setUploadLoading(false);
                setImageUrl(imageUrl);
                if(info.file.response)
                    _tempData['avatar'] =  info.file.response.data;
                },
            );
        }
    }

    async function get_attachment_url(id) {
        await fetch(defaultWPUrl + '/attachment/' + id)
            .then(res => res.json())
            .then(res => data['avatar'] = res.data);
        return data['avatar'];
    }

    const history = useHistory();
    return (
        <SpinLoading
            loading={loading}
            mode="logo"
        >
            <Col
                span={24}
                lg={6}
                className="d-flex flex-row flex-lg-column
                            align-items-center justify-content-between"
            >
                <div>
                    {
                        !change ?
                            <Upload
                                name="avatar"
                                listType="picture-card"
                                className="avatar-uploader AvatarProfile w-100"
                                showUploadList={false}
                                action={defaultWPUrl + "/upload"}
                                headers={{Authorization : ISAccessUserToken}}
                                data={val => ({file : val})}
                                beforeUpload={beforeUpload}
                                onChange={handleChange}
                                size={165}
                                shape="circle"
                            >
                                {
                                    uploadLoading ?
                                        uploadButton :
                                        imageUrl ? <Avatar shape="circle" src={imageUrl} size={165} className="AvatarProfile" /> :
                                            !data.avatar ?
                                                uploadButton :
                                                <Avatar
                                                    shape="circle"
                                                    src={data.avatar}
                                                    size={165}
                                                    className="AvatarProfile"
                                                />
                                }
                            </Upload>
                            :
                            <Avatar
                                size={165}
                                src={!data.avatar ? "" : data.avatar}
                                shape="circle"
                                icon={!data.avatar ? "user" : ""}
                                className="AvatarProfile"
                            />
                    }
                </div>
                {change ?
                    <button onClick={() => SetChange(!change)}
                            className="EditButton mt-lg-5 cursor-p font-F-Gothic">
                        {__('Edit Profile')}
                    </button>
                    :
                    <button onClick={val => updateDetails(val, _tempData, history)}
                            className="UpdateButton mt-lg-5 cursor-p font-F-Gothic">
                        {__('Update Profile')}
                    </button>
                }
            </Col>
            <Col span={24} lg={17} className="mt-3 mt-md-0">
                <div
                    // onSubmit={val => updateDetails(val, _tempData, history)}
                    className="w-100 d-flex flex-column
                               InformationProfile"
                >
                    <div className="Row w-100 d-flex flex-column flex-lg-row">
                        <Col
                            span={24}
                            lg={10}
                            className="d-flex"
                            style={!props.RtlDir ? {marginRight: "2.5rem"} : {marginLeft: "2.5rem"}}
                        >
                            <div className="d-flex flex-row align-items-center w-100 mb-3 mb-md-0">
                                <h4 className="BlueTextProfile font-F-Gothic">
                                    {__('User Name')} :
                                </h4>
                                <h4 className={`font-F-Gothic mb-0 ${props.RtlDir
                                    ? "mr-2"
                                    : "ml-2"
                                    }`}>{data && data.user_login}</h4>
                            </div>
                        </Col>
                        <Col
                            span={24}
                            lg={10}
                            className="d-flex flex-column"
                        >
                            <div
                                className="
                                d-flex
                                flex-row
                                justify-content-between
                                align-items-center
                                w-100
                                mb-3 mb-md-0"
                            >
                                <div className="d-flex">
                                    <h4 className="BlueTextProfile font-F-Gothic">
                                        {__('Password')} :
                                    </h4>
                                    <Input
                                        readOnly
                                        type="Password"
                                        value="0000000000000000000000"
                                        style={{border:"none", outline:"none", boxShadow:"none"}}
                                    />
                                </div>
                                {!change &&
                                <div>
                                    <button
                                        className={
                                            `EditButton cursor-p font-F-Gothic 
                                        ${props.RtlDir
                                                ? "mr-2"
                                                : "ml-2"
                                                }`}
                                        onClick={() => setVisible(true)}
                                    >
                                        {__('Edit')}
                                    </button>
                                </div>
                                }
                            </div>
                        </Col>
                    </div>
                    <div className="Row w-100 d-flex flex-column flex-lg-row">
                        <Col
                            span={24}
                            lg={10}
                            className="d-flex"
                            style={!props.RtlDir ? {marginRight: "2.5rem"} : {marginLeft: "2.5rem"}}
                        >
                            <div className="d-flex flex-row align-items-center w-100 mb-3 mb-md-0">
                                <h4 className="BlueTextProfile font-F-Gothic">
                                    {__('First Name')} :
                                </h4>
                                {change ?
                                    <h4
                                        className={
                                            `font-F-Gothic 
                                            mb-0 
                                            ${props.RtlDir
                                                ? "mr-2"
                                                : "ml-2"
                                                }`}
                                    >
                                        {data.first_name}
                                    </h4>
                                    :
                                    <input
                                        name="first_name"
                                        onChange={updateFields}
                                        type="text"
                                        placeholder={data.first_name}
                                        className={`InputsProfile ${props.RtlDir
                                            ? "mr-2"
                                            : "ml-2"
                                            }`}
                                    />
                                }
                            </div>
                        </Col>
                        <Col
                            span={24}
                            lg={10}
                            className="d-flex flex-column"
                        >
                            <div className="d-flex flex-row align-items-center w-100 mb-3 mb-md-0">
                                <h4 className="BlueTextProfile font-F-Gothic">
                                    {__('Last Name')} :
                                </h4>
                                {change ?
                                    <h4
                                        className={
                                            `font-F-Gothic mb-0 
                                            ${props.RtlDir
                                                ? "mr-2"
                                                : "ml-2"
                                                }`}
                                    >
                                        {data.last_name}
                                    </h4>
                                    :
                                    <input
                                        name="last_name"
                                        onChange={updateFields}
                                        type="text"
                                        placeholder={data.last_name}
                                        className={`InputsProfile ${props.RtlDir
                                            ? "mr-2"
                                            : "ml-2"
                                            }`}
                                    />
                                }
                            </div>
                        </Col>
                    </div>
                    <div
                        className={
                            `Row w-100 
                             d-lg-flex 
                             flex-column
                             flex-lg-row
                            ${res ? 'd-flex' : "d-none"}`}
                    >
                        <Col
                            span={24}
                            lg={10}
                            className="d-flex"
                            style={!props.RtlDir ? {marginRight: "2.5rem"} : {marginLeft: "2.5rem"}}
                        >
                            <div className="d-flex flex-row align-items-center w-100 mb-3 mb-md-0">
                                <h4 className="BlueTextProfile font-F-Gothic">
                                    {__('Gender')} :
                                </h4>
                                {change ?
                                    <h4 className={`font-F-Gothic mb-0 ${props.RtlDir
                                        ? "mr-2"
                                        : "ml-2"
                                        }`}>{__(data && data.gender)}</h4>
                                    :
                                    <div
                                        className="
                                        d-flex
                                        flex-row
                                        justify-content-between
                                        align-items-center
                                        mx-4"
                                    >
                                        <h4
                                            className={`
                                            font-F-Gothic mb-0 
                                            ${props.RtlDir
                                                ? "mr-2"
                                                : "ml-2"
                                                }`}
                                        >
                                            {__('Male')}
                                        </h4>
                                        <input
                                            type="radio"
                                            name="gender"
                                            onChange={updateFields}
                                            value="male"
                                            defaultChecked={data.gender === "male"}
                                            className="cursor-p mx-2"
                                        />
                                        <h4
                                            className={`font-F-Gothic mb-0 ${props.RtlDir
                                                ? "mr-2"
                                                : "ml-2"
                                                }`}>
                                            {__('Female')}
                                        </h4>
                                        <input
                                            type="radio"
                                            name="gender"
                                            onChange={updateFields}
                                            value="female"
                                            className="cursor-p mx-2"
                                            defaultChecked={data.gender === "female"}
                                        />
                                    </div>
                                }
                            </div>
                        </Col>
                        <Col
                            span={24}
                            lg={10}
                            className="d-flex flex-column"
                        >
                            <div className="d-flex flex-row align-items-center w-100 mb-3 mb-md-0">
                                <h4 className="BlueTextProfile font-F-Gothic">
                                    {__('Date of birth')} :
                                </h4>
                                {change ?
                                    <h4 className={`font-F-Gothic mb-0 ${props.RtlDir
                                        ? "mr-2"
                                        : "ml-2"
                                        }`}>
                                        {data && data.birth_date}
                                    </h4>
                                    :
                                    <input
                                        name="birth_date"
                                        onChange={updateFields}
                                        type="text"
                                        placeholder={data && data.birth_date}
                                        className={`InputsProfile ${props.RtlDir
                                            ? "mr-2"
                                            : "ml-2"
                                            }`}
                                    />
                                }
                            </div>
                        </Col>
                    </div>
                    <div className={`Row w-100 d-lg-flex flex-column flex-lg-row ${res ? 'd-flex' : "d-none"}`}>
                        <Col
                            span={24}
                            lg={10}
                            className="d-flex"
                            style={!props.RtlDir ? {marginRight: "2.5rem"} : {marginLeft: "2.5rem"}}
                        >
                            <div className="d-flex flex-row align-items-center w-100 mb-3 mb-md-0">
                                <h4 className="BlueTextProfile font-F-Gothic">
                                    {__('Phone Number')} :
                                </h4>
                                {change ?
                                    <h4 className={`font-F-Gothic mb-0 ${props.RtlDir
                                        ? "mr-2"
                                        : "ml-2"
                                        }`}>{data && data.phone_number}</h4>
                                    :
                                    <input
                                        name="phone_number"
                                        onChange={updateFields}
                                        type="text"
                                        placeholder={data && data.phone_number}
                                        className={`InputsProfile ${props.RtlDir
                                            ? "mr-2"
                                            : "ml-2"
                                            }`}
                                    />
                                }
                            </div>
                        </Col>
                        <Col
                            span={24}
                            lg={10}
                            className="d-flex flex-column"
                        >
                            <div className="d-flex flex-row align-items-center w-100 mb-3 mb-md-0">
                                <h4 className="BlueTextProfile font-F-Gothic">
                                    {__('Email Address')} :
                                </h4>
                                {change ?
                                    <h4 className={`font-F-Gothic mb-0 ${props.RtlDir
                                        ? "mr-2"
                                        : "ml-2"
                                        }`}>{data && data.user_email}</h4>
                                    :
                                    <input
                                        name="user_email"
                                        onChange={updateFields}
                                        type="text"
                                        placeholder={data && data.user_email}
                                        className={`InputsProfile ${props.RtlDir
                                            ? "mr-2"
                                            : "ml-2"
                                            }`}
                                    />
                                }
                            </div>
                        </Col>
                    </div>
                    <div className={`w-100 d-lg-flex flex-column flex-lg-row ${res ? 'd-flex' : "d-none"}`}>
                        <Col
                            span={24}
                            lg={10}
                            className="d-flex"
                            style={!props.RtlDir ? {marginRight: "2.5rem"} : {marginLeft: "2.5rem"}}
                        >
                            <div className="d-flex flex-column w-100 mb-3 mb-md-0">
                                <h4 className="BlueTextProfile font-F-Gothic">
                                    {__('Social Pages')} :
                                </h4>
                                {change ?
                                    <h4 className="font-F-Gothic mb-0">
                                        {data && data.social_pages}
                                    </h4>
                                    :
                                    <textarea
                                        name="social_pages"
                                        onChange={updateFields}
                                        placeholder={data && data.social_pages}
                                        className="InputsProfile"
                                    />
                                }
                            </div>
                        </Col>
                        <Col
                            span={24}
                            lg={10}
                            className="d-flex flex-column"
                        >
                            <div className="d-flex flex-column w-100 mb-3 mb-md-0">
                                <h4 className="BlueTextProfile font-F-Gothic">
                                    {__('Address')} :
                                </h4>
                                {change ?
                                    <h4 className="font-F-Gothic mb-0">
                                        {data && data.address}
                                    </h4>
                                    :
                                    <textarea
                                        name="address"
                                        onChange={updateFields}
                                        placeholder={data && data.address}
                                        className="InputsProfile"
                                    />
                                }
                            </div>
                        </Col>
                    </div>
                </div>
                <ChangePasswordModal setVisible={setVisible}  visible={visible} history={history} />
            </Col>
            {!res &&
                <h4
                    className="font-F-Gothic MoreInfoProfile
                                   mt-2 d-block d-lg-none"
                    onClick={() => Setres(true)}
                >
                    {__("More Info")}
                </h4>
            }
        </SpinLoading>
    )
}

const mapStateToProps = states => (
    {
        RtlDir: states.Rtl,
        userDetails : states.userDetails
    }
);
export default withRouter(connect(mapStateToProps)(ProfileBox));