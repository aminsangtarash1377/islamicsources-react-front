import React, {Component} from "react";
import {GoogleApiWrapper, Map, InfoWindow, Marker} from 'google-maps-react';

export class MapContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showingInfoWindow: false,
            activeMarker: {},
            selectedPlace: {},
        };
    }


    onMarkerClick = (props, marker, e) =>
        this.setState({
            selectedPlace: props,
            activeMarker: marker,
            showingInfoWindow: true
        });
    render() {
        return (
            <Map
                google={this.props.google}
                zoom={14}
                className="position-relative w-75"
                initialCenter={{
                    lat: 34.63,
                    lng: 50.87
                }}>

                <Marker onClick={this.onMarkerClick}
                        name={'Current location'} />

                <InfoWindow
                    marker={this.state.activeMarker}
                    visible={this.state.showingInfoWindow}>
                    <div>
                        <h1>{this.state.selectedPlace.name}</h1>
                    </div>
                </InfoWindow>
            </Map>
        );
    }
}

export default GoogleApiWrapper({
    apiKey: ("AIzaSyDCZNlLOpdRbxzaTrSzDHyw3lCqIC-ShKQ")
})(MapContainer)