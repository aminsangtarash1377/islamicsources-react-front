import React from "react";
import AudioPlayer from "react-h5-audio-player";
import "./SoundPlayer.css";

export default function SoundPlayer(props) {
    const {playing,data,pausing} = props;

    let audio = new Audio(data && data);

    function start(){
        return  audio.play();
    }

    function listen(value) {
        console.log(value);
    }
    return(
        <div>
            <AudioPlayer
                onListen={listen}
                autoPlay={false}
                src={data && data.src}
                onPlay={playing}
                onPause={pausing}
                topTitle={data && data.title}
                // other props here
            />
        </div>
    )
}