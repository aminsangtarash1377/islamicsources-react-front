import React, {useState} from 'react';
import {Button, Icon, Row} from "antd";
import {Link} from "react-router-dom";
import PropTypes from "prop-types";
import {__} from "../../../global";
import './TabSection.css';


export default function TabSection(props) {
    let [mode, SetMode] = useState("Books");
    function onTabChange(key) {
        const {onTabChange} = props;
        onTabChange(key);
        if(key.mode === "article")
            SetMode("Articles");
        else if(key.mode === "journal")
            SetMode("Journals")
    }


    const {
        title, className, tabs,
        moreLink, more, hr,
        defaultSelectedButton,
        IconSubject, onClickTrue,
        onClickFalse, IconCategory,
        onClickShow, styleSearchPage,
        styleHr,subCategoryLink
    } = props;


    return (
        <Row
            type="flex"
            className={
                "tabSection-parent " +
                "justify-content-lg-between " +
                "align-items-start " +
                "flex-md-row " +
                "w-100 " +
                "flex-column " +
                className
            }
        >
            <div
                className={`d-flex flex-lg-row flex-column justify-content-between
                 align-items-start w-100 position-relative`}
            >
                <h3 className={`title-BookCollection font-F-Gothic mb-1 ${!hr && "position-absolute bordered-box"}`}>
                    {title && title}
                </h3>
                <div
                    className={`ExceptTitle w-100 justify-content-between
                    ${!hr && "bordered-box d-flex"} 
                    ${tabs || more ? "BorderXs" : ""}
                    ${!tabs && more && !IconCategory && !IconSubject ? "d-none d-md-flex" : "d-flex"}`}
                >
                    {
                        tabs &&
                        <div className={
                            hr ?
                                "d-flex mx-lg-3"
                                :
                                `d-flex justify-content-between mr-lg-3 
                                ${styleSearchPage && "AllButtonWidth"}`
                        }
                        >
                            {tabs.length > 1 && tabs.map((d, i) => (
                                <Button
                                    key={i}
                                    onClick={() => onTabChange(d)}
                                    className={
                                        defaultSelectedButton === d.title ?
                                            (more ? "mx-md-2 " : "mr-md-2 ") +
                                            "d-flex " +
                                            "justify-content-center " +
                                            "button-BookCollection " +
                                            "font-F-Gothic " +
                                            "selected-tab-button"
                                            :
                                            (more ? "mx-md-2 " : "mr-md-2 ") +
                                            "mx-rtl " +
                                            "d-flex " +
                                            "justify-content-center " +
                                            "button-BookCollection " +
                                            "font-F-Gothic"
                                    }
                                >
                                    {d.title}
                                </Button>
                            ))}
                        </div>
                    }
                    {hr &&
                    <p className="colorGaryBorder d-none d-md-block
                     mt-3 d-lg-block d-none"
                       style={styleHr}
                    />
                    }
                    {more &&
                    <Link
                        to={subCategoryLink ? moreLink : (tabs ? `/${mode}/` : moreLink)}
                        className={"d-flex justify-content-end More-button mx-2"}
                    >
                        {__("more")}
                    </Link>
                    }
                    {IconSubject &&
                    <div className="d-none d-lg-flex justify-content-between DivParentIcon">
                        <button
                            onClick={onClickFalse}
                            className="
                             d-flex
                             justify-content-center
                             align-items-center
                             IconSubject cursor-p"
                        >
                            <Icon type="bars"/>
                        </button>
                        <button
                            onClick={onClickTrue}
                            className="
                             d-flex
                             justify-content-center
                             align-items-center
                             IconSubject cursor-p"
                        >
                            <Icon type="table"/>
                        </button>
                    </div>
                    }
                    {IconCategory &&
                    <div
                        className="d-flex justify-content-between
                        DivParentCategoryIcon my-2 my-lg-0"
                    >
                        <button
                            onClick={onClickShow}
                            className="IconSubject cursor-p"
                            name="book"
                        >
                            <Icon type="bars"/>
                        </button>
                        <button
                            onClick={onClickShow}
                            className="IconSubject cursor-p"
                            name="bars"
                        >
                            <Icon type="bars"/>
                        </button>
                        <button
                            onClick={onClickShow}
                            className="IconSubject cursor-p d-none d-lg-flex"
                            name="table"
                        >
                            <Icon type="table"/>
                        </button>
                    </div>
                    }
                </div>
            </div>
        </Row>
    )
}

TabSection.propTypes = {
    title: PropTypes.string,
    moreLink: PropTypes.string,
    tabs: PropTypes.array,
    onTabChange: PropTypes.func,
    defaultSelectedButton: PropTypes.string,
    IconSubject: PropTypes.bool,
    className: PropTypes.string,
    classNameSearchPage: PropTypes.bool,
    styleHr: PropTypes.object,
    styleHejab: PropTypes.object,
};
