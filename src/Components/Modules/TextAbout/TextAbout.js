import React from 'react';
import {SpinLoading} from "../../Module";
import './TextAbout.css';

export default function TextAbout(props) {
    const {data, loading} = props;
    return (
        <SpinLoading
            loading={loading}
            mode="withoutBox"
        >
            <div className="text-TextAbout font-F-Gothic my-5 p-lg-0 px-3">
                <p
                    dangerouslySetInnerHTML=
                        {{
                            __html: data && data.siteInfo && data.siteInfo
                        }}
                />
            </div>
        </SpinLoading>
    );
}
