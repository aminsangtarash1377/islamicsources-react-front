import React, {useEffect, useState} from 'react';
import {Avatar, Breadcrumb, Button, Dropdown, Icon, Menu} from "antd";
import {CustomImage, Login} from "../../Module"
import BTNHeaderSubmit from '../../../images/Upload.svg';
import DownHeaderSubmit from '../../../images/Down Icon.svg';
import {Link, useLocation, useHistory, withRouter} from "react-router-dom";
import {HashLink} from "react-router-hash-link";
import connect from "react-redux/es/connect/connect";
import {__} from "../../../global";
import {Authenticated} from "../../../actions";
import defaultLogo from "../../../images/logo.png";
import {deleteCookie} from "../../../appConfig";
import './Header.css';

const {SubMenu} = Menu;
// const svg =
//     <svg
//         style={{width:"10%"}}
//         id="Layer_1"
//         data-name="Layer 1"
//         xmlns="http://www.w3.org/2000/svg"
//         viewBox="0 0 351 353"
//     >
//         <path  style={{fill: "#fdfefe"}}
//               d="M7.73,144.59c0-67.3,49-123.56,114.43-132.68,72.51-10.09,140,40.77,149.94,113.3,4.45,32.48-2.24,62.78-20.13,90.32-2.73,4.21-2.09,6.24,1.21,9.51q41.1,40.68,81.86,81.74c10.44,10.48,9.88,24.92-1.07,33.75a21.44,21.44,0,0,1-27.44,0,91.87,91.87,0,0,1-6.69-6.41q-39.54-39.51-79-79.1c-2.57-2.59-4.22-2.64-7.21-.68C131.69,307.86,22.47,258.53,9,162A94,94,0,0,1,7.73,144.59Zm132.72,100.5A101.67,101.67,0,0,0,242.09,143.73c.08-56-45.32-101.63-101.24-101.75a101.56,101.56,0,1,0-.4,203.11Z"/>
//         <path style={{fill: "#1ca9b1"}}
//               d="M140.45,245.09A101.56,101.56,0,1,1,140.85,42c55.92.12,101.32,45.76,101.24,101.75A101.67,101.67,0,0,1,140.45,245.09Z"/>
//     </svg>;

function Header(props) {
    const location = useLocation();
    const [visible, setVisible] = useState(false);
    const [MenuItem, setMenuItem] = useState("");

    useEffect(() => {
        setMenuItem();
        data && data.available_menus.map((d) => {
                const split = location.pathname.split("/");
                if (split.filter(val => val === d.url.split("/")[0]).length > 0) {
                    setMenuItem(d.url.split("/")[0])
                }
                if (d.url === "Home/" && split.filter(val => val === "").length === 2) {
                    setMenuItem("Home")
                }
            }
        );
    }, []);

    async function logout(history) {
        await deleteCookie("ISAccessUserToken");
        await props.dispatch(Authenticated(false));
        if (history.location.pathname.toLowerCase() === "/profile")
            await history.push('/');
    }

    const {data, userDetails, isAuthorized} = props;
    const user_avatar = isAuthorized ? isNaN(parseInt(userDetails.avatar)) ? userDetails.avatar : "" : "";
    const history = useHistory();
    let splitLocation = location.pathname.split("/");
    const menu = (
        <Menu>
            <Menu.Item>
                <Link to="/profile">
                    <Icon type="user" className="mx-2"/>{__('Profile')}
                </Link>
            </Menu.Item>
            <Menu.Item>
                <p className="my-0" onClick={() => logout(history)}>
                    <Icon type="logout" className="mx-2"/>{__('Logout')}
                </p>
            </Menu.Item>
        </Menu>
    );
    splitLocation = splitLocation.filter(val => val !== "");

    function breadCrumbLinkProcessor(location, slug) {
        return location.pathname.split(slug)[0] + slug
    }

    return (
        <React.Fragment>
            <div
                className="Header d-flex align-items-center justify-content-between position-fixed"
            >
                <div className="d-flex align-items-center w-100">
                    <Link to="/">
                        <img
                            width={85}
                            src={data ? data.customLogo : defaultLogo}
                            alt="islamic sources"
                        />
                    </Link>
                    <Menu
                        className="
                        Header-menu
                        d-lg-flex
                        d-none
                        "
                        mode="horizontal"
                        selectedKeys={[MenuItem]}
                        style={{lineHeight: '64px', width: "85%"}}
                    >
                        {data && data.available_menus.map((d, i) =>
                            d.target === "1" &&
                            <Menu.Item onClick={e => setMenuItem(e.key)} key={d.url.split("/")[0]}>
                                <Link to={`/${d.url === "Home/" ? "" : d.url.split("/")[0]}`}>
                                    {d.name}
                                </Link>
                            </Menu.Item>
                        )}
                    </Menu>
                </div>
                <div className="d-flex align-items-center">
                    {
                        isAuthorized ?
                            <div className="d-flex align-items-center">
                                <HashLink
                                    scroll={el => el.scrollIntoView({behavior: 'smooth', block: 'center'})}
                                    to="/profile#submit_post"
                                    className="mx-1"
                                >
                                    <button
                                        className="
                                            b-flat
                                            btn
                                            btn-header-submit
                                            d-none
                                            d-lg-flex
                                            align-items-center
                                            justify-content-between"
                                    >
                                        <CustomImage url={BTNHeaderSubmit}/>
                                        <span>{__('Submit Article')}</span>
                                    </button>
                                </HashLink>
                                <Dropdown overlay={menu} placement="bottomCenter">
                                    <Avatar
                                        className="d-flex align-items-center justify-content-center mx-3 cursor-p avatar-user"
                                        src={user_avatar !== "" ? user_avatar : ""}
                                        icon={user_avatar !== "" ? "user" : ""}
                                    />
                                </Dropdown>
                            </div>
                            :
                            <>
                                <Button
                                    key={1}
                                    className="Header-menu login-header background-login text-bold"
                                    onClick={() => setVisible(true)}
                                >
                                    {__('Login')}
                                </Button>
                                <Login
                                    visible={visible}
                                    onCancel={() => setVisible(false)}
                                    onClick={() => setVisible(false)}
                                />
                            </>
                    }
                    <a href={data ? data.languageBtn.link : "/"}>
                        <div
                            className="
                                Header-menu
                                d-none
                                d-lg-flex
                                flex-column
                                align-items-center
                                cursor-p
                            "
                        >
                            <span
                                className="
                                    text-bold
                                    font-F-Gothic
                                "
                                style={{fontSize: ".85rem", marginBottom: ".2rem"}}
                            >
                                {data && data.languageBtn.name}
                            </span>
                            <CustomImage
                                className="lang-Header-icon position-relative"
                                url={DownHeaderSubmit}
                            />
                        </div>
                    </a>
                    {/*<Link to="/SearchPage">*/}
                    {/*    {svg}*/}
                    {/*</Link>*/}
                    <Menu
                        className="
                            Header-menu
                            d-flex
                            d-lg-none
                            menu-lg-flex
                        "
                        mode="horizontal"
                    >
                        <SubMenu
                            key="sub1"
                            title={
                                <Icon className="" type="menu"/>
                            }
                            className="submenu-res"
                        >
                            {data && data.available_menus.map((d) =>
                                d.target === "1" &&
                                <Menu.Item onClick={(e) => setMenuItem(e.key)} key={d.url.split('/')[0]}>
                                    <Link to={`/${d.url === "Home/" ? "" : d.url.split("/")[0]}/`}>
                                        {d.name}
                                    </Link>
                                </Menu.Item>
                            )}
                        </SubMenu>
                    </Menu>
                </div>
            </div>
            <Breadcrumb
                className={props.RtlDir
                    ?
                    "py-3 py-md-4 Breadcrumb font-F-Gothic d-flex"
                    :
                    "py-3 py-md-4 Breadcrumb font-F-Gothic"
                }
            >
                <Breadcrumb.Item>
                    <Link to="/">
                        {__("Home")}
                    </Link>
                </Breadcrumb.Item>
                {splitLocation.map((d, i) =>
                    d === splitLocation.slice(-1)[0] ?
                        <Breadcrumb.Item key={i}>
                            {__(d)}
                        </Breadcrumb.Item>
                        :
                        <Link to={`${breadCrumbLinkProcessor(location, d)}`}>
                            <Breadcrumb.Item>
                                {__(d)}
                            </Breadcrumb.Item>
                        </Link>
                )}
            </Breadcrumb>
        </React.Fragment>
    );
}

const mapStateToProps = states => (
    {
        RtlDir: states.Rtl,
        userDetails: states.userDetails,
        isAuthorized: states.authorization
    }
);
export default withRouter(connect(mapStateToProps)(Header));