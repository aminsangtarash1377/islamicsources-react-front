import React, {Component} from 'react';
import {Button, Form, Input, message, Modal} from "antd";
import {__} from "../../../global";
import PropTypes from "prop-types";
import axios from "axios";
import {defaultWPUrl, ISAccessUserToken} from "../../../config";
import {SpinLoading} from "../../Module";

class ChangePasswordModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading : false,
            changePasswordInformation: {
                password : "",
                confirmPassword : "",
            }
        };
    }

    updatePasswordFields = target => {
        let {changePasswordInformation} = this.state;
        changePasswordInformation[target.target.name] = target.target.value;
    };

    updatePassword = async (val, input) => {
        const {setVisible, history} = this.props;
        val.preventDefault();
        this.setState({loading : true});
        await axios.post(
            defaultWPUrl + '/users/password/current',
            {
                password : input.password === "" ? null : input.password,
                confirm_password : input.confirmPassword === "" ? null : input.confirmPassword,
            },
            {headers : {Authorization : ISAccessUserToken}}
        )
            .then(async res => {
                if(res.status === 200){
                    message.success(__('Your password successfully changed'));
                    setVisible(false);
                    this.setState({loading : false});
                    history.push('/profile')
                }
            })
            .catch(async res => {
                this.setState({loading : false});
                if(res.response && res.response.data.message){
                    message.error(__(res.response.data.message))
                }
            })
    };

    static propTypes = {
        visible : PropTypes.bool.isRequired,
        setVisible : PropTypes.isRequired
    };

    render() {
        const {loading, changePasswordInformation} = this.state;
        const {visible, setVisible} = this.props;
        return (
            <Modal
                centered
                width={400}
                visible={visible}
                onCancel={() => setVisible(false)}
                onClick={() => setVisible(false)}
                footer={
                    <SpinLoading
                        loading={loading}
                        mode="loading"
                    >
                        <Button
                            htmlType="submit"
                            className="
                                signIn-ButtonDown
                                font-F-Gothic
                                text-white"
                            onClick={val => this.updatePassword(val, changePasswordInformation)}
                        >
                            {__('Update')}
                        </Button>
                    </SpinLoading>
                }
            >
                <Form onSubmit={val => this.updatePassword(val, changePasswordInformation)}>
                    <Form.Item>
                        <Input
                            type="Password"
                            placeholder={__("Password")}
                            className="FormEditPassword-Input"
                            onChange={this.updatePasswordFields}
                            name="password"
                        />
                    </Form.Item>
                    <Form.Item>
                        <Input
                            type="Password"
                            placeholder={__("Confirm Password")}
                            className="FormEditPassword-Input"
                            onChange={this.updatePasswordFields}
                            name="confirmPassword"
                        />
                    </Form.Item>
                </Form>
            </Modal>
        );
    }
}

export default ChangePasswordModal;