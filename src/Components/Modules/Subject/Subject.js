import React, {useState} from 'react';
import {Avatar, Col, message, notification, Rate} from "antd";
import {Link, withRouter} from "react-router-dom";
import {CustomImage} from "../CustomImage";
import gift from "../../../images/gift.svg";
import one from "../../../images/rate/Point A-A.svg";
import two from "../../../images/rate/Point A-B.svg";
import three from "../../../images/rate/Point A-C.svg";
import four from "../../../images/rate/Point A-D.svg";
import five from "../../../images/rate/Point A-E.svg";
import {Parent} from "../../../actions/index";
import connect from "react-redux/es/connect/connect";
import axios from "axios";
import {defaultRahUrl, setRateByUser} from "../../../config";
import {__} from "../../../global";
import './Subject.css';


function Subject(props) {

    const [rate, SetRating] = useState(true);
    const [UnitRate, SetUnitRate] = useState(props.data &&
        props.data.topicfileds.topicRate &&
        props.data.topicfileds.topicRate / 100);
    const [imageLevel, SetImageLevel] = useState(one);
    const {data} = props;

    function notificate(Title, Message, Type) {
        notification[Type]({
            message: Title,
            description: Message,
            duration: 3
        });
    }

    function handleRate(value) {
        switch (value) {
            case 1:
                SetUnitRate(1);
                SetImageLevel(one);
                break;
            case 2:
                SetUnitRate(2);
                SetImageLevel(two);
                break;
            case 3:
                SetUnitRate(3);
                SetImageLevel(three);
                break;
            case 4:
                SetUnitRate(4);
                SetImageLevel(four);
                break;
            case 5:
                SetUnitRate(5);
                SetImageLevel(five);
                break;
        }
        SetRating(!rate);
    }

    async function SetRate(rate) {
        if(setRateByUser(data && data.termTaxonomyId)) {
            await axios.post(defaultRahUrl + `/rate/?type=tax&id=${data && data.termTaxonomyId}&rate=${rate}`)
                .then(result => {
                    if (result.data) {
                        handleRate(result.data && Math.round(result.data.rate));
                        notificate("پیام ادمین", ` امتیاز${rate}  با موفقیت ثبت شد `, "success")
                    } else {
                        handleRate(1);
                    }
                })
        }
        else{
            message.warning(__('You rate this post once'));
        }
    }

    return (
        <Col
            span={7}
            className="Subject cursor-p mb-4 mt-3 d-flex
                       justify-content-center align-items-center"
            onClick={() => props.dispatch(Parent(data && data.topicId))}
        >
            <Col span={13} lg={13} className="d-flex justify-content-center">
                <Link to={`/Subjects/${data && data.slug}/`}>
                    <Avatar
                        size={120}
                        src={data && data.topicfileds &&
                        data.topicfileds.topicImage && data.topicfileds.topicImage.sourceUrl}
                    />
                </Link>
            </Col>
            <Col span={11} lg={11}>
                <Link to={`/Subjects/${data && data.slug}/`}>
                    <div className="mb-2 titleSubject">
                        <p className="mb-0 font-F-Gothic">
                            {data.name}
                        </p>
                    </div>
                    <div className="mb-2 d-flex flex-column">
                        <p className="mb-0 TextSubject font-F-Gothic">
                            {__("Book")}: {data && data.booksCount && data.booksCount}
                        </p>
                        <p className="mb-0 TextSubject font-F-Gothic">
                            {__("Article")}: {data && data.articlesCount && data.articlesCount}
                        </p>
                    </div>
                </Link>
                {rate === true ?
                    <div className="d-flex flex-row DivRateAndGift">
                        <CustomImage
                            url={imageLevel}
                            onClick={() => SetRating(!rate)}
                            className="SubjectRateAndGift"
                        />
                        <CustomImage
                            url={gift}
                            className="SubjectRateAndGift mx-4"
                        />
                    </div>
                    :
                    <Rate
                        defaultValue={UnitRate}
                        onChange={async (e) => {
                            await SetRate(e);
                        }}
                        character={
                            <div
                                className="position-relative "
                                style={{
                                    width: "1rem",
                                    height: "1rem",
                                }}
                            >
                                <div
                                    style={{
                                        width: ".9rem",
                                        height: ".9rem",
                                        background: "#b3b3b3",
                                        borderRadius: "2px"
                                    }}
                                    className="RatingSubject position-absolute"
                                />
                                <div
                                    style={{
                                        width: ".9rem",
                                        height: ".9rem",
                                        background: "#b3b3b3",
                                        transform: "rotate(45deg)",
                                        borderRadius: "2px"
                                    }}
                                    className="RatingSubject position-absolute"
                                />
                            </div>
                        }
                        allowHalf={false}
                    />
                }
            </Col>
        </Col>
    )
}

const mapStateToProps = states => (
    {
        Parent: states.Parent
    }
);
export default withRouter(connect(mapStateToProps)(Subject));