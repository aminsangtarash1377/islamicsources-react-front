import React from 'react';
import connect from "react-redux/es/connect/connect";
import {withRouter} from "react-router-dom";
import './MediaBox.css';

function MediaBox(props) {

    const {src,title} = props;

    return (
        <div
            className="
            d-flex flex-column
            justify-content-center
            align-items-center ParentMediaBox"
        >
            <div className="ParentImageMediaBox">
                <img src={src} alt=""/>
            </div>
            <h3 className="my-3 font-F-Gothic TitleMediaBox">{title}</h3>
        </div>
    )
}

const mapStateToProps = states => (
    {
        RtlDir: states.Rtl,
        Single: states.SingleInCategoriesPageTitle,
        SingleSlug: states.SingleInCategoriesPageSlug
    }
);
export default withRouter(connect(mapStateToProps)(MediaBox));