import React, {useState} from 'react';
import {Col, Input, Button, message} from "antd";
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import {defaultWPUrl, ISAccessUserToken} from "../../../config";
import {SpinLoading} from "../../Module";
import {__} from "../../../global";
import axios from "axios";
import './Contact.css';


function Contact(props) {
    const [loading, setLoading] = useState(false);
    let [contact_information] = useState({
        firstName: "",
        lastName: "",
        EmailOrPhone: "",
        Country: "",
        Message: ""
    });

    /**
     * @return {boolean}
     */
    async function PostInformation() {
        if (
            contact_information.firstName === "" || contact_information.lastName === "" ||
            contact_information.EmailOrPhone === "" || contact_information.Country === "" ||
            contact_information.Message === ""
        ) {
            await message.error("Please fill in all fields");
            return false;
        }
        return true;
    }

    async function PostInformationAbout(prevent) {
        prevent.preventDefault();
        setLoading(true);
        if(await PostInformation())
        {
            await axios.post(defaultWPUrl + "/users/post", {
                    "term": "opinion",
                    "post_title": contact_information.firstName + " " + contact_information.lastName,
                    "post_content": contact_information.Message,
                    "writer": contact_information.EmailOrPhone,
                    "subject": contact_information.Country
                },
                {headers: {Authorization: ISAccessUserToken}})
                .then(result => {
                    if (result.status === 200) {
                        message.success(__("Your comment has been successfully submitted"))
                    }
                })
                .catch(res => {
                    if (res.response.data && res.response.data.message)
                        message.error(__(res.response.data.message))
                });
        }
        setLoading(false);
    }

    function setValue(value) {
        contact_information[value.target.name] = value.target.value;
    }

    const {TextArea} = Input;
    return (
        <form onSubmit={val => PostInformationAbout(val)} className="w-100 mb-5 paddingInResponsive">
            <SpinLoading
                loading={loading}
                mode="loading"
            >
                <div className="d-flex align-items-center">
                    <h4
                        className="title-Location mb-0 font-F-Gothic"
                        style={{minWidth: "6rem"}}
                    >
                        {__("Contact us")}
                    </h4>
                    <hr className="w-100 GaryBorder mx-4"/>
                </div>
                <div
                    className="
                        w-100
                        mt-5
                        mb-lg-0 mb-3
                        font-F-Gothic
                        d-flex
                        flex-lg-row
                        flex-column-reverse
                        justify-content-lg-between"
                >
                    <Col lg={12} className="d-flex">
                        <TextArea
                            placeholder={__("Massage")}
                            rows={4}
                            className="pt-3 InputMassage-About"
                            name="Message"
                            onChange={setValue}
                        />
                    </Col>
                    <Col
                        lg={11}
                        className="m-h-12 d-flex flex-column justify-content-between"
                    >
                        <div className="d-flex flex-lg-column flex-row">
                            <Input
                                className="Inputs-About mb-lg-3"
                                placeholder={__("First name")}
                                name="firstName"
                                type="text"
                                onChange={setValue}
                            />
                            <Input
                                className={
                                    props.RtlDir ?
                                        "Inputs-About mr-lg-0 mr-4"
                                        :
                                        "Inputs-About ml-lg-0 ml-4"
                                }
                                placeholder={__("Last name")}
                                type="text"
                                name="lastName"
                                onChange={setValue}
                            />
                        </div>
                        <div className="mb-lg-0 mb-3 d-flex flex-lg-column flex-row">
                            <Input
                                className={
                                    props.RtlDir ?
                                        "Inputs-About mb-lg-3 ml-lg-0 ml-4"
                                        :
                                        "Inputs-About mb-lg-3 mr-lg-0 mr-4"
                                }
                                placeholder={__("Email Or Phone")}
                                type="email"
                                name="EmailOrPhone"
                                onChange={setValue}
                            />
                            <Input
                                className="Inputs-About mr-lg-3 mr-0 d-lg-none d-block"
                                placeholder={__("Country")}
                                type="text"
                                name="Country"
                                onChange={setValue}
                            />
                            <div className="w-100 d-lg-flex d-none">
                                <Input
                                    className={
                                        props.RtlDir ?
                                            "Inputs-About ml-lg-3 ml-0"
                                            :
                                            "Inputs-About mr-lg-3 mr-0"
                                    }
                                    placeholder={__("Country")}
                                    type="text"
                                    name="Country"
                                    onChange={setValue}
                                />
                                <Button
                                    className="SendButton"
                                    htmlType="submit"
                                    onClick={val => PostInformationAbout(val)}
                                >
                                    {__("Send")}
                                </Button>
                            </div>
                        </div>
                    </Col>
                </div>
                <div className="d-flex justify-content-center">
                    <Button
                        className="SendButton d-block d-lg-none"
                        onClick={val => PostInformationAbout(val)}
                    >
                        {__("Send")}
                    </Button>
                </div>
            </SpinLoading>
        </form>
    );
}

const mapStatsToProps = states => (
    {
        RtlDir: states.Rtl
    }
);

export default withRouter(connect(mapStatsToProps)(Contact))