import React from 'react';
import './LocationAndContect.css';
import {Col, Row} from "antd";
import WhatsApp from "../../../images/publishers/whatsapp.svg";
import EmailAddress from "../../../images/publishers/message.svg";
import location from "../../../images/publishers/Location.svg";
import instagram from "../../../images/Instagram A.svg";
import Twiter from "../../../images/Twitter A.svg";
import ww from "../../../images/publishers/ww.svg";
import telegram from "../../../images/Telegram A.svg";
import faceBook from "../../../images/facebook B.svg";
import {CustomImage} from "../CustomImage";
import connect from "react-redux/es/connect/connect";
import {withRouter} from "react-router-dom";
import Map from 'pigeon-maps';
import Marker from 'pigeon-marker';
import Overlay from 'pigeon-overlay';
import ph from "../../../images/Phone.svg";
import {__} from "../../../global";


function LocationAndContect(props) {
    const {data} = props;

    const icons2 = [
        {name: instagram, color: "#c32aa3"},
        {name: Twiter, color: "#55acee"},
        {name: telegram, color: "#0088cc"},
        {name: ww, color: "#F27461"},
        {name: faceBook, color: "#1877f2"}
    ];

    function setColor(target, e, hover) {
        let paths = target.currentTarget.querySelectorAll("path");
        if (hover) {
            for (let i = 0; i < paths.length; i++) {
                paths[i].style.fill = e;
            }
        } else {
            for (let i = 0; i < paths.length; i++) {
                paths[i].style.fill = "#a8a8a8";
            }
        }
    }
    return (
        data &&
        <Row type="flex"
             justify="space-between"
             style={{minHeight: "60vh"}}
             className="mb-3 mb-md-5">
            <Col span={11} lg={11}>
                <div className="d-flex align-items-center">
                    <h4 className="title-Location mb-0 font-F-Gothic">
                        {__("Contact us")}
                    </h4>
                    <hr className="w-100 GaryBorder mx-4"/>
                </div>
                <div className="w-100 mt-5 pt-3 mx-5 px-4 d-flex flex-column">
                    {data && data.centerfields && data.centerfields.centerPhone &&
                        <div
                            className="d-flex justify-content-start ColumnIcons
                                       align-items-center"
                            style={{minWidth: "18rem"}}
                        >
                            <CustomImage
                                url={ph}
                                style={{width: "2.3rem"}}
                                className={props.RtlDir ? "mb-3 ml-4" : "mb-3 mr-4"}
                                alt="Our communication ways"
                                onMouseOver={target => setColor(target, '#f16e00', true)}
                                onMouseLeave={target => setColor(target, '#f16e00', false)}
                            />
                            <p className="textIcon-Location">
                                {data && data.centerfields && data.centerfields.centerPhone}
                            </p>
                        </div>
                    }
                    <div
                        className="d-flex justify-content-start ColumnIcons align-items-center"
                        style={{minWidth: "18rem"}}
                    >
                        <CustomImage
                            url={WhatsApp}
                            style={{width: "2.3rem"}}
                            className={props.RtlDir ? "mb-3 ml-4" : "mb-3 mr-4"}
                            alt="Our communication ways"
                            onMouseOver={target => setColor(target, '#25d366', true)}
                            onMouseLeave={target => setColor(target, '#25d366', false)}
                        />
                        <p className="textIcon-Location">
                            00982537745897
                        </p>
                    </div>
                    {data && data.centerfields && data.centerfields.centerMail &&
                    <div
                        className="d-flex justify-content-start ColumnIcons align-items-center"
                        style={{minWidth: "18rem"}}
                    >
                        <CustomImage
                            url={EmailAddress}
                            style={{width: "2.3rem"}}
                            className={props.RtlDir ? "mb-3 ml-4" : "mb-3 mr-4"}
                            alt="Our communication ways"
                            onMouseOver={target => setColor(target, "#00a6e3", true)}
                            onMouseLeave={target => setColor(target, "#00a6e3", false)}
                        />
                        <p className="textIcon-Location">
                            {data && data.centerfields && data.centerfields.centerMail}
                        </p>
                    </div>
                    }
                    {data && data.centerfields && data.centerfields.centerAddress &&
                    <div
                        className="d-flex justify-content-start ColumnIcons align-items-center"
                        style={{minWidth: "18rem"}}
                    >
                        <CustomImage
                            url={location}
                            style={{width: "2.3rem"}}
                            className={props.RtlDir ? "mb-3 ml-4" : "mb-3 mr-4"}
                            alt="Our communication ways"
                            onMouseOver={target => setColor(target, "#822F2B", true)}
                            onMouseLeave={target => setColor(target, "#822F2B", false)}
                        />
                        <p className="textIcon-Location">
                            {data && data.centerfields && data.centerfields.centerAddress}
                        </p>
                    </div>
                    }
                </div>
                <div className="d-flex w-100 mx-5 px-4 DownIcons">
                    {icons2.map((x, i) => (
                        <CustomImage
                            url={x.name}
                            key={i}
                            style={{width: "2.3rem"}}
                            onMouseOver={target => setColor(target, x.color, true)}
                            onMouseLeave={target => setColor(target, x.color, false)}
                        />
                    ))}
                </div>
            </Col>
            <Col span={11} lg={11}>
                <div className="d-flex justify-content-between align-items-center">
                    <h4 className="title-Location mb-0 font-F-Gothic">
                        {(__("Location"))}
                    </h4>
                    <hr className={`w-100 GaryBorder ${props.RtlDir ? "mr-4" : "ml-4"}`}/>
                </div>
                <div
                    className="w-100 DivParentMap d-flex
                    justify-content-center align-items-end"
                >
                    <Map
                        center={[data && data.centerfields.centerLocation &&
                        data.centerfields.centerLocation.latitude &&
                        data.centerfields.centerLocation.latitude,
                            data && data.centerfields.centerLocation &&
                            data.centerfields.centerLocation.longitude &&
                            data.centerfields.centerLocation.longitude]}
                        zoom={15}
                    >
                        <Marker
                            anchor={[data && data.centerfields.centerLocation &&
                            data.centerfields.centerLocation.latitude &&
                            data.centerfields.centerLocation.latitude,
                                data && data.centerfields.centerLocation &&
                                data.centerfields.centerLocation.longitude &&
                                data.centerfields.centerLocation.longitude]}
                            payload={1}
                            // onClick={({event, anchor, payload}) => {
                            // }}
                        />

                        <Overlay
                            anchor={[data && data.centerfields.centerLocation &&
                            data.centerfields.centerLocation.latitude &&
                            data.centerfields.centerLocation.latitude,
                                data && data.centerfields.centerLocation &&
                                data.centerfields.centerLocation.longitude &&
                                data.centerfields.centerLocation.longitude]}
                            offset={[120, 79]}
                        >
                        </Overlay>
                    </Map>
                </div>
            </Col>
        </Row>
    );
}

const mapStateToProps = states => (
    {
        RtlDir: states.Rtl
    }
);
export default withRouter(connect(mapStateToProps)(LocationAndContect));

