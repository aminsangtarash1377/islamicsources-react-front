import React from 'react';
import './ResourceStatistics.css';

export default function ResourceStatistics(props) {
    return (
        <div className="w-100 p-1">
            <h4 className={`ResourceStatisticsTitle ${props.text === 2 
                ? `text-right` : `text-left`}` }
            >{props.title}</h4>
            <hr className="MidLine"/>
            <h4 className={`ResourceStatisticsNumber ${props.text === 2 
                ? `text-right` : `text-left`}`}
            >{props.number}</h4>
        </div>
    );
}