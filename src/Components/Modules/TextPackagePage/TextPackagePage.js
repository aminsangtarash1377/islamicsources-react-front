import React from 'react';
import {Col, Empty} from "antd";
import {SpinLoading} from "../../Module"
import "./TextPackagePage.css";

export default function TextPackagePage(props) {
    const {data, loading} = props;

    return (
        <>
            {loading ?
                <SpinLoading
                    loading={loading}
                    mode="withoutBox"
                />
                :
                <>
                    {data ?
                        <Col
                            span={22}
                            lg={10}
                            className="my-5 font-F-Gothic d-flex flex-column justify-content-start"
                        >
                            <h5 className="mb-3 title-PackagePage">
                                {data && data.title && data.title}
                            </h5>
                            <p
                                className="text-PackagePage"
                                dangerouslySetInnerHTML={{__html: data && data.content}}
                            />
                        </Col>
                        :
                        <div
                            className="
                                w-100
                                d-flex
                                justify-content-center
                                align-items-center"
                        >
                            <Empty/>
                        </div>
                    }
                </>
            }
        </>
    );
}
