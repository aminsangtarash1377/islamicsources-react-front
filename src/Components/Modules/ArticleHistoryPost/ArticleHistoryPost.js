import React, {Component} from 'react';
import {Col, Row} from "antd";
import PropTypes from "prop-types";
import './ArticleHistoryPost.css';

class ArticleHistoryPost extends Component {
    static propTypes = {
        data : PropTypes.object.isRequired
    };
    render() {
        const {data} = this.props;
        return (
            <Row type="flex" className="w-100 ArticleHistoryPost p-3" style={{height : "4.5rem"}}>
                <Col span={18} className="d-flex flex-column align-items-start justify-content-center h-100">
                    <h4 className="font-F-Gothic m-0" style={{fontSize : "1.2rem"}}>{data.post_title}</h4>
                    <p className="font-F-Gothic m-0" style={{color:"#b3b3b3",fontSize: ".9rem"}}>{data.writer}</p>
                </Col>
                <Col span={6} className="d-flex align-items-center justify-content-end h-100">
                    <span style={{color:"#b3b3b3",fontSize: ".7rem"}}>{data.date}</span>
                </Col>
            </Row>
        );
    }
}

export default ArticleHistoryPost;