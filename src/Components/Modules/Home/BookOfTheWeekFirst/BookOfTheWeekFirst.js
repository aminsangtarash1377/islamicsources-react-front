import React from 'react';
import {Col, Row} from "antd";
import {__} from "../../../../global";
import {Link, withRouter} from "react-router-dom";
import defaultImage from "../../../../images/defaultBook.png";
import connect from "react-redux/es/connect/connect";
import './BookOfTheWeekFirst.css';

function BookOfTheWeekFirst(props) {

    const {data} = props;

    return (
        <Row
            type="flex"
            className="
            my-2
            my-md-5
            border
            justify-content-between
            align-items-center
            font-F-Gothic"
        >
            <Col
                xs={8}
                md={5}
                lg={3}
                className="ParentImage-BookOfTheWeek h-100"
            >
                <img
                    src={data && data.attachment ? data.attachment : defaultImage}
                    className="w-100 mh-100"
                    alt={__("Book of the week")}
                />
            </Col>
            <Col xs={16} md={19} lg={8} className="d-flex flex-column pr-2 pr-lg-0">
                <h3 className="mb-1 title-BookOfTheWeek">
                    {__("Book of the week")}
                </h3>
                <p className="mb-1 name-BookOfTheWeek">
                    {data && data.post_title}
                </p>
                <p className="mb-0 Author-BookOfTheWeek">
                    <span className="px-1" style={{fontWeight: "bold"}}>
                        {__("Author")}:
                    </span>
                    {data &&
                    data.meta_fields &&
                    data.meta_fields.act_author &&
                    data.meta_fields.act_author[0] &&
                    data.meta_fields.act_author[0].post_title ?
                        data.meta_fields.act_author[0].post_title
                        :
                        "---"
                    }
                </p>
                <p className="mb-2 Author-BookOfTheWeek">
                    <span className="px-1" style={{fontWeight: "bold"}}>
                        {__("Publisher")}:
                    </span>
                    {data &&
                    data.meta_fields &&
                    data.meta_fields.act_publisher &&
                    data.meta_fields.act_publisher.post_title ?
                        data.meta_fields.act_publisher.post_title
                        :
                        "---"
                    }
                </p>
                <div className="d-flex align-items-center">
                    <span className={`points-Article ${
                        props.RtlDir ? "ml-2" : "mr-2"}`}>
                        {data &&
                        data.meta_fields &&
                        isNaN(Math.round(data.meta_fields.rate)) ?
                            "-"
                            :
                            Math.round(data.meta_fields.rate)
                        }
                    </span>
                    <span className="count-BookOfTheWee">
                        {data &&
                        data.meta_fields &&
                        data.meta_fields.view ?
                            data.meta_fields.view
                            :
                            "0 " + " "
                        }{__("View")}
                    </span>
                </div>
            </Col>
            <Col span={20} lg={12} className="d-none d-lg-block">
                <p className="text-BookOfTheWee p-4">
                    {data && data.post_excerpt}
                    <Link to={`/Books/${data && data.post_name}`}
                          className="font-F-Gothic"
                          style={{color: "#0dc3c5", fontWeight: "bold"}}>
                        {" "} {__("More")} ...
                    </Link>
                </p>
            </Col>
        </Row>
    );
}

const mapStateToProps = states => (
    {
        RtlDir: states.Rtl,
    }
);
export default withRouter(connect(mapStateToProps)(BookOfTheWeekFirst));
