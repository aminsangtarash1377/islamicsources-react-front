import React from 'react';
import {Avatar, Col, Row} from "antd";
import {Link} from "react-router-dom";
import {__} from "../../../../global";
import './AuthorOfTheWeek.css';


export default function AuthorOfTheWeek(props) {

    const {data} = props;
    return (
        <Row
            type="flex"
            justify="center"
            className="mt-3 mt-md-5 d-flex flex-row border align-items-center py-3"
            style={{minHeight: "26vh"}}
        >
            <Col
                span={8}
                lg={3}
                 className="d-flex justify-content-center justify-content-lg-start"
            >
                <Avatar
                    src={data && data.attachment && data.attachment}
                    icon={!data.attachment && "user"}
                    size={90}
                    className="d-flex justify-content-center align-items-center"
                />
            </Col>
            <Col span={16} lg={19} className="font-F-Gothic">
                <h3 className="title-AuthorOfTheWeek">
                    {__("Author of the Week")}
                </h3>
                <p className="text-AuthorOfTheWeek">
                    <span style={{color: "#4e4e4e", fontWeight: "bold"}}>
                        {data && data.post_title} {""}
                    </span>
                    {data && data.post_excerpt}
                    <Link
                        to={`/Persons/${data && data.post_name}`}
                        style={{color: "#0dc3c5", fontWeight: "bold"}}
                    >
                        {" "} {__("More")} ...
                    </Link>
                </p>
            </Col>
        </Row>
    );
}
