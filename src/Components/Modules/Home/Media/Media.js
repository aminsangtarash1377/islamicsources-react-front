import React, {useEffect, useState} from 'react';
import {Row, Spin} from "antd";
import {Book, TabSection, Video} from "../../../Module";
import PropTypes from 'prop-types';
import {Scrollbars} from "react-custom-scrollbars";
import {loadingItem} from "../../../../config";


Media.propTypes = {
    title: PropTypes.string.isRequired,
    hr: PropTypes.bool,
    defaultSelectedButton: PropTypes.string,
    tabs: PropTypes.array
};

export default function Media(props) {
    const [content, onTabChange] = useState("");
    const {title, hr, more, tabs} = props;

    useEffect(() => {
        tabs && tabs.length > 1 ? onTabChange(tabs[0]) : onTabChange(tabs)
    }, [tabs]);

    return (
        <Row type="flex" className="my-5">
            <TabSection
                tabs={tabs}
                title={title}
                onTabChange={(e) => onTabChange(e)}
                defaultSelectedButton={content && content.title && content.title}
                moreLink={content && content.title}
                hr={hr}
                more={more}
            />
            <Row className="w-100">
                {
                    content ?
                        <div
                            className="mt-4 d-flex flex-lg-row flex-column justify-content-lg-between
                                       align-items-center align-items-center"
                        >
                            {content.mode === "Picture" &&
                            <Scrollbars className="ScrollBooks Scrollbars-Suppress-vertical">
                                {content.data.map((d, i) => (
                                    <Book
                                        key={i}
                                        data={d}
                                        targetLocation="Picture"
                                        popover="Picture"
                                        className="Picture"
                                    />
                                ))}
                            </Scrollbars>
                            }
                            {content.mode === "Video" &&
                            <Scrollbars className="ScrollBooks Scrollbars-Suppress-vertical">
                                {content.data.map((d, i) => (
                                    <Video data={d} key={i}/>
                                ))}
                            </Scrollbars>
                            }
                        </div>
                        :
                        <div
                            className="w-100 d-flex
                            justify-content-center
                            align-items-center spin"
                            style={{minHeight:"40vh"}}
                        >
                            <Spin
                                spinning
                                indicator={
                                    <img
                                        className="w-25 h-auto"
                                        src={loadingItem}
                                        alt="loading"
                                    />
                                }
                            />
                        </div>
                }
            </Row>
        </Row>
    );
}
