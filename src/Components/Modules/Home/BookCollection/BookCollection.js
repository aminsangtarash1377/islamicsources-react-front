import React, {useEffect, useState} from 'react';
import {Empty, Row, Spin} from "antd";
import {Article, Book, TabSection} from "../../../Module";
import {Scrollbars} from 'react-custom-scrollbars';
import PropTypes from "prop-types";
import {loadingItem} from "../../../../config";
import './BookCollection.css';
import {__} from "../../../../global";

BookCollection.propTypes = {
    title: PropTypes.string,
    hr: PropTypes.bool,
    defaultSelectedButton: PropTypes.string,
    tabs: PropTypes.array,
};

export default function BookCollection(props) {
    const [content, onTabChange] = useState(null);

    const {title, hr, more, tabs} = props;

    useEffect(() => {
        tabs && tabs.length > 1 ? onTabChange(tabs[0]) : onTabChange(tabs[0])
    }, [tabs]);

    return (
        <Row
            type="flex"
            justify="space-between"
            className={`pt-4 flex-column align-items-center mt-2 mt-md-4 ${!hr && "inbox"}`}
        >
            <TabSection
                tabs={tabs}
                title={title ? title : ""}
                onTabChange={(e) => {
                    onTabChange(e);
                }}
                defaultSelectedButton={content && content.title && content.title}
                moreLink={content && content.title}
                hr={hr}
                more={more}
            />
            <Row className={`w-100 ${hr ? "mt-3" : "mt-1"}`}>
                {
                    content ?
                        <div
                            className={
                                content.mode !== "article"
                                    ?
                                    "d-flex flex-row mt-3 ParentBook"
                                    :
                                    "d-flex flex-row flex-wrap justify-content-around mt-3"
                            }
                        >
                            {content.mode && content.mode === "book" &&
                            <Scrollbars
                                renderView={props => (
                                    <div {...props} style={{ ...props.style, overflowY: 'hidden' }} />
                                )}
                                className="ScrollBooks Scrollbars-Suppress-vertical"
                            >
                                {content.data && content.data.length !== 0 ? content.data.map((x, i) => (
                                        <Book
                                            key={i}
                                            data={x}
                                            popover="Book"
                                            targetLocation="Books"
                                            className="Book"
                                        />
                                    )) :
                                    <div
                                        className="
                                         w-100
                                         d-flex
                                         justify-content-center
                                         align-items-center"
                                    >
                                        <Empty/>
                                    </div>
                                }
                            </Scrollbars>
                            }
                            {content.mode && content.mode === "article" &&
                            <React.Fragment>
                                {content.data.length !== 0 ? content.data.map((x, i) => (
                                        <Article
                                            key={i}
                                            title={x.title}
                                            slug={x.slug}
                                            link="Articles"
                                            rate={
                                                x.articlefields ?
                                                    (x.articlefields.rate === "INF" || x.articlefields.rate === null)
                                                        ?
                                                        "-"
                                                        :
                                                        x.articlefields.rate
                                                    :
                                                    x.points && x.points
                                            }
                                            view={
                                                x.articlefields ?
                                                    x.articlefields.view &&
                                                    x.articlefields.view
                                                    :
                                                    x.count && x.count
                                            }
                                            author={
                                                x.articlefields ?
                                                    x.articlefields.actAuthor &&
                                                    x.articlefields.actAuthor[0] &&
                                                    x.articlefields.actAuthor[0].title
                                                    :
                                                    x.subTitle && x.subTitle
                                            }
                                        />
                                    ))
                                    :
                                    <div className="
                                    w-100
                                    d-flex
                                    justify-content-center
                                    align-items-center">
                                        <Empty/>
                                    </div>
                                }
                            </React.Fragment>
                            }
                            {content.mode && content.mode === "journal" &&
                            <Scrollbars
                                className="ScrollBooks Scrollbars-Suppress-vertical"
                            >
                                {content.data && content.data.length !== 0 ? content.data.map((x, i) => (
                                        <Book
                                            key={i}
                                            data={x}
                                            popover="Book"
                                            targetLocation="Journals"
                                            className="Book"
                                        />
                                    )) :
                                    <div
                                        className="
                                         w-100
                                         d-flex
                                         justify-content-center
                                         align-items-center"
                                    >
                                        <Empty/>
                                    </div>
                                }
                            </Scrollbars>
                            }
                        </div>
                        :
                        <div
                            className="w-100 d-flex
                                 justify-content-center
                                 align-items-center spin"
                            style={{height: "30vh"}}
                        >
                            <Spin
                                spinning
                                indicator={
                                    <img
                                        className="w-25 h-auto"
                                        src={loadingItem}
                                        alt="loading"
                                    />
                                }
                            />
                        </div>
                }
            </Row>
        </Row>
    );
}