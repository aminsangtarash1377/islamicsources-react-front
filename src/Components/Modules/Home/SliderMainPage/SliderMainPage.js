import React from 'react';
import './SliderMainPage.css';
import {Row} from "antd";
import {BookOfTheWeekSecondSingle} from "../../../Module";
import Slider from "react-slick";


export default function SliderMainPage(props) {

    const {data} = props;
    const settings = {
        dots: false,
        infinite: false,
        speed: 500,
        slidesToShow: 2,
        slidesToScroll: 2,
        initialSlide: 0,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            }
            ]
    };

    return (
        <Row
            type="flex"
            className="my-2 my-md-5 flex-column"
        >
            <div className="d-flex align-items-center">
                <h4 className="title-Location mb-0 font-F-Gothic" style={{minWidth:"14rem"}}>
                    {props.title && props.title}
                </h4>
                <hr className="w-75 GaryBorder"/>
            </div>
            <div className="w-100 SlideParentSocial">
                <Slider
                    className={props.RtlDir ? "dir-rtl" : "dir-ltr"}
                    {...settings}
                >
                    {data &&
                    data.map((x, i) => (
                        <BookOfTheWeekSecondSingle
                            key={i}
                            data={x}
                        />
                    ))
                    }
                </Slider>
            </div>
        </Row>
    );
}
