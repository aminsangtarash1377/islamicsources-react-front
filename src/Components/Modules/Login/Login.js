import React, {useState} from 'react';
import {Button, message, Modal, Row} from "antd";
import {Link, useHistory, withRouter} from "react-router-dom";
import google from '../../../images/google.svg'
import {FancyInput, SpinLoading} from "../../Module";
import {__} from "../../../global";
import {defaultWPUrl, rtlMode, StripHTML} from "../../../config";
import {Authenticated, UserDetailPush} from "../../../actions";
import {baseUrl, setCookie} from "../../../appConfig";
import {connect} from "react-redux";
import axios from "axios";
import './Login.css';


function Login(props) {
    let [showInput, setName] = useState("Login");
    let [loginInformation] = useState({
        email: "",
        password: ""
    });
    let [signUpInformation] = useState({
        first_name: "",
        last_name: "",
        email: "",
        password: "",
        confirmPassword: ""
    });

    const [loading, setLoading] = useState(false);

    function changeInputValue(target, object) {
        object[target.target.name] = target.target.value;
    }

    async function login(val, history, username = "", password = "", for_sign_up = false) {
        val.preventDefault();
        if (!for_sign_up) {
            setLoading(true)
        }
        await axios.post(
            baseUrl + '/wp-json/jwt-auth/v1/token',
            {
                username: username !== "" ? username : loginInformation.email,
                password: password !== "" ? password : loginInformation.password
            },
            {}
        )
            .then(async res => {
                if (res.status === 200) {
                    await setCookie('ISAccessUserToken', res.data.token, 1);
                    if (!for_sign_up) {
                        message.success(__('You logged-in successfully'));
                    }
                    axios.get(`${defaultWPUrl}/users/current`,
                        {headers: {"Authorization": "Bearer " + res.data.token, "Content-Type": "application/json"}})
                        .then(async response => {
                            if (response.status === 200) {
                                await props.dispatch(Authenticated(true));
                                await props.dispatch(UserDetailPush(response.data.data));
                                props.onCancel();
                                window.location.replace('/profile');
                            }
                        });
                }
            })
            .catch(res => {
                setLoading(false);
                if (res.response && res.response.data.message) {
                    message.error(__(StripHTML(res.response.data.message)))
                }
            })

    }

    async function signUp(val, history) {
        val.preventDefault();
        setLoading(true);
        await axios.post(
            defaultWPUrl + '/users/signup',
            {
                first_name: signUpInformation.first_name === "" ? null : signUpInformation.first_name,
                last_name: signUpInformation.last_name === "" ? null : signUpInformation.last_name,
                user_email: signUpInformation.email === "" ? null : signUpInformation.email,
                user_pass: signUpInformation.password === "" ? null : signUpInformation.password,
                confirm_password: signUpInformation.confirmPassword === "" ? null : signUpInformation.confirmPassword,
            },
            {}
        )
            .then(async res => {
                if (res.status === 200) {
                    await message.success(__('You signed up successfully. Enjoy'));
                    await login(val, history, signUpInformation.email, signUpInformation.password, true)
                }
            })
            .catch(async res => {
                setLoading(false);
                if (res.response && res.response.data.message) {
                    message.error(__(res.response.data.message))
                }
            })
    }

    const history = useHistory();
    return (
        <Modal
            className="d-flex justify-content-center"
            width={400}
            visible={props.visible}
            onClick={props.onClick}
            onCancel={props.onCancel}
            bodyStyle={{
                height: "27rem",
                paddingTop: "3rem",
                borderRadius: "0.5rem",
                backgroundColor: "#f7f9f8"
            }}
            footer={[
                <SpinLoading
					loading={loading}
                    mode="loading"
                >
                    <div key={1} className="d-flex justify-content-center align-items-center">
                        <Button
                            className="signIn-ButtonDown font-F-Gothic text-white"
                            onClick={val => showInput === "Login" ? login(val, history) : signUp(val, history)}
                        >
                            {
                                showInput === "Login" ? __("Login") :
                                    showInput === "savedSignUp" ? __("Sign Up") : ""
                            }
                        </Button>
                        <span className="mx-2 text-Or font-F-Gothic">
                            {__("Or")}
                        </span>
                        <Link to="">
                            <img
                                src={google}
                                height={50}
                                alt="Google Account on IslamicSources"
                            />
                        </Link>
                    </div>
                </SpinLoading>
            ]}
        >
            <div className="d-flex justify-content-around align-items-center">
                <Button
                    name="savedSignUp"
                    onClick={() => setName(showInput = "savedSignUp")}
                    value="savedSignUp"
                    className={"signIn-ButtonTop font-F-Gothic " +
                    (showInput === "savedSignUp" ? "selected-tab-button" : "")}
                >
                    {__("Sign Up")}
                </Button>
                <Button
                    name="Login"
                    onClick={() => {
                        setName(showInput = "Login");
                    }}
                    value="Login"
                    className={"signIn-ButtonTop font-F-Gothic " +
                    (showInput === "Login" ? "selected-tab-button" : "")}
                >
                    {__("Login")}
                </Button>
            </div>
            <Row className="px-4 h-100 d-flex flex-column justify-content-around">
                {(showInput === "Login") &&
                <form onSubmit={val => login(val, history)}>
                    <FancyInput
                        rtl={rtlMode}
                        labelStyle={{fontSize: "1.3rem", top: "-5px"}}
                        textStyle={{fontSize: "1.3rem"}}
                        classNameWrapper="w-100 font-F-Gothic"
                        id="EmailForLogin"
                        placeholder={__("Your Email")}
                        type="email"
                        onChange={val => changeInputValue(val, loginInformation)}
                        name="email"
                    />
                    <FancyInput
                        rtl={rtlMode}
                        labelStyle={{fontSize: "1.3rem", top: "-5px"}}
                        textStyle={{fontSize: "1.3rem"}}
                        classNameWrapper="w-100 font-F-Gothic"
                        id="PasswordForLogin"
                        placeholder={__("Password")}
                        type="password"
                        onChange={val => changeInputValue(val, loginInformation)}
                        name="password"
                    />
                    <Link to=""
                          className="mt-n5 ForgotPassword"
                    >
                        {__("Forgot Password ?")}
                    </Link>
                    <button className="d-none" type="submit"/>
                </form>
                }
                {(showInput === "savedSignUp") &&
                <form onSubmit={val => signUp(val, history)}>
                    <FancyInput
                        rtl={rtlMode}
                        labelStyle={{fontSize: "1.3rem", top: "-5px"}}
                        textStyle={{fontSize: "1.3rem"}}
                        classNameWrapper="w-100 font-F-Gothic"
                        id="FirstNameForLogin"
                        placeholder={__("First Name")}
                        type="text"
                        onChange={val => changeInputValue(val, signUpInformation)}
                        name="first_name"
                    />
                    <FancyInput
                        rtl={rtlMode}
                        labelStyle={{fontSize: "1.3rem", top: "-5px"}}
                        textStyle={{fontSize: "1.3rem"}}
                        classNameWrapper="w-100 font-F-Gothic"
                        id="LastNameForLogin"
                        placeholder={__("Last Name")}
                        type="text"
                        onChange={val => changeInputValue(val, signUpInformation)}
                        name="last_name"
                    />
                    <FancyInput
                        rtl={rtlMode}
                        labelStyle={{fontSize: "1.3rem", top: "-5px"}}
                        textStyle={{fontSize: "1.3rem"}}
                        classNameWrapper="w-100 font-F-Gothic"
                        id="EmailForLogin"
                        placeholder={__("Your Email")}
                        type="email"
                        onChange={val => changeInputValue(val, signUpInformation)}
                        name="email"
                    />
                    <FancyInput
                        rtl={rtlMode}
                        labelStyle={{fontSize: "1.3rem", top: "-5px"}}
                        textStyle={{fontSize: "1.3rem"}}
                        classNameWrapper="w-100 font-F-Gothic"
                        id="PasswordForLogin"
                        placeholder={__("Password")}
                        type="password"
                        onChange={val => changeInputValue(val, signUpInformation)}
                        name="password"
                    />
                    <FancyInput
                        rtl={rtlMode}
                        labelStyle={{fontSize: "1.3rem", top: "-5px"}}
                        textStyle={{fontSize: "1.3rem"}}
                        classNameWrapper="w-100 font-F-Gothic"
                        id="ConfirmPasswordForLogin"
                        placeholder={__("Confirm Password")}
                        type="password"
                        onChange={val => changeInputValue(val, signUpInformation)}
                        name="confirmPassword"
                    />
                    <button className="d-none" type="submit"/>
                </form>
                }
            </Row>
        </Modal>
    );
}
const mapStateToProps = states => (
    {
    }
);
export default withRouter(connect(mapStateToProps)(Login))

