import PropTypes from "prop-types";
import {FilterSelectsMode} from "../../Module";
import connect from "react-redux/es/connect/connect";
import React from "react";
import {withRouter} from "react-router-dom";


FilterSelectsModeComponent.propTypes = {
    data: PropTypes.array,
    className: PropTypes.string,
    checks: PropTypes.object,
    CheckFilterSelectedProp: PropTypes.func,
    type: PropTypes.string,
    SearchMode: PropTypes.bool,
    placeholder: PropTypes.string
};
function FilterSelectsModeComponent(props) {
    const {data,checks,className,type,CheckFilterSelectedProp,SearchMode,placeholder,plusId} = props;
    return(
        <div
            style={{
                minHeight: "1.68rem",
                padding: props.FilterRedux === type ? "" : ".6rem 0"
            }}
            className={"d-flex flex-column align-items-center "+className}
        >
            {type === props.FilterRedux ?
                <FilterSelectsMode
                    placeholder={placeholder}
                    SearchMode={SearchMode}
                    CheckFilterSelected={(e)=> CheckFilterSelectedProp(e)}
                    className={`w-100 ${SearchMode && "px-3"}`}
                    data={data}
                    checks={checks && checks.array}
                    type={type}
                    plusId={plusId}
                />
                :
                checks && checks.value
            }
        </div>
    )
}
const mapStateToProps = states => (
    {
        FilterRedux: states.Filter,
    }
);
export default withRouter(connect(mapStateToProps)(FilterSelectsModeComponent));