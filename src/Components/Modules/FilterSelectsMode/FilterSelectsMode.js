import React from 'react';
import './FilterSelectsMode.css'
import PropTypes from "prop-types";
import {Input} from "antd";

const { Search } = Input;

InputCheckBoxItems.propTypes = {
    name: PropTypes.string,
};

function InputCheckBoxItems(props) {
    const {checks, name, CheckFilterSelected,type,plusId} = props;
    return (
        <div className="InputCheckBoxItemsClass d-flex align-items-center my-1">
            <input
                onChange={(e) => CheckFilterSelected({
                    name: name,
                    checked: e.target.checked,
                    type:type
                })}
                id={(plusId&&plusId)+name}
                className="position-relative mx-2"
                type="checkbox"
                checked={checks[name] && checks[name].checked ? checks[name].checked : null}
            />
            <label htmlFor={(plusId&&plusId)+name}>{name}</label>
        </div>
    )
}


FilterSelectsMode.propTypes = {
    data: PropTypes.array,
    className: PropTypes.string,
    CheckFilterSelected: PropTypes.func
};
export default function FilterSelectsMode(props) {

    const {className, checks, CheckFilterSelected, SearchMode,placeholder,type,plusId} = props;
    return (
        <div
            className={SearchMode
                ?
                "FilterSelectsModeClass bg-white d-flex align-items-start flex-column " + className
                :
                "FilterSelectsModeClass bg-white d-flex align-items-center flex-column " + className
            }
        >
            {SearchMode &&
            <div className="w-100 d-flex justify-content-center">
                <Search
                    placeholder={placeholder}
                    onSearch={value => console.log(value)}
                    style={{ width: "95%" }}
                    className="search-modeClass"
                />
            </div>
            }
            {props.data &&
            props.data.map((d, i) =>
                <InputCheckBoxItems
                    checks={checks}
                    CheckFilterSelected={(e) =>
                        CheckFilterSelected(e)
                    }
                    key={i}
                    name={d.name}
                    type={type}
                    plusId={plusId}
                />
            )
            }
        </div>
    );
}

