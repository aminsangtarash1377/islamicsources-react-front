import React, {Component} from 'react';
import PropTypes from "prop-types";

export class CustomImage extends Component {
    state = {
        image: null,
        loading: false,
    };

    componentDidMount() {
        fetch(this.props.url)
            .then(res => res.text())
            .then(text =>
                this.setState({
                    image: text
                })
            )
    }

    UNSAFE_componentWillReceiveProps(nextProps, nextContext) {
        fetch(nextProps.url)
            .then(res => res.text())
            .then(text =>
                this.setState({
                    image: text
                })
            )
    }

    static propTypes = {
        url: PropTypes.string.isRequired
    };

    render() {
        const {loading, image} = this.state;
        const {url, style, ...restProps} = this.props;
        if (loading) {
            return <div className="spinner"/>;
        } else if (!image) {
            return <div className="error"/>
        }
        if (image.indexOf('svg') > 0) {
            return <div {...restProps} style={style} dangerouslySetInnerHTML={{__html: this.state.image}}/>;
        } else {
            return <img style={style} src={url} alt="لوگوی متغیر" {...restProps}  />;

        }
    }
}