import React from "react";
import "./FilterOption.css"
import connect from "react-redux/es/connect/connect";
import {withRouter} from "react-router-dom";
import {Filter} from "../../../actions";
import PropTypes from "prop-types";
import {CustomImage} from "../CustomImage";
import arrowB from '../../../images/DirectionLeft.svg';

FilterOption.propTypes = {
    name: PropTypes.string,
    childStatic: PropTypes.bool,
    className: PropTypes.string,
};

function FilterOption(props) {
    return (
        <React.Fragment>
            <div
                onClick={() => props.dispatch(Filter(props.name))}
                className={
                    "w-100 " +
                    "d-flex " +
                    "justify-content-center " +
                    "position-relative " +
                    "FilterOptionStyle " +
                    "cursor-p py-2 " +
                    props.className
                }
            >
                <span>{props.name}</span>
                <CustomImage
                    className="position-absolute"
                    style={{
                        transform:"rotate(90deg)",
                        width:'.15rem',
                        right:"1rem",
                        top:"25%"
                    }}
                    url={arrowB}
                />
            </div>
            {props.childStatic ?
                props.children
                :
                props.name === props.FilterRedux && props.children
                    ?
                    props.children
                    :
                    <div style={{height: '1.68rem'}}/>
            }
        </React.Fragment>
    )
}

const mapStateToProps = states => (
    {
        FilterRedux: states.Filter
    }
);
export default withRouter(connect(mapStateToProps)(FilterOption));
