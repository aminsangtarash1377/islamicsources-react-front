import React, {useEffect, useState} from 'react';
import {CustomImage} from "../CustomImage";
import gift from "../../../images/gift.svg";
import axios from "axios";
import {Rate, message} from "antd";
import one from "../../../images/rate/Point A-A.svg";
import two from "../../../images/rate/Point A-B.svg";
import three from "../../../images/rate/Point A-C.svg";
import four from "../../../images/rate/Point A-D.svg";
import five from "../../../images/rate/Point A-E.svg";
import BookMarkFalse from "../../../images/neshan.svg";
import BookMarkTrue from "../../../images/neshan A.svg";
import {Link} from "react-router-dom";
import {defaultRahUrl, defaultWPUrl, ISAccessUserToken, setRateByUser} from "../../../config";
import {SpinLoading} from "../../Module";
import {__} from "../../../global";
import "./CardView.css";

export default function CardView(props){
    const {data,link} = props;
    const [rate, SetRating] = useState(true);
    const [bookMark,SetBookMark] = useState(false);
    const [UnitRate, SetUnitRate] = useState(null);
    const [imageLevel, SetImageLevel] = useState(one);
    let [loading, SetLoading] = useState(false);

    useEffect(()=>{
        handleRate(data && data[props.type+'fields'] && Math.round(data[props.type+'fields'].rate/10));
        SetRating(true);
    },[]);


    function handleRate(value){
        switch (value) {
            case 1:
                SetUnitRate(1);
                SetImageLevel(one);
                break;
            case 2:
                SetUnitRate(2);
                SetImageLevel(two);
                break;
            case 3:
                SetUnitRate(3);
                SetImageLevel(three);
                break;
            case 4:
                SetUnitRate(4);
                SetImageLevel(four);
                break;
            case 5:
                SetUnitRate(5);
                SetImageLevel(five);
                break;
        }
        SetRating(!rate);
    }

    async function SetRate(rate, targetId){
        if(setRateByUser(targetId)){
            await axios.post(defaultRahUrl+`/rate/?type=post&id=${targetId}&rate=${rate}`)
                .then(result =>{
                    if(result.data) {
                        handleRate(result.data && Math.round(result.data.rate));
                        message.success("Done")
                    }else{
                        handleRate(1);
                    }
                })
                .catch(res=>{
                    if (res.response && res.response.data.message){
                        message.error(__(res.response.data.message))
                    }
                })
        }else{
            message.warning(__('You rate this post once'))
        }
    }

    async function SetBookMarking(PostId){
        SetLoading(true);
        await axios.post(defaultWPUrl+`/users/BookMarks`,
            {"PostId":PostId},
            {headers:{"Content-Type":"application/json",
                    Authorization : ISAccessUserToken}}
            )
            .then(result =>{
                console.log(result);
                if (result.status === 200){
                    SetBookMark(result.data.data);
                    message.success(__('Done'));
                }
            })
            .catch(res => {
                if (res.response && res.response.data.message){
                    message.error(__(res.response.data.message))
                }
            });
        SetLoading(false);
    }

    const targetId = eval('data.'+props.type + 'Id');
    return(
        <SpinLoading
            loading={loading}
            mode="loading"
        >
            <div className="CardView my-2 my-lg-3">
                <Link to={`/${link}/${data && data.slug}`}>
                    <h4 className="font-F-Gothic">
                        {data && data.title}
                    </h4>
                </Link>
                <div
                    className="d-flex flex-row"
                    style={{height:"1.5rem"}}
                >
                    {rate ?
                        <div className="d-flex flex-row DivRateAndGift">
                            {
                                <CustomImage
                                    url={ bookMark ? BookMarkTrue : BookMarkFalse }
                                    onClick={async ()=>{await SetBookMarking(targetId)}}
                                    className="SingleCategoryRateAndGift"
                                />
                            }
                            <CustomImage
                                url={imageLevel}
                                onClick={() => SetRating(!rate)}
                                className="SingleCategoryRateAndGift mx-4 mx-lg-2"
                            />
                            <CustomImage
                                url={gift}
                                className="SingleCategoryRateAndGift"
                            />
                        </div>
                        :
                        <Rate
                            defaultValue={UnitRate}
                            onChange={async (e) => {await SetRate(e, targetId);}
                            }
                            character={
                                <div
                                    className="position-relative "
                                    style={{
                                        width: "1rem",
                                        height: "1rem",
                                    }}
                                >
                                    <div
                                        style={{
                                            width: ".9rem",
                                            height: ".9rem",
                                            background: "#b3b3b3",
                                            borderRadius: "2px"
                                        }}
                                        className="RatingSubject position-absolute"
                                    />
                                    <div
                                        style={{
                                            width: ".9rem",
                                            height: ".9rem",
                                            background: "#b3b3b3",
                                            transform: "rotate(45deg)",
                                            borderRadius: "2px"
                                        }}
                                        className="RatingSubject position-absolute"
                                    />
                                </div>
                            }
                            allowHalf={false}
                        />
                    }
                </div>
            </div>
        </SpinLoading>
    )
}