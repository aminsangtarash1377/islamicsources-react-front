import React from "react";
import connect from "react-redux/es/connect/connect";
import {CheckTypeSelected, CheckFormatSelected, CheckCreatorSelected} from "../../../actions";
import {withRouter} from "react-router-dom";
import PropTypes from "prop-types";

NewSelectInput.propTypes = {
    name: PropTypes.string,
    checked: PropTypes.bool,
    type: PropTypes.string
};

function NewSelectInput(props) {
    const {name,checked,type,plusId} = props;
    return(
        <input
            onChange={()=>{
                type === "TYPE" &&
                props.dispatch(CheckTypeSelected({
                    checked:false,
                    name:name
                }));
                type === "FORMAT" &&
                props.dispatch(CheckFormatSelected({
                    checked:false,
                    name:name
                }));
                type === "CREATOR" &&
                props.dispatch(CheckCreatorSelected({
                    checked:false,
                    name:name
                }));
            }}
            id={(plusId&&plusId)+name}
            className="position-relative mx-2"
            type="checkbox"
            defaultChecked={checked }
        />
    )
}

const mapStateToProps = () => (
    {}
);
export default withRouter(connect(mapStateToProps)(NewSelectInput));