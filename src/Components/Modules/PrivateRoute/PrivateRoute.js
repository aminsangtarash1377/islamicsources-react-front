import React, {Component} from 'react';
import {Redirect, Route} from "react-router-dom";
import {connect} from "react-redux";

class PrivateRoute extends Component {
    render() {
        const {component: Component, props: cProps,...restProps} = this.props;
        const isAuthenticated = this.props.authorization;
        return (
             <Route
                 {...restProps}
                 render={(props) =>
                     isAuthenticated ?  (
                         <Component
                             {...props}
                             {...cProps}
                         />
                     ) : (
                         <Redirect
                             {...cProps}
                             to={{
                                 pathname: `/`,
                                 state: {from: props.location}
                             }}
                         />

                     )
                 }
             />
        );
    }
}


const mapStateToProps = (state) => {
    return {
        authorization: state.authorization
    }
};
export default connect(mapStateToProps)(PrivateRoute)
