import React, {useEffect, useState} from 'react';
import "./SearchIn.css";
import {Button, Slider} from "antd";
import {Link, withRouter} from "react-router-dom";
import {FilterOption, FilterSelectsModeComponent} from "../../../Module";
import {CheckCreatorSelected, CheckFormatSelected, CheckTypeSelected} from "../../../../actions";
import connect from "react-redux/es/connect/connect";



let dataBuilderSearch = {
    Subject:{},
    Writer:{},
    Interpreter:{},
};
function SearchIn(props) {
    const [minYear, setMinYear] = useState(null);
    const [maxYear, setMaxYear] = useState(2019);
    const [typeFilter, setTypeFilter] = useState([]);
    const [formatFilter, setFormatFilter] = useState([]);
    const [creatorFilter, setCreatorFilter] = useState([]);

    function SetChanges(e) {
        setMinYear(e[0]);
        setMaxYear(e[1]);
    }
    const SubjectData = [
        {
            name: "Book"
        },
        {
            name: "Article"
        },
        {
            name: "Journal"
        }
    ];
    const YearData = [
        {
            name: "Format1"
        },
        {
            name: "Format2"
        },
        {
            name: "Format3"
        }
    ];
    const WriterData = [
        {
            name: "Book"
        },
        {
            name: "Article"
        },
        {
            name: "Journal"
        }
    ];
    const InterpreterData = [
        {
            name: "Format1"
        },
        {
            name: "Format2"
        },
        {
            name: "Format3"
        }
    ];
    const PrintPlaceData = [
        {
            name: "Book"
        },
        {
            name: "Article"
        },
        {
            name: "Journal"
        }
    ];
    const PublisherData = [
        {
            name: "Format1"
        },
        {
            name: "Format2"
        },
        {
            name: "Format3"
        }
    ];

    function FCheckSelected() {
        useEffect(() => {
            SubjectData.map(d => {
                typeFilter.push({
                    name: d.name
                });
                return setTypeFilter(typeFilter)
            });
            typeFilter.map(d => {
                const array = ({
                    name: d.name,
                    checked: false,
                });
                return props.dispatch(CheckTypeSelected(array))
            });

            YearData.map(d => {
                typeFilter.push({
                    name: d.name
                });
                return setTypeFilter(typeFilter)
            });
            typeFilter.map(d => {
                const array = ({
                    name: d.name,
                    checked: false,
                });
                return props.dispatch(CheckTypeSelected(array))
            });

            WriterData.map(d => {
                formatFilter.push({
                    name: d.name
                });
                return setFormatFilter(formatFilter)
            });
            formatFilter.map(d => {
                const array = ({
                    name: d.name,
                    checked: false,
                });
                return props.dispatch(CheckFormatSelected(array))
            });

            InterpreterData.map(d => {
                formatFilter.push({
                    name: d.name
                });
                return setFormatFilter(formatFilter)
            });
            formatFilter.map(d => {
                const array = ({
                    name: d.name,
                    checked: false,
                });
                return props.dispatch(CheckFormatSelected(array))
            });

            PrintPlaceData.map(d => {
                creatorFilter.push({
                    name: d.name
                });
                return setCreatorFilter(creatorFilter)
            });
            creatorFilter.map(d => {
                const array = ({
                    name: d.name,
                    checked: false,
                });
                return props.dispatch(CheckCreatorSelected(array))
            });

            PublisherData.map(d => {
                creatorFilter.push({
                    name: d.name
                });
                return setCreatorFilter(creatorFilter)
            });
            creatorFilter.map(d => {
                const array = ({
                    name: d.name,
                    checked: false,
                });
                return props.dispatch(CheckCreatorSelected(array))
            })
        }, [])
    }

    FCheckSelected();
    function SendDataToProp(e) {
        dataBuilderSearch[e.type][e.name] = e;
        props.sendData(dataBuilderSearch);
        console.log(props.CheckCreatorSelected&& props.CheckCreatorSelected)
    }
    const {id} = props;
    return (
        <div className="mt-5 paddingX d-flex flex-lg-row flex-column align-items-center font-F-Gothic">
            <div
                className="mx-5 d-flex flex-column SearchPageMenu"
                style={{width: "15rem"}}
            >
                <FilterOption name="Subject" className="SearchPageMenuOption">
                    <FilterSelectsModeComponent
                        data={SubjectData}
                        CheckFilterSelectedProp={(e) => {
                            SendDataToProp(e)
                        }}
                        checks={props.CheckSelected && props.CheckSelected}
                        type="Subject"
                        plusId={id}
                    />
                </FilterOption>

                <FilterOption name="Year" className="SearchPageMenuOption">
                    {"Year" === props.FilterRedux
                        ?
                        <div
                            className="
                                FilterOptionStyle
                                bg-white
                                my-2
                                py-2
                                px-2
                                d-flex
                                justify-content-between
                                align-items-center
                            "
                        >
                            <span>1800</span>
                            <Slider
                                onChange={(e) => SetChanges(e)}
                                style={{width: "60%"}}
                                min={1800}
                                max={2019}
                                className="m-0 SliderRangeFilterYear"
                                range
                                step={1}
                                defaultValue={[minYear, maxYear]}
                            />
                            <span>2019</span>
                        </div>
                        :
                        minYear
                            ?
                            ""
                            :
                            <div
                                style={{height: '1.68rem'}}
                            />
                    }
                </FilterOption>

                <FilterOption name="Writer" className="SearchPageMenuOption">
                    <FilterSelectsModeComponent
                        data={WriterData}
                        CheckFilterSelectedProp={(e) => {
                            SendDataToProp(e)
                        }}
                        checks={props.CheckFormatSelected && props.CheckFormatSelected}
                        type="Writer"
                        plusId={id}
                    />
                </FilterOption>
            </div>
            <div
                className="mx-5 d-flex flex-column SearchPageMenu"
                style={{width: "15rem"}}
            >
                <FilterOption name="Interpreter" className="SearchPageMenuOption">
                    <FilterSelectsModeComponent
                        data={InterpreterData}
                        CheckFilterSelectedProp={(e) => {
                            props.dispatch(CheckCreatorSelected(e));
                            SendDataToProp(e)
                        }}
                        checks={props.CheckCreatorSelected && props.CheckCreatorSelected}
                        type="Interpreter"
                        plusId={id}
                    />
                </FilterOption>
                <FilterOption name="Print place" className="SearchPageMenuOption">
                </FilterOption>
                <FilterOption name="Publisher" className="SearchPageMenuOption">
                </FilterOption>
            </div>
            <Link to={``}>
                <Button className="SearchPageButton">
                    Search
                </Button>
            </Link>
        </div>
    );
}

const mapStateToProps = states => (
    {
        FilterRedux: states.Filter,
        RtlDir: states.Rtl,
        CheckSelected: states.CheckTypeSelected,
        CheckFormatSelected: states.CheckFormatSelected,
        CheckCreatorSelected: states.CheckCreatorSelected
    }
);
export default withRouter(connect(mapStateToProps)(SearchIn));
