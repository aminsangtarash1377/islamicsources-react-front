import React from 'react';
import './MyList.css';
import {Icon} from "antd";
import connect from "react-redux/es/connect/connect";
import {withRouter} from "react-router-dom";


function MyList(props) {
    return (
        <div className="mt-5">
            <div className="d-flex align-items-center">
                <h4
                    className="title-Location mb-0 font-F-Gothic"
                    style={{minWidth: "5rem"}}
                >
                    My List
                </h4>
                <hr className="w-100 mx-4 GaryBorder"/>
                <div
                    className="
                    IconCloseBackG cursor-p
                    d-flex justify-content-center
                    align-items-center"
                >
                    <Icon type="close"/>
                </div>
            </div>
            <div className="d-flex justify-content-start">
                {props.SearchFiltersMyLists && props.SearchFiltersMyLists.value}
            </div>
        </div>
    );
}

const mapStateToProps = states => (
    {
        FilterRedux: states.Filter,
        RtlDir: states.Rtl,
        SearchFiltersMyLists: states.SearchFiltersMyLists,
    }
);
export default withRouter(connect(mapStateToProps)(MyList));