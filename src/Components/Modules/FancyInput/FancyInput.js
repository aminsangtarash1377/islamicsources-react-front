import React, {Component} from 'react';
import PropTypes from 'prop-types';
import './FancyInput.css';

class FancyInput extends Component {
    constructor(props) {
        super(props);
        const value = props.defaultValue ? props.defaultValue : "";
        this.state = {
            selfValue: value,
            blur : value === "" || value === undefined,
        };
    }

    onChange = val => {
        const {onChange} = this.props;
        if(onChange){
            onChange(val);
            this.setState({selfValue : val.target.value})
        }else{
            this.setState({selfValue : val.target.value})
        }
    };

    inputFocus = () => {
        this.setState({blur : false})
    };

    inputBlur = () => {
        const {selfValue} = this.state;
        if(selfValue === "" || selfValue === undefined){
            this.setState({blur : true})
        }
    };

    static propTypes = {
        id : PropTypes.string.isRequired,
        rtl : PropTypes.bool.isRequired,
        placeholder : PropTypes.string.isRequired,
        onChange : PropTypes.func,
        value : PropTypes.string,
        name : PropTypes.string,
        className : PropTypes.string,
        classNameWrapper : PropTypes.string,
        defaultValue : PropTypes.string,
        type : PropTypes.string,
        style : PropTypes.object,
        textStyle : PropTypes.object,
        labelStyle : PropTypes.object,
        readOnly : PropTypes.bool,
        disabled : PropTypes.bool
    };

    render() {
        const {selfValue, blur} = this.state;
        const {
            value, placeholder, id, type,
            disabled, readOnly, rtl, style,
            textStyle, labelStyle, className = "", classNameWrapper = "", name = ""} = this.props;
        const rtlMode = rtl ? "BtxFormControl-Rtl" : "";
        return (
            <div className={"BtxFormControl-root BtxTextField-root BtxFormControl-marginNormal " +
            rtlMode + " " + classNameWrapper} style={style}>
                {
                    placeholder &&
                    <label
                        style={labelStyle}
                        htmlFor={id}
                        className={
                            blur ?
                                "BtxFormLabel-root BtxInputLabel-root BtxInputLabel-formControl BtxInputLabel-animated"
                                :
                                "BtxFormLabel-root BtxInputLabel-root BtxInputLabel-formControl" +
                                " BtxInputLabel-animated BtxInputLabel-shrink Btx-focused"
                        }
                        data-shrink={!blur}
                    >
                        {placeholder}
                    </label>
                }
                <div className={
                    blur ?
                        "BtxInputBase-root BtxInput-root BtxInput-underline" +
                        " BtxInputBase-formControl BtxInput-formControl"
                        :
                        "BtxInputBase-root BtxInput-root BtxInput-underline " +
                        "BtxInputBase-formControl BtxInput-formControl Btx-focused"
                }>
                    <input
                        id={id}
                        style={textStyle}
                        readOnly={readOnly ? readOnly : false}
                        disabled={disabled ? disabled : false}
                        className={"BtxInputBase-input BtxInput-input BtxInputBase-inputTypeSearch BtxInput-inputTypeSearch "
                        + className}
                        value={value !== undefined ? value : selfValue}
                        type={type ? type : "text"}
                        name={name}
                        onChange={this.onChange}
                        onBlur={this.inputBlur}
                        onFocus={this.inputFocus}
                        onClick={this.inputFocus}
                    />
                </div>
            </div>
        );
    }
}

export default FancyInput;