import React, {useEffect, useState} from "react";
import {Col} from "antd";
import {TabSectionForMedia} from "../../Module";
import connect from "react-redux/es/connect/connect";
import sound from "../../../Sound/Shahrooz Habibi - Labayk Shahe Atshan (128).mp3";
import {withRouter} from "react-router-dom";
import {SoundPlayer} from "../../Module";
import './SoundBoxInMediapage.css';


function SoundBoxInMediaPage(props) {
    const [content, onTabChange] = useState(null);
    const tabs_image = [
        {
            title: "New",
            mode: "New"
        },
        {
            title: "Hot",
            mode: "Hot"
        }
    ];

    function Playing(e) {
        let parent = e.currentTarget.parentElement.parentElement.parentElement.parentElement;
        let value = document.querySelectorAll(".ParentPlayer");

        if (value.length === 0 && !parent.classList.contains("ParentPlayer")) {
            parent.classList.add("ParentPlayer");
        } else {
                value.forEach(x => {
                    if(parent !== x) {
                        x.classList.remove("ParentPlayer");
                        x.children[0].children[0].children[0].children[0].pause();
                        parent.classList.add("ParentPlayer");
                    }
                })
        }
    }

    function pausing(e){
        let parent = e.currentTarget.parentElement.parentElement.parentElement.parentElement;
        parent.classList.remove("ParentPlayer");
    }

    let array = [
        {
            title:"The Philosophy of the Kalam",
            src:sound
        },
        {
            title:"The Value of The Study In Terms Of Islam",
            src:sound
        },
        {
            title:"Sports At The Sight Of Islam",
            src:sound
        },
        {
            title:"MetaPhysical Penetrations",
            src:sound
        },
        {
            title:"MetaPhysical Penetrations",
            src:sound
        },
        ];

    useEffect(() => {
        tabs_image && tabs_image.length > 0 ? onTabChange(tabs_image[0]) : onTabChange(tabs_image[0])
    }, []);
    return (
        <div className="d-flex flex-column w-100 mt-5">
            <TabSectionForMedia
                tabs={tabs_image}
                title="Sound"
                onTabChange={(e) => {
                    onTabChange(e);
                }}
                hr
                more
                defaultSelectedButton={content && content.title}
                moreLink={``}
            />
            <div
                className="w-100 d-flex flex-row
                justify-content-center align-items-center mt-3 mt-lg-5"
            >
                <Col span={24} lg={21}>
                    <div className="w-100 d-flex flex-column">
                        <div className="w-100 d-flex flex-column flex-lg-row">
                            <div className="BorderOptionSound">
                                <div className="">

                                </div>
                            </div>
                            <div className="ListSound d-flex flex-column justify-content-end">
                                {array.map((data, i) => (
                                    <div
                                        key={i}
                                        className={`w-100 d-flex justify-content-end`}
                                    >
                                        <SoundPlayer
                                            key={i}
                                            data={data}
                                            playing={Playing}
                                            pausing={pausing}
                                        />
                                    </div>
                                ))}
                            </div>
                        </div>
                    </div>
                </Col>
            </div>
        </div>
    )
}

const mapStateToProps = states => (
    {
        RtlDir: states.Rtl
    }
);
export default withRouter(connect(mapStateToProps)(SoundBoxInMediaPage));