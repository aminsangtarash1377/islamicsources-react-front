import React, {useEffect, useState} from "react";
import {Col} from "antd";
import {TabSectionForMedia} from "../../Module";
import one from "../../../images/pic/Picture Media A.jpg";
import two from "../../../images/pic/Picture Media B.jpg";
import three from "../../../images/pic/Picture Media C.jpg";
import four from "../../../images/pic/Picture Media D.jpg";
import five from "../../../images/pic/Picture Media E.jpg";
import six from "../../../images/pic/Picture Media F.jpg";
import seven from "../../../images/pic/Picture Media G.jpg";
import connect from "react-redux/es/connect/connect";
import {withRouter} from "react-router-dom";
import './ImageBoxInMediaPage.css';


function ImageBoxInMediaPage(props) {
    const [content, onTabChange] = useState(null);
    const tabs_image =[
        {
            title:"New",
            mode:"New"
        },
        {
            title:"Hot",
            mode:"Hot"
        }
    ];
    useEffect(()=>{
        tabs_image && tabs_image.length > 0 ? onTabChange(tabs_image[0]) : onTabChange(tabs_image[0])
    },[]);

    const arrayImg = [two,three,four,five,six,seven];
    return(
        <React.Fragment>
            <TabSectionForMedia
                tabs={tabs_image}
                title="Image"
                onTabChange={(e) => {
                    onTabChange(e);
                }}
                hr
                more
                defaultSelectedButton={content && content.title}
                moreLink={``}
            />
            <div
                className="w-100 d-flex flex-row
                justify-content-center align-items-center mt-3 mt-lg-5"
            >
                <Col span={24} lg={21}>
                    <div className="w-100 d-flex flex-column flex-md-row">
                        <Col span={24} lg={9}>
                            <div
                                className="w-100 d-flex
                                justify-content-center
                                align-items-center LargeImageInMedia"
                            >
                                <img
                                    src={one}
                                    alt=""
                                    className="w-100"
                                />
                            </div>
                        </Col>
                        <Col span={24} lg={15}>
                            <div className="d-flex flex-row flex-wrap">
                                {arrayImg.map((x,i)=>(
                                    <img
                                        src={x}
                                        alt=""
                                        key={i}
                                        className="imageForImageMedia"
                                    />
                                ))}
                            </div>
                        </Col>
                    </div>
                </Col>
            </div>
        </React.Fragment>
    )
}
const mapStateToProps = states => (
    {
        RtlDir: states.Rtl,
        Single: states.SingleInCategoriesPageTitle,
        SingleSlug: states.SingleInCategoriesPageSlug
    }
);
export default withRouter(connect(mapStateToProps)(ImageBoxInMediaPage));