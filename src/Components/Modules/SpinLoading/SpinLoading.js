import React, {Component} from 'react';
import {Icon, Spin} from "antd";
import {loadingItem} from "../../../config";
import PropTypes from "prop-types";

class SpinLoading extends Component {

    static propTypes = {
        loading : PropTypes.bool,
        mode : PropTypes.oneOf(['logo', 'loading', 'withoutBox'])
    };
    render() {
        const {loading = true, mode = "logo", ...attrs} = this.props;
        return (
            mode === "withoutBox" ?
                loading ?
                    <div {...attrs}>
                        <Spin
                            style={{width : "100%"}}
                            spinning={loading}
                            indicator={
                                mode === "loading" ?
                                    <Icon type="loading" style={{ fontSize: 24 }} spin />
                                    :
                                    <div className="w-100 h-100 d-flex align-items-center justify-content-center" style={{right : 0,top : "10%"}}>
                                        <img className="w-25 h-auto" src={loadingItem} alt="loading" />
                                    </div>
                            }
                        />
                    </div>
                    :
                    this.props.children
                :
                <Spin
                    spinning={loading}
                    indicator={
                        mode === "loading" ?
                            <Icon type="loading" style={{ fontSize: 24 }} spin />
                            :
                            <div className="w-100 h-100 d-flex align-items-center justify-content-center" style={{right : 0,top : "10%"}}>
                                <img className="w-25 h-auto" src={loadingItem} alt="loading" />
                            </div>
                    }
                >
                    {this.props.children}
                </Spin>
        );
    }
}

export default SpinLoading;