import React, {useEffect, useState} from 'react';
import './SearchFilterMenu.css'
import {CustomImage, FilterOption, FilterSelectsModeComponent} from "../../Module"
import connect from "react-redux/es/connect/connect";
import {withRouter} from "react-router-dom";
import {Scrollbars} from "react-custom-scrollbars";
import {Slider} from "antd";
import Arrow from '../../../images/DirectionLeft.svg'
import {CheckTypeSelected, CheckFormatSelected, CheckCreatorSelected} from '../../../actions'
import {__} from "../../../global";


function SearchFilterMenu(props) {
    const [minYear, setMinYear] = useState(null);
    const [maxYear, setMaxYear] = useState(2019);
    const [typeFilter, setTypeFilter] = useState([]);
    const [formatFilter, setFormatFilter] = useState([]);
    const [creatorFilter, setCreatorFilter] = useState([]);

    function SetChanges(e) {
        setMinYear(e[0]);
        setMaxYear(e[1]);
    }

    const data = [
        {
            name: "Book"
        },
        {
            name: "Article"
        },
        {
            name: "Journal"
        }
    ];

    const dataFormat = [
        {
            name: "Format1"
        },
        {
            name: "Format2"
        },
        {
            name: "Format3"
        }
    ];

    const dataCreator = [
        {
            name: "creator1"
        },
        {
            name: "creator2"
        }
    ];

    function FCheckSelected() {
        useEffect(() => {
            data.map(d => {
                typeFilter.push({
                    name: d.name
                });
                return setTypeFilter(typeFilter)
            });
            typeFilter.map(d => {
                const array = ({
                    name: d.name,
                    checked: false,
                });
                return props.dispatch(CheckTypeSelected(array))
            });

            dataFormat.map(d => {
                formatFilter.push({
                    name: d.name
                });
                return setFormatFilter(formatFilter)
            });
            formatFilter.map(d => {
                const array = ({
                    name: d.name,
                    checked: false,
                });
                return props.dispatch(CheckFormatSelected(array))
            });

            dataCreator.map(d => {
                creatorFilter.push({
                    name: d.name
                });
                return setCreatorFilter(creatorFilter)
            });
            creatorFilter.map(d => {
                const array = ({
                    name: d.name,
                    checked: false,
                });
                return props.dispatch(CheckCreatorSelected(array))
            })
        }, []);
    }

    FCheckSelected();
    return (
        <div>
            <div
                className="Search-Filter-Menu font-F-Gothic"
                style={{
                    left: props.RtlDir ? "" : props.filter ? "-20rem" : "0",
                    right: props.RtlDir ? props.filter ? "-20rem" : "0" : "",
                    paddingTop: props.top
                }}
            >
                <Scrollbars
                    autoHide
                    className="right-scrollFilter position-relative rtl-scroll-Control"
                    style={{height: "100%", width: "100%", overflowX: "hidden", zIndex: "2"}}
                >
                    <div className="d-flex align-items-center mt-4-5 justify-content-between mb-3">
                    <span className="filter-title">
                        Filter
                    </span>
                        <hr className="mt-1 mb-0 hr-Filter mx-0"/>
                    </div>

                    <FilterOption childStatic name="Year">
                        {"Year" === props.FilterRedux
                            ?
                            <div
                                className="
                                FilterOptionStyle
                                bg-white
                                my-2
                                py-2
                                px-2
                                d-flex
                                justify-content-between
                                align-items-center
                            "
                            >
                                <span>1800</span>
                                <Slider
                                    onChange={(e) => SetChanges(e)}
                                    style={{width: "60%"}}
                                    min={1800}
                                    max={2019}
                                    className="m-0 SliderRangeFilterYear"
                                    range
                                    step={1}
                                    defaultValue={[minYear, maxYear]}
                                />
                                <span>2019</span>
                            </div>
                            :
                            minYear
                                ?
                                <div className="d-flex justify-content-between my-3">
                                    <div className="d-flex align-items-center number-year-parent">
                                        <CustomImage
                                            onClick={() => {
                                                minYear > 1800 &&
                                                setMinYear(minYear - 1);
                                                document.getElementById("yearMinSearch").value = minYear
                                            }}
                                            className="mr-3 arrow-filters"
                                            style={{
                                                transform: "rotate(180deg)",
                                            }}
                                            url={Arrow}
                                        />
                                        <div
                                            style={{
                                                maxWidth: "2rem",
                                                overflow: "hidden"
                                            }}
                                        >
                                            <input
                                                id="yearMinSearch"
                                                type="number"
                                                onBlur={(e) => {
                                                    (e.target.value >= maxYear) ?
                                                        (e.target.value = maxYear - 1)
                                                        :
                                                        (e.target.value > 2018) &&
                                                        (e.target.value = "2018");

                                                    (e.target.value < 1800) &&
                                                    (e.target.value = "1800");
                                                    setMinYear(parseInt(e.target.value))
                                                }}
                                                min={1800}
                                                max={2018}
                                                style={{
                                                    background: "transparent",
                                                    border: "none",
                                                    maxWidth: "3rem",
                                                    outline: "none"
                                                }}
                                                className="text-black D-letter"
                                                defaultValue={minYear}
                                            />
                                        </div>
                                        <CustomImage
                                            onClick={() => {
                                                minYear < 2018 && minYear < maxYear - 2 &&
                                                setMinYear(minYear + 1);
                                                document.getElementById("yearMinSearch").value = minYear
                                            }}
                                            className="ml-3 arrow-filters"
                                            url={Arrow}
                                        />
                                    </div>
                                    <span>to</span>
                                    <div className="d-flex align-items-center number-year-parent">
                                        <CustomImage
                                            onClick={() => {
                                                maxYear > 1801 && maxYear >= (minYear + 2) &&
                                                setMaxYear(maxYear - 1);
                                                document.getElementById("yearMaxSearch").value = maxYear
                                            }}
                                            className="mr-3 arrow-filters"
                                            style={{
                                                transform: "rotate(180deg)",
                                            }}
                                            url={Arrow}
                                        />
                                        <div
                                            style={{
                                                maxWidth: "2rem",
                                                overflow: "hidden"
                                            }}
                                        >
                                            <input
                                                id="yearMaxSearch"
                                                type="number"
                                                onBlur={(e) => {
                                                    (e.target.value <= minYear)
                                                        ?
                                                        (e.target.value = minYear + 1)
                                                        :
                                                        (e.target.value < 1800) &&
                                                        (e.target.value = "1801");

                                                    (e.target.value > 2019) &&
                                                    (e.target.value = "2019");
                                                    setMaxYear(parseInt(e.target.value));
                                                }}
                                                min={1801}
                                                max={2019}
                                                style={{
                                                    background: "transparent",
                                                    border: "none",
                                                    maxWidth: "3rem",
                                                    outline: "none"
                                                }}
                                                className="text-black D-letter"
                                                defaultValue={maxYear}
                                            />
                                        </div>
                                        <CustomImage
                                            onClick={() => {
                                                maxYear < 2019 &&
                                                setMaxYear(maxYear + 1);
                                                document.getElementById("yearMaxSearch").value = maxYear;
                                            }}
                                            className="ml-3 arrow-filters"
                                            url={Arrow}
                                        />
                                    </div>
                                </div>
                                :
                                <div
                                    style={{height: '1.68rem'}}
                                />
                        }
                    </FilterOption>

                    <FilterOption childStatic name="Type">
                        <FilterSelectsModeComponent
                            data={typeFilter}
                            CheckFilterSelectedProp={(e) => props.dispatch(CheckTypeSelected(e))}
                            checks={props.CheckSelected && props.CheckSelected}
                            type="Type"
                        />
                    </FilterOption>

                    <FilterOption childStatic name="Format">
                        <FilterSelectsModeComponent
                            data={formatFilter}
                            CheckFilterSelectedProp={(e) => props.dispatch(CheckFormatSelected(e))}
                            checks={props.CheckFormatSelected && props.CheckFormatSelected}
                            type="Format"
                        />
                    </FilterOption>

                    <FilterOption childStatic name="Creator">
                        <FilterSelectsModeComponent
                            placeholder={__("Creator")+"..."}
                            data={creatorFilter}
                            SearchMode={true}
                            CheckFilterSelectedProp={(e) => props.dispatch(CheckCreatorSelected(e))}
                            checks={props.CheckCreatorSelected && props.CheckCreatorSelected}
                            type="Creator"
                        />
                    </FilterOption>

                    <FilterOption name="Sort by">
                    </FilterOption>

                    <FilterOption name="Publisher">
                    </FilterOption>

                </Scrollbars>
            </div>
            <div
                onClick={props.onFilter}
                style={{
                    width: props.filter ? "0" : "100vw",
                    opacity: props.filter ? "0" : ".55"
                }}
                className="backFilterClick position-fixed"
            >
            </div>
        </div>
    );
}

const mapStateToProps = states => (
    {
        FilterRedux: states.Filter,
        RtlDir: states.Rtl,
        CheckSelected: states.CheckTypeSelected,
        CheckFormatSelected: states.CheckFormatSelected,
        CheckCreatorSelected: states.CheckCreatorSelected
    }
);
export default withRouter(connect(mapStateToProps)(SearchFilterMenu));
