import React, {useEffect} from 'react';
import connect from "react-redux/es/connect/connect";
import {Link, withRouter} from "react-router-dom";
import PropTypes from "prop-types";
import {Drawer, Icon, Menu, Spin} from "antd";
import './SidebarCategory.css';
import {
    SingleInBooksPageSlug,
    SingleInBooksPageTitle,
    SingleInArticlesPageTitle,
    SingleInArticlesPageSlug,
    SingleInJournalPageSlug,
    SingleInJournalPageTitle, SingleInCategoriesPageTitle, SingleInCategoriesPageSlug
} from "../../../actions";
import {Scrollbars} from "react-custom-scrollbars";
import {loadingItem} from "../../../config";
import {__} from "../../../global";


const {SubMenu} = Menu;

SidebarCategory.propTypes = {
    className: PropTypes.string
};


function SidebarCategory(props) {
    const {sidebarCollapse, location} = props;

    useEffect(()=>{
        props.dispatch(SingleInArticlesPageTitle(null));
        props.dispatch(SingleInArticlesPageSlug(null));
        props.dispatch(SingleInJournalPageTitle(null));
        props.dispatch(SingleInJournalPageSlug(null));
        props.dispatch(SingleInBooksPageTitle(null));
        props.dispatch(SingleInBooksPageSlug(null));
        if(location.pathname !== `/Categories`){
            props.dispatch(SingleInCategoriesPageTitle(null));
            props.dispatch(SingleInCategoriesPageSlug(null))
        }
    },[location.pathname]);

    return (
        <Drawer
            width={342}
            closable
            bodyStyle={{
                overflow: !sidebarCollapse && "hidden",
                padding: 0,
                zIndex: 100,
            }}
            mask={false}
            visible={!sidebarCollapse}
            onClose={() => props.setCategoryPosition(!sidebarCollapse)}
            style={{
                zIndex: 9
            }}
            className={props.className}
            placement={props.RtlDir ? "right" : "left"}
        >
            <div
                style={{
                    background: "#fff",
                    height: "100vh",
                    zIndex: "7",
                    position: 'relative'
                }}
                className={
                    `SidebarCategory-styles font-F-Gothic ${props.RtlDir ? "dir-rtl" : "dir-ltr"}`
                }
            >
                <Scrollbars
                    autoHide
                    style={{height: "100%", position: "relative"}}
                    className="category-scroller"
                >
                    <div className="d-flex align-items-center justify-content-between ">
                        <Link to={`/Categories`}>
                            <span className="CategorySide-title">
                                {__("Category")}
                            </span>
                        </Link>
                        <hr className="mt-1 mb-1 mx-0"/>
                    </div>
                    {!props.Loading
                        ?
                        <Menu
                            style={{width: "100%"}}
                            mode="inline"
                        >
                            {props.DataForSideBar && props.DataForSideBar.map(x => {
                                let count = 0;
                                x.children.nodes.map(x=>{
                                  return  x.count ? count += x.count : ""
                                });
                                return <SubMenu
                                    key={x.slug}
                                    onTitleClick={async () => {
                                        if (location.pathname === "/Books") {
                                            props.dispatch(SingleInBooksPageTitle(x.name));
                                            props.dispatch(SingleInBooksPageSlug(x.slug));
                                        } else if (location.pathname === "/Articles") {
                                            props.dispatch(SingleInArticlesPageTitle(x.name));
                                            props.dispatch(SingleInArticlesPageSlug(x.slug));
                                        } else if (location.pathname === "/Journals") {
                                            props.dispatch(SingleInJournalPageTitle(x.name));
                                            props.dispatch(SingleInJournalPageSlug(x.slug));
                                        }
                                    }
                                    }
                                    title={
                                        <div className="d-flex justify-content-between">
                                            <span>{x.name}</span>
                                            <span className="mx-4">({count})</span>
                                        </div>
                                    }
                                >
                                    {x.children.nodes.map(x => (
                                        location.pathname === "/" ?
                                            <Menu.Item
                                                key={x.slug}
                                            >
                                                <Link onClick={()=>{
                                                    props.dispatch(SingleInCategoriesPageTitle(x.name));
                                                    props.dispatch(SingleInCategoriesPageSlug(x.slug))
                                                }} to={{pathname:`/Categories`, state : {slug:x.slug,name:x.name} }}>
                                                    {x.name}
                                                </Link>
                                            </Menu.Item>
                                            :
                                            <Menu.Item
                                                key={x.slug}
                                                onClick={() => {
                                                    if (location.pathname === "/Books") {
                                                        props.dispatch(SingleInBooksPageTitle(x.name));
                                                        props.dispatch(SingleInBooksPageSlug(x.slug))
                                                    } else if (location.pathname === "/Articles") {
                                                        props.dispatch(SingleInArticlesPageTitle(x.name));
                                                        props.dispatch(SingleInArticlesPageSlug(x.slug))
                                                    } else if (location.pathname === "/Journals") {
                                                        props.dispatch(SingleInJournalPageTitle(x.name));
                                                        props.dispatch(SingleInJournalPageSlug(x.slug))
                                                    } else if (location.pathname === "/Categories") {
                                                        props.dispatch(SingleInCategoriesPageTitle(x.name));
                                                        props.dispatch(SingleInCategoriesPageSlug(x.slug))
                                                    }
                                                }}
                                            >
                                                {x.name}
                                            </Menu.Item>
                                    ))}
                                </SubMenu>
                            })}
                        </Menu>
                        :
                        <div
                            className="w-100 d-flex
                                 justify-content-center
                                 align-items-center spin"
                        >
                            <Spin
                                spinning
                                indicator={
                                    <img
                                        className="w-25 h-auto"
                                        src={loadingItem}
                                        alt="loading"
                                    />
                                }
                            />
                        </div>
                    }

                </Scrollbars>
            </div>

            {
                location.pathname !== "/Books" &&
                location.pathname !== "/Articles" &&
                location.pathname !== "/Journals" &&
                location.pathname !== "/Categories"
                    &&
            <Icon
                onClick={()=> props.setCategoryPosition(!sidebarCollapse)}
                style={{
                    position:"absolute",
                    top:"5.4rem",
                    left:props.RtlDir ? "1rem" : "",
                    right:props.RtlDir ? "" : "1rem",
                    height:"3rem",
                    color:"#000",
                    zIndex:"1003"
                }}
                type="close"
            />}
        </Drawer>
    );
}

const mapStateToProps = states => (
    {
        RtlDir: states.Rtl,
        DataForSideBar: states.DataForSideBar,
        Loading: states.LoadingForSidebar
    }
);
export default withRouter(connect(mapStateToProps)(SidebarCategory));