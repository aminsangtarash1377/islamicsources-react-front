import React, {useEffect, useState} from 'react';
import './ArticlesSubject.css';
import {TabSectionSingle, Article} from '../../Module';
import axios from "axios";
import {defaultRahUrl, loadingItem} from "../../../config";
import {Empty, Spin} from "antd";


export default function ArticlesSubject(props) {
    let [data] = useState([]);
    let [endCursor, SetEndCursor] = useState("");
    let [NextPage, SetNextPage] = useState(null);
    let [loading , SetLoading] = useState(false);

    async function GetInformation() {
        SetLoading(true);
        await axios.get(
            defaultRahUrl
            + (props.location.state ?
                `/category/articles?per_page=6&slug=${props.match.params.slug}&end=${endCursor}` :
                `/subject/articles?per_page=6&slug=${props.match.params.slug}&end=${endCursor}`))
            .then(result => {
                    result.data &&
                    result.data.articles &&
                    result.data.articles.data &&
                    result.data.articles.data.articles &&
                    result.data.articles.data.articles.nodes.map(x=>{
                       data.push(x);
                    });
                SetNextPage(
                    result.data &&
                    result.data.articles &&
                    result.data.articles.data &&
                    result.data.articles.data.articles &&
                    result.data.articles.data.articles.pageInfo &&
                    result.data.articles.data.articles.pageInfo.hasNextPage
                );
                SetEndCursor(
                    result.data &&
                    result.data.articles &&
                    result.data.articles.data &&
                    result.data.articles.data.articles &&
                    result.data.articles.data.articles.pageInfo &&
                    result.data.articles.data.articles.pageInfo.endCursor
                );
            });
        SetLoading(false);
    }

    useEffect(() => {
        GetInformation()
    }, []);

    return (
        <div className="w-100 d-flex flex-column
        justify-content-center align-items-center"
        >
            <TabSectionSingle title={props.location.title ? props.location.title : props.match.params.slug}/>
            <div className="w-100 d-flex flex-wrap mt-2 justify-content-between">
                {!loading ? data.length >0 ? data.map((x, i) => (
                    <Article key={i}
                             title={x.title}
                             slug={x.slug && x.slug}
                             rate={x.articlefields &&
                             (x.articlefields.rate === "INF"||x.articlefields.rate === null)
                                 ?
                                 "-"
                                 :
                                 x.articlefields.rate}
                             view={
                                 x.articlefields &&
                                 x.articlefields.view
                             }
                             author={
                                 x.articlefields &&
                                 x.articlefields.actAuthor &&
                                 x.articlefields.actAuthor[0].title
                             }
                    />
                ))
                    :
                    <div className="w-100 d-flex
                        justify-content-center align-items-center">
                        <Empty/>
                    </div>
                    :
                    <div
                        className="w-100 d-flex
                            justify-content-center
                            align-items-center spin"
                    >
                        <Spin
                            spinning
                            indicator={
                                <img
                                    className="w-25 h-auto"
                                    src={loadingItem}
                                    alt="loading"
                                />
                            }
                        />
                    </div>
                }
            </div>
            {NextPage &&
            <button
                className="font-F-Gothic cursor-p my-3 my-md-5 More"
                onClick={()=>GetInformation()}
            >More</button>
            }
        </div>
    );
}
