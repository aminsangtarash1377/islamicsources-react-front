import React, {useEffect, useState} from 'react';
import connect from "react-redux/es/connect/connect";
import {withRouter, useLocation, Link} from "react-router-dom";
import {Book, BookCollection, CardView, TabSection} from "../../Module";
import {Scrollbars} from "react-custom-scrollbars";
import axios from "axios";
import {defaultRahUrl, loadingItem} from "../../../config";
import {Spin} from 'antd';
import {DataForSideBar, LoadingForSidebar, Rtl} from "../../../actions";
import {__} from "../../../global";

function Books(props) {
    const Location = useLocation();
    const [data, SetData] = useState(null);
    const [show, setShow] = useState("book");
    let [dataSingle, SetDataSingle] = useState(null);
    const [endCursor, SetEndCursor] = useState("");
    let [slug, setSlug] = useState(null);
    let [NextPage, SetNextPage] = useState(null);
    let [Box, SetBox] = useState(null);
    let [BookData] = useState([]);
    let [ArticleData] = useState([]);
    let [MagazineData] = useState([]);
    let [loading, SetLoading] = useState(false);

    async function onClickShow(e) {
        await setShow(e.currentTarget.getAttribute("name"));
    }

    async function getInformation() {
        await props.dispatch(LoadingForSidebar(true));
        await axios.get(defaultRahUrl + "/categories?per_page_book=6&per_page_article=0&books=1&articles=0")
            .then(result => {
                if (result.status === 200) {
                    props.dispatch(DataForSideBar(
                        result.data &&
                        result.data.category &&
                        result.data.category.data &&
                        result.data.category.data.categories &&
                        result.data.category.data.categories.nodes &&
                        result.data.category.data.categories.nodes
                    ));
                    SetData(
                        result.data &&
                        result.data.category &&
                        result.data.category.data &&
                        result.data.category.data.categories &&
                        result.data.category.data.categories.nodes &&
                        result.data.category.data.categories.nodes);
                    SetBox(
                        result.data &&
                        result.data.subjectBox);
                    result.data &&
                    result.data.subjectBox &&
                    result.data.subjectBox.books &&
                    result.data.subjectBox.books.data &&
                    result.data.subjectBox.books.data.books &&
                    result.data.subjectBox.books.data.books.nodes &&
                    result.data.subjectBox.books.data.books.nodes.map(x => (
                        BookData.push(x)
                    ));
                    result.data &&
                    result.data.subjectBox &&
                    result.data.subjectBox.articles &&
                    result.data.subjectBox.articles.data &&
                    result.data.subjectBox.articles.data.articles &&
                    result.data.subjectBox.articles.data.articles.nodes &&
                    result.data.subjectBox.articles.data.articles.nodes.map(x => (
                        ArticleData.push(x)
                    ));
                    result.data &&
                    result.data.subjectBox &&
                    result.data.subjectBox.magazines &&
                    result.data.subjectBox.magazines.data &&
                    result.data.subjectBox.magazines.data.magazines &&
                    result.data.subjectBox.magazines.data.magazines.nodes &&
                    result.data.subjectBox.magazines.data.magazines.nodes.map(x => (
                        MagazineData.push(x)
                    ));
                }
            });
        await props.dispatch(LoadingForSidebar(false));
    }

    const tabs = [
        {
            title: __("Book"),
            mode: "book",
            data: BookData && BookData
        },
        {
            title: __("Article"),
            mode: "article",
            data: ArticleData && ArticleData
        },
        {
            title: __("Journal"),
            mode: "journal",
            data: MagazineData && MagazineData
        }
    ];

    async function getSingleData() {
        SetLoading(true);
        await axios.get(defaultRahUrl +
            `/category/books?per_page=18&slug=${props.SingleSlug}&end=${slug === props.SingleSlug ? endCursor : ""}`
        )
            .then(result => {
                if (result.status === 200) {
                    if (slug === props.SingleSlug) {
                        result.data.books.data.books.nodes.forEach(x => {
                            dataSingle.push(x);
                        });
                    } else {
                        SetDataSingle(
                            result.data &&
                            result.data.books &&
                            result.data.books.data &&
                            result.data.books.data.books &&
                            result.data.books.data.books.nodes &&
                            result.data.books.data.books.nodes);
                        SetEndCursor(
                            result.data &&
                            result.data.books &&
                            result.data.books.data &&
                            result.data.books.data.books &&
                            result.data.books.data.books.pageInfo &&
                            result.data.books.data.books.pageInfo.endCursor);
                    }
                    SetNextPage(
                        result.data &&
                        result.data.books &&
                        result.data.books.data &&
                        result.data.books.data.books &&
                        result.data.books.data.books.pageInfo &&
                        result.data.books.data.books.pageInfo.hasNextPage);
                    setSlug(props.SingleSlug);
                }
            });
        SetLoading(false);
    }

    useEffect(() => {
        !props.Single && getInformation();
        props.Single && getSingleData();
    }, [props.Single]);

    let tabletr = ["Title", "Writer", "Year", "Publisher", "Format", "Rate"];

    return (
        (
            Location.pathname.split("/")[2] === ""
            ||
            Location.pathname.split("/")[2] === undefined
        ) &&

        <React.Fragment>
            {props.Single !== null
                ?
                <div className="w-100 d-flex flex-column align-items-center">
                    <div className="mt-2 w-100">
                        <TabSection
                            title={props.Single}
                            IconCategory
                            hr
                            onClickShow={onClickShow}
                        />
                    </div>
                    {show === "book" &&
                    <div className="w-100 d-flex flex-row flex-wrap justify-content-between mt-4">
                        {!loading ? dataSingle && dataSingle.map((x, i) => (
                            <Book
                                key={i}
                                data={x}
                                popover="Book"
                                targetLocation="Books"
                                className="Book"
                            />
                        ))
                            :
                            <div
                                className="w-100 d-flex
                                 justify-content-center
                                 align-items-center spin"
                            >
                                <Spin
                                    spinning
                                    indicator={
                                        <img
                                            className="w-25 h-auto"
                                            src={loadingItem}
                                            alt="loading"
                                        />
                                    }
                                />
                            </div>
                        }
                    </div>
                    }
                    {show === "bars" &&
                    <div className="d-flex flex-row flex-wrap justify-content-between">
                        {
                            !loading ? dataSingle && dataSingle.map((x, i) => (
                                <CardView
                                    type="book"
                                    data={x}
                                    key={i}
                                    link="Books"
                                />
                            ))
                                :
                                <div
                                    className="w-100 d-flex
                                        justify-content-center
                                        align-items-center spin"
                                >
                                    <Spin
                                        spinning
                                        indicator={
                                            <img
                                                className="w-25 h-auto"
                                                src={loadingItem}
                                                alt="loading"
                                            />
                                        }
                                    />
                                </div>
                        }
                    </div>
                    }
                    {show === "table" &&
                    <table className="TableSubject w-100 mt-2">
                        <tr className="w-100">
                            <th className="font-F-Gothic"
                                style={{width: "20%"}}>
                                {tabletr[0]}
                            </th>
                            <th className="font-F-Gothic"
                                style={{width: "20%"}}>
                                {tabletr[1]}
                            </th>
                            <th className="font-F-Gothic"
                                style={{width: "15%"}}>
                                {tabletr[2]}
                            </th>
                            <th className="font-F-Gothic"
                                style={{width: "18%"}}>
                                {tabletr[3]}
                            </th>
                            <th className="font-F-Gothic"
                                style={{width: "13%"}}>
                                {tabletr[4]}
                            </th>
                            <th className="font-F-Gothic"
                                style={{width: "10%"}}>
                                {tabletr[5]}
                            </th>
                        </tr>
                        {!loading ?
                            dataSingle && dataSingle.map((x, i) => (
                                <Link key={i} to={`/Books/${x.slug}/`}>
                                    <tr className="w-100">
                                        <td
                                            style={{width: "20%"}}
                                            key={i}
                                            className="font-F-Gothic">
                                            <h4>{x.title}</h4>
                                        </td>
                                        <td
                                            style={{width: "20%"}}
                                            key={i}
                                            className="font-F-Gothic">
                                            <h4>
                                                {x.bookfields && x.bookfields.actAuthor
                                                    ?
                                                    x.bookfields.actAuthor
                                                    :
                                                    "---"
                                                }
                                            </h4>
                                        </td>
                                        <td
                                            style={{width: "15%"}}
                                            key={i}
                                            className="font-F-Gothic">
                                            <h4>
                                                {x.bookfields && x.bookfields.actPublishDate
                                                    ?
                                                    x.bookfields.actPublishDate
                                                    :
                                                    "---"
                                                }
                                            </h4>
                                        </td>
                                        <td
                                            style={{width: "18%"}}
                                            key={i}
                                            className="font-F-Gothic">
                                            <h4>
                                                {x.bookfields &&
                                                x.bookfields.actPublisher &&
                                                x.bookfields.actPublisher.title ?
                                                    x.bookfields.actPublisher.title : "---"
                                                }
                                            </h4>
                                        </td>
                                        <td
                                            style={{width: "13%"}}
                                            key={i}
                                            className="font-F-Gothic">
                                            <h4>
                                                {x.bookfields && x.bookfields.actFile ? x.bookfields.actFile.map(x => (
                                                    x.actFileFormat
                                                )) : "---"}
                                            </h4>
                                        </td>
                                        <td
                                            style={{width: "10%"}}
                                            key={i}
                                            className="font-F-Gothic">
                                            <h4>{x.bookfields && Math.round(x.bookfields.rate)}</h4>
                                        </td>
                                    </tr>
                                </Link>
                            ))
                            :
                            <div
                                className="w-100 d-flex
                                 justify-content-center
                                 align-items-center spin"
                            >
                                <Spin
                                    spinning
                                    indicator={
                                        <img
                                            className="w-25 h-auto"
                                            src={loadingItem}
                                            alt="loading"
                                        />
                                    }
                                />
                            </div>
                        }
                    </table>
                    }
                    {NextPage &&
                    <button
                        className="More my-3 my-md-5 font-F-Gothic"
                        onClick={async () => {
                            await getSingleData();
                        }}
                    >
                        {__("More")}
                    </button>
                    }
                </div>
                :
                <div className="py-5">
                    {data ? data.slice(0, 1).map((x, i) => (
                            <React.Fragment key={i}>
                                <TabSection
                                    hr
                                    more
                                    title={x.name}
                                    moreLink={{pathname: `/Subject/${x.slug}/Books/`, state: "Books", title: x.name}}
                                    className="my-2"
                                />
                                <Scrollbars
                                    className=
                                        "ScrollBooks
                                    Scrollbars-Suppress-vertical
                                    my-3"
                                >
                                    {x.books.data.books.nodes.map((x, i) => (
                                        <Book
                                            key={i}
                                            data={x}
                                            targetLocation="Books"
                                            className="Book"
                                            popover="Book"
                                        />
                                    ))}
                                </Scrollbars>
                            </React.Fragment>
                        ))
                        :
                        <div
                            className="w-100 d-flex
                                 justify-content-center
                                 align-items-center spin"
                        >
                            <Spin
                                spinning
                                indicator={
                                    <img
                                        className="w-25 h-auto"
                                        src={loadingItem}
                                        alt="loading"
                                    />
                                }
                            />
                        </div>
                    }
                    <div className="w-100 mt-3 mt-md-5">
                        {Box &&
                        <BookCollection
                            more
                            title={Box && Box.subjectName}
                            tabs={tabs}
                            hr={false}
                            moreLink="/"
                        />
                        }
                    </div>
                    <div className="w-100 mt-2 mt-lg-4 d-flex flex-column">
                        {data && data.slice(1, 4).map((x, i) => (
                            <React.Fragment key={i}>
                                <TabSection
                                    title={x.name}
                                    hr
                                    moreLink={{pathname: `/Subject/${x.slug}/Books/`, state: "Books", title: x.name}}
                                    more
                                    className="my-2"
                                />
                                <Scrollbars className="ScrollBooks Scrollbars-Suppress-vertical my-3">
                                    {x.books.data.books.nodes.map((x, i) => (
                                        <Book
                                            key={i}
                                            data={x}
                                            targetLocation="Books"
                                            popover="Book"
                                            className="Book"
                                        />
                                    ))}
                                </Scrollbars>
                            </React.Fragment>
                        ))}
                    </div>
                </div>
            }
        </React.Fragment>
    )
}

const mapStateToProps = states => (
    {
        RtlDir: states.Rtl,
        Single: states.SingleInBooksPageTitle,
        SingleSlug: states.SingleInBooksPageSlug
    }
);
export default withRouter(connect(mapStateToProps)(Books));