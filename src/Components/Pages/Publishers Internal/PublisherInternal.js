import React, {useEffect, useState} from 'react';
import "./PublisherInternal.css";
import {LocationAndContect, BoxInformation, TabSection, Book, SpinLoading} from "../../Module";
import connect from "react-redux/es/connect/connect";
import {withRouter} from "react-router-dom";
import axios from "axios";
import {defaultRahUrl} from "../../../config";
import {Scrollbars} from "react-custom-scrollbars";
import {Empty} from "antd";
import {__} from "../../../global";

function PublisherInternal(props) {

    const [data, Setdata] = useState(null);
    const [books, setBooks] = useState(null);
    const [loading, setLoading] = useState(null);

    async function getInformation() {
        setLoading(true);
        await axios.get(defaultRahUrl + `/publisher?slug=${props.match.params.slug}`)
            .then(result => {
                if (result.status === 200) {
                    Setdata(result.data.publisher &&
                        result.data.publisher.data &&
                        result.data.publisher.data.publisherBy);
                    setBooks(result.data.publisherBooks && result.data.publisherBooks);
                }
            });
        setLoading(false);
    }

    useEffect(
        () => {
            getInformation()
        },
        []
    );
    return (
        <div className="w-100 d-flex flex-column">
            <BoxInformation
                data={data}
                Logo="PublisherInternal"
                StartingTime="Launched:"
                loading={loading}
            />
            <div className="mt-5">
                <TabSection
                    hr
                    more
                    title={__("Publishing books")}
                    moreLink={``}
                />
            </div>
            {
                <SpinLoading
                    loading={loading}
                    mode="withoutBox"
                >
                    <Scrollbars
                        className="ScrollBooks Scrollbars-Suppress-vertical"
                    >
                        {
                            books &&
                            books.length !== 0 ?
                                books.map((x, i) => (
                                    <Book
                                        key={i}
                                        data={x}
                                        popover="Book"
                                        targetLocation="Books"
                                        className="Book"
                                    />
                                ))
                                :
                                <Empty
                                    className="
                                    w-100 d-flex
                                    flex-column
                                    justify-content-center
                                    align-items-center"
                                    style={{minHeight: "30vh"}}
                                />
                        }
                    </Scrollbars>
                </SpinLoading>
            }

            <div className="d-none d-lg-block mt-4">
                <LocationAndContect data={data && data}/>
            </div>
        </div>
    )
}

const mapStateToProps = states => (
    {
        RtlDir: states.Rtl
    }
);
export default withRouter(connect(mapStateToProps)(PublisherInternal));