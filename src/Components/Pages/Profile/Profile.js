import React, {useEffect, useState} from 'react';
import {useHistory} from 'react-router-dom';
import {BookCollection, ProfileBox, TabSection, SubmitArticle, ArticleHistoryPost, SpinLoading} from "../../Module";
import {defaultWPUrl, ISAccessUserToken, PostArticleConverter, PostBookConverter} from "../../../config";
import axios from "axios";
import {Empty} from "antd";
import {__} from "../../../global";
import "./Profile.css";

export default function Profile() {
    const [bookMarkData, setBookMarkData] = useState([]);
    const [bookMarkLoading, setBookMarkLoading] = useState(true);
    const [postHistory, setPostHistory] = useState([]);
    const [postHistoryLoading, setPostHistoryLoading] = useState(true);


    useEffect(() => {
        axios.get(defaultWPUrl + '/users/bookMarks', {headers: {Authorization: ISAccessUserToken}})
            .then(res => {
                if (res.status === 200) {
                    setBookMarkData(res.data.data);
                }
                setBookMarkLoading(false);
            })
            .catch(res => {
                setBookMarkLoading(false);
            });
        axios.get(defaultWPUrl + '/posts/ByUser', {headers: {Authorization: ISAccessUserToken}})
            .then(res => {
                if (res.status === 200) {
                    setPostHistory(res.data.data.map(data => ({
                        post_title: data.post_title,
                        writer: data.meta_fields.writer,
                        date: data.post_date.split(' ')[0].replace(/-/g, "/")
                    })));
                    setPostHistoryLoading(false);
                }
            })
    }, []);

    const tabs = [
        {
            title: __("Book"),
            mode: "book",
            data: bookMarkData.filter(val => val.post_type === "book").map(data => PostBookConverter(data))
        },
        {
            title: __("Article"),
            mode: "article",
            data: bookMarkData.filter(val => val.post_type === "article").map(data => PostArticleConverter(data))
        },
        {
            title: __("Journal"),
            mode: "journal",
            data: bookMarkData.filter(val => val.post_type === "magazine").map(data => PostBookConverter(data))
        }
    ];


    const history = useHistory();
    return (
        <div className="d-flex flex-column">
            <ProfileBox/>
            <SpinLoading
                loading={bookMarkLoading}
            >
                <BookCollection
                    hr
                    more={false}
                    title={__('Marked titles')}
                    name="books"
                    tabs={tabs}
                />
            </SpinLoading>
            <SubmitArticle
                message="Your post successfully sent to admins"
                history={history}
            />
            <SpinLoading loading={postHistoryLoading}>
                <div className="mt-3 mt-lg-5">
                    <TabSection
                        hr
                        more={false}
                        title={__("Last Submitted Post")}
                    />
                    <div
                        className={`
                         d-flex 
                         flex-column 
                         flex-lg-row
                         flex-lg-wrap
                         justify-content-${
                            postHistory.length === 0 ?
                                "center"
                                :
                                "between"
                            } 
                                mt-3
                                `
                        }
                        style={{minHeight: "5rem"}}
                    >
                        {
                            postHistory.length === 0 ?
                                <div
                                    className="
                                    w-100
                                    d-flex
                                    justify-content-center
                                    align-items-center
                                    spin"
                                >
                                    <Empty/>
                                </div>
                                :
                                postHistory.map((data, i) => (
                                    <div className="w-50 px-3 mb-4">
                                        <ArticleHistoryPost
                                            data={data}
                                            key={i}
                                        />
                                    </div>
                                ))
                        }
                    </div>
                </div>
            </SpinLoading>
        </div>
    )
}
