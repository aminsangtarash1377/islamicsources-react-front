import React from 'react';
import connect from "react-redux/es/connect/connect";
import {withRouter} from "react-router-dom";
import {ImageBoxInMediaPage, SoundBoxInMediaPage} from "../../Module";
import './MediaPage.css';


function MediaPage(props) {

    return (
        <div>
            <ImageBoxInMediaPage/>
            <SoundBoxInMediaPage/>
        </div>
    )
}

const mapStateToProps = states => (
    {
        RtlDir: states.Rtl,
    }
);
export default withRouter(connect(mapStateToProps)(MediaPage));