import React, {useEffect, useState} from 'react';
import connect from "react-redux/es/connect/connect";
import {withRouter} from "react-router-dom";
import {Col, Row, Rate, Spin, Icon} from "antd";
import scan from '../../../images/scan.png'
import {message} from "antd";
import PropTypes from "prop-types";
import {BookCollection, SpinLoading} from "../../Module";
import defaultImage from "../../../images/defaultMagazine.png"
import {defaultRahUrl, defaultWPUrl, ISAccessUserToken, loadingItem, setRateByUser} from "../../../config";
import {__} from "../../../global";
import axios from "axios";
import "./SinlgeJournal.css";
import BookMarkTrue from "../../../images/neshan A.svg";
import BookMarkFalse from "../../../images/neshan.svg";

InformationItem.propTypes = {
    name: PropTypes.string.isRequired,
    response: PropTypes.string.isRequired
};

function InformationItem(props) {
    return (
        <div className="px-3 px-md-0 d-flex justify-content-start my-1">
            <span
                className="text-bold font-color-Information-text InformationItemTitle"
            >
                {props.name}:
            </span>
            <span
                className="mx-1 text-black font-color-Information-text InformationItemValue"
            >
                {props.response}
            </span>
        </div>
    )
}

function SingleJournal(props) {

    const [data, setData] = useState(null);
    let [dataSimilarBook] = useState([]);
    let [dataSimilarArticle] = useState([]);
    let [dataSimilarMagazines] = useState([]);
    let [AmountPoint, SetAmountPoint] = useState(null);
    let [loading, SetLoading] = useState(true);
    let [loadingRate, SetLoadingRate] = useState(false);
    const [bookMark, SetBookMark] = useState(false);

    const tabs = [
        {
            title: __("Book"),
            mode: "book",
            data: dataSimilarBook
        },
        {
            title: __("Article"),
            mode: "article",
            data: dataSimilarArticle
        },
        {
            title: __("Journal"),
            mode: "journal",
            data: dataSimilarMagazines
        }
    ];

    function getData() {
        axios.get(defaultRahUrl + `/magazine?slug=${props.match.params.slug}`)
            .then(result => {
                console.log(result);
                setData(
                    result.data.magazine.data &&
                    result.data.magazine.data.magazine
                );

                result.data &&
                result.data.similarActs &&
                result.data.similarActs.data &&
                result.data.similarActs.data.books &&
                result.data.similarActs.data.books.nodes.map((d, i) => {
                    return dataSimilarBook.push(
                        {
                            featuredImage: {sourceUrl: d.featuredImage && d.featuredImage.sourceUrl},
                            title: d.title,
                            slug: d.slug,
                            key: i,
                            bookfields: {
                                rate: d.bookfields.rate,
                                actPublishDate: d.bookfields.actPublishDate,
                                view: d.bookfields.view
                            }
                        }
                    );
                });
                result.data &&
                result.data.similarActs &&
                result.data.similarActs.data &&
                result.data.similarActs.data.articles &&
                result.data.similarActs.data.articles.nodes.map((d, i) => {
                    return dataSimilarArticle.push(
                        {
                            title: d.title,
                            slug: d.slug,
                            articlefields: {
                                rate: d.articlefields.rate,
                                // actPublishDate:d.articlefields.actPublishDate,
                                view: d.articlefields.view
                            }
                        }
                    );
                });
                result.data &&
                result.data.similarActs &&
                result.data.similarActs.data &&
                result.data.similarActs.data.magazines &&
                result.data.similarActs.data.magazines.nodes.map((d, i) => {
                    return dataSimilarMagazines.push(
                        {
                            featuredImage: {sourceUrl: d.featuredImage && d.featuredImage.sourceUrl},
                            title: d.title,
                            slug: d.slug,
                            magazinefields: {
                                rate: d.magazinefields.rate,
                                // actPublishDate:d.magazinefields.actPublishDate,
                                view: d.magazinefields.view
                            }
                        }
                    );
                });
            });
        SetLoading(false);
    }

    /**
     * @return {string}
     */
    function Get_Field(data) {
        if (typeof data.value === "string") {
            return data.value
        } else if (data.value.length) {
            return data.value.map(x => {
                return x.post_title
            }).join()
        } else if (!data.value.length) {
            return Object.values(data.value).filter(val => val !== "").join()
        }
    }

    async function setValue(e, id) {
        SetLoadingRate(true);
        if(setRateByUser(data && data.magazineId)){
        await axios.post(defaultRahUrl +
            `/rate?type=post&id=${id}&rate=${e}`)
            .then(result => {
                if (result.status === 200) {
                    message.success("Done");
                    result.data.rate > 0.5 ?
                        SetAmountPoint(Math.round(result.data.rate))
                        :
                        SetAmountPoint(1);
                }
            });
        }else{
            message.warning(__('You rate this post once'))
        }
        SetLoadingRate(false);
    }

    async function SetBookMarking(PostId) {
        await axios.post(defaultWPUrl + `/users/BookMarks`,
            {"PostId": PostId},
            {
                headers: {
                    "Content-Type": "application/json",
                    Authorization: ISAccessUserToken
                }
            }
        )
            .then(result => {
                if (result.status === 200) {
                    SetBookMark(result.data.data);
                    message.success(__('Done'));
                }
            })
            .catch(res => {
                if (res.response && res.response.data.message) {
                    message.error(__(res.response.data.message))
                }
            });
    }

    useEffect(() => {
        getData()
    }, []);

    return (
        <React.Fragment>
            {!loading
                ?
                <div className="d-flex flex-column">
                    <h3
                        className="
                        text-bold
                        color-Header-responsive
                        d-lg-none
                        d-block"
                    >
                        {data && data.title}
                    </h3>
                    <Row
                        className="
                        d-flex
                        flex-row
                        border
                        justify-content-between
                        align-items-center
                        mt-2
                        BookInfoInformation
                        "
                    >
                        <Col
                            span={10}
                            lg={6}
                            className="d-flex justify-content-center align-items-center"
                        >
                            <img
                                style={{width: "100%", maxHeight: "100%"}}
                                src={
                                    data &&
                                    data.featuredImage &&
                                    data.featuredImage.mediaDetails &&
                                    data.featuredImage.mediaDetails.sizes.slice(-1)[0] &&
                                    data.featuredImage.mediaDetails.sizes.slice(-1)[0].sourceUrl ?
                                        data.featuredImage.mediaDetails.sizes.slice(-1)[0].sourceUrl :
                                        defaultImage
                                }
                                alt="book Islamic Sources"
                            />
                        </Col>
                        <Col
                            span={14}
                            lg={14}
                            className="InformationBookInfo font-F-Gothic"
                        >
                            <h3 className="text-bold font-color-Information-text d-lg-block d-none">
                                {
                                    data &&
                                    data.title
                                }
                            </h3>
                            <Row
                                type="flex"
                                className="justify-content-lg-start
                                justify-content-center pt-2
                                parentValuesToShow
                                "
                            >
                                <Col
                                    span={20}
                                    lg={24}
                                    className="w-100 h-100 d-flex flex-column flex-wrap"
                                >
                                    {data &&
                                    data.valuesToShow.map((data, i) => {
                                        if (data.value)
                                            return (
                                                <InformationItem
                                                    key={i}
                                                    name={data.label}
                                                    response={Get_Field(data)}
                                                />
                                            )
                                    })
                                    }
                                </Col>
                            </Row>
                            <Row type="flex" align="middle" className="pt-3 d-lg-flex d-none">
                                <SpinLoading mode="loading" loading={loadingRate}>
                                    <Rate
                                        value={
                                            AmountPoint ?
                                                AmountPoint : data &&
                                                data.magazinefields &&
                                                data.magazinefields.rate
                                        }
                                        onChange={(e) => setValue(e, data && data.magazineId)}
                                        character={
                                            <div
                                                className="position-relative "
                                                style={{
                                                    width: "1rem",
                                                    height: "1rem",
                                                }}
                                            >
                                                <div
                                                    style={{
                                                        width: ".9rem",
                                                        height: ".9rem",
                                                        background: "#b3b3b3",
                                                        borderRadius: "2px"
                                                    }}
                                                    className="RatingIslamicSources position-absolute"
                                                />
                                                <div
                                                    style={{
                                                        width: ".9rem",
                                                        height: ".9rem",
                                                        background: "#b3b3b3",
                                                        transform: "rotate(45deg)",
                                                        borderRadius: "2px"
                                                    }}
                                                    className="RatingIslamicSources position-absolute"
                                                />
                                            </div>
                                        }
                                        allowHalf={false}
                                    />
                                </SpinLoading>
                                <span
                                    className="mx-2 text-black"
                                    style={{fontSize: ".75rem"}}
                                >
                            {`(
                                ${
                            data &&
                            data.magazinefields &&
                            data.magazinefields.votes
                                } ${(__("vote"))})`
                            }
                        </span>
                                <img
                                    src={bookMark ? BookMarkTrue : BookMarkFalse}
                                    className="mx-2 h-100 cursor-p"
                                    alt=""
                                    onClick={() => SetBookMarking(
                                        data && data.magazineId
                                    )}
                                    style={{width: ".65rem"}}
                                />
                                <div className="ShareIcon mx-2 d-flex align-items-center cursor-p">
                                    <Icon type="share-alt"/>
                                </div>
                            </Row>
                        </Col>
                        <Col
                            span={24}
                            lg={4}
                            className="d-lg-flex d-none align-items-center justify-content-start"
                        >
                            <div className="d-flex flex-column align-items-center">
                                <img
                                    src={
                                        data &&
                                        data.magazinefields &&
                                        data.magazinefields.actQrCode ?
                                            data.magazinefields.actQrCode :
                                            scan
                                    }
                                    alt="scan book"
                                    width={140}
                                    className="mb-1"
                                />
                                <a
                                    href={`/${data &&
                                    data.magazinefields &&
                                    data.magazinefields.actFile &&
                                    data.magazinefields.actFile.actFileDoc}`}
                                    download={`/${data &&
                                    data.magazinefields &&
                                    data.magazinefields.actFile &&
                                    data.magazinefields.actFile.actFileDoc}`}
                                    className="
                                    btn-Book-study
                                    btn
                                    d-flex
                                    justify-content-center
                                    font-F-Gothic
                                    b-flat
                                    px-3-5
                                    mt-3
                            "
                                >
                                    {(__("Download"))}
                                </a>
                                <button
                                    className="
                                    btn-Book-study
                                    btn
                                    d-flex
                                    justify-content-center
                                    font-F-Gothic
                                    b-flat
                                    px-3-5
                                    mt-3
                            "
                                >
                                    {__("Online Study")}
                                </button>
                            </div>
                        </Col>
                    </Row>
                    <Row
                        type="flex"
                        align="middle"
                        className="px-2 mt-5 d-lg-none d-flex"
                    >
                        <SpinLoading mode="loading" loading={loadingRate}>
                            <Rate
                                value={ AmountPoint ?
                                    AmountPoint : data &&
                                    data.magazinefields &&
                                    data.magazinefields.rate
                                }
                                onChange={(e) => setValue(e, data && data.magazineId)}
                                character={
                                    <div
                                        className="position-relative "
                                        style={{
                                            width: "1.5rem",
                                            height: "1rem",
                                        }}
                                    >
                                        <div
                                            style={{
                                                width: "1.20rem",
                                                height: "1.20rem",
                                                background: "#b3b3b3",
                                                borderRadius: "2px"
                                            }}
                                            className="RatingIslamicSources position-absolute"
                                        />
                                        <div
                                            style={{
                                                width: "1.20rem",
                                                height: "1.20rem",
                                                background: "#b3b3b3",
                                                transform: "rotate(45deg)",
                                                borderRadius: "2px"
                                            }}
                                            className="RatingIslamicSources position-absolute"
                                        />
                                    </div>
                                }
                                allowHalf={false}
                            />
                        </SpinLoading>
                        <span
                            className="mx-2 text-black"
                            style={{fontSize: "1rem"}}
                        >
                            {`(
                                ${
                            data &&
                            data.magazinefields &&
                            data.magazinefields.votes
                                } ${(__("vote"))})`
                            }
                        </span>
                        <img
                            src={bookMark ? BookMarkTrue : BookMarkFalse}
                            className="mx-2 h-100 cursor-p"
                            alt=""
                            onClick={() => SetBookMarking(
                                data && data.magazineId
                            )}
                            style={{width: ".65rem"}}
                        />
                        <div className="ShareIcon mx-2 d-flex align-items-center cursor-p">
                            <Icon type="share-alt"/>
                        </div>
                    </Row>
                    <Row
                        type="flex"
                        justify="space-between"
                        align="middle"
                        className="mt-5 d-lg-none justify-content-between align-items-center D-letter"
                    >
                        <a
                            href={`/${data &&
                            data.magazinefields &&
                            data.magazinefields.actFile &&
                            data.magazinefields.actFile.actFileDoc}`}
                            download={`/${data &&
                            data.magazinefields &&
                            data.magazinefields.actFile &&
                            data.magazinefields.actFile.actFileDoc}`}
                            className="
                            btn-Book-studyInResponsive
                            btn
                            d-flex
                            justify-content-center
                            align-items-center
                            font-F-Gothic
                            b-flat
                            px-3-5
                            mt-3
                            "
                        >
                            {(__("Download"))}
                        </a>
                        <button
                            className="
                            btn-Book-studyInResponsive
                            btn
                            d-flex
                            justify-content-center
                            align-items-center
                            font-F-Gothic
                            b-flat
                            px-3-5
                            mt-3
                            "
                        >
                            {__("Online Study")}
                        </button>
                    </Row>
                </div>
                :
                <div className="w-100 d-flex
                        justify-content-center align-items-center spin"
                >
                    <Spin
                        indicator={
                            <img className="w-25 h-auto" src={loadingItem} alt="loading"/>
                        }
                    />
                </div>
            }
            <Row className="my-5 Introduction-text-book">
                <h2
                    className="font-F-Gothic text-bold"
                >
                    {(__("Introduction"))}
                </h2>
                <div
                    className="font-F-Gothic"
                    dangerouslySetInnerHTML={{
                        __html:
                            data && data.content
                    }}
                />
            </Row>
            <BookCollection
                title={__("Similar titles")}
                hr
                tabs={tabs}
                name="books"
            />
        </React.Fragment>
    )
}

const mapStateToProps = states => (
    {
        RtlDir: states.Rtl
    }
);
export default withRouter(connect(mapStateToProps)(SingleJournal));
