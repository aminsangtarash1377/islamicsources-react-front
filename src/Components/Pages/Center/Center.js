import React, {useEffect, useState} from 'react';
import {TabSection, Book} from "../../Module";
import axios from "axios";
import {defaultRahUrl, loadingItem} from "../../../config";
import {useLocation} from "react-router-dom";
import {Empty, Spin} from "antd";
import {__} from "../../../global";

export default function Centers() {
    const [data] = useState([]);
    let [endCursor, SetEndCursor] = useState("");
    let [NextPage, SetNextPage] = useState(false);
    let [loading, SetLoading] = useState(false);

    const Location = useLocation();

    async function GetInformation() {
        SetLoading(true);
        await axios.get(defaultRahUrl + `/centers?per_page=10&end=${endCursor}`)
            .then(result => {
                if (result.status === 200) {
                    result.data &&
                    result.data.centers.data.centers.nodes.map(x => {
                        data.push(x)
                    });
                    SetNextPage(
                        result.data &&
                        result.data.centers.data.centers.pageInfo &&
                        result.data.centers.data.centers.pageInfo.hasNextPage
                    );
                    SetEndCursor(
                        result.data &&
                        result.data.centers.data.centers.pageInfo &&
                        result.data.centers.data.centers.pageInfo.endCursor
                    )
                }
            });
        SetLoading(false);
    }

    useEffect(() => {
        GetInformation()
    }, []);


    return (
        (
            Location.pathname.split("/")[2] === ""
            ||
            Location.pathname.split("/")[2] === undefined
        ) &&
        <div className="w-100">
            <div className="w-100 mt-5">
                <TabSection
                    title={__("Centers")}
                    hr
                    more={false}
                />
            </div>
            <div
                className="w-100 d-flex flex-wrap
                justify-content-between my-4"
            >
                {!loading ? data.length > 0 ? data.map((x, i) => (
                        <Book
                            data={x}
                            key={i}
                            targetLocation="Publishers"
                            popover="Person"
                            className="Book"
                        />
                    ))
                    :
                    <div
                        className="w-100 d-flex
                            justify-content-center
                            align-items-center"
                    >
                        <Empty/>
                    </div>
                    :
                    <div
                        className="w-100 d-flex
                            justify-content-center
                            align-items-center spin"
                        style={{minHeight: "40vh"}}
                    >
                        <Spin
                            spinning
                            indicator={
                                <img
                                    className="w-25 h-auto"
                                    src={loadingItem}
                                    alt="loading"
                                />
                            }
                        />
                    </div>
                }
            </div>
            {NextPage &&
            <div className="w-100 d-flex justify-content-center mb-5">
                <button
                    className="More font-F-Gothic"
                    onClick={() => GetInformation()}
                >
                    {__("More")}
                </button>
            </div>
            }
        </div>
    )
}