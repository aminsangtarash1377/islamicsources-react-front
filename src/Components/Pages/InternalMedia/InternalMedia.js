import React from 'react';
import connect from "react-redux/es/connect/connect";
import {withRouter} from "react-router-dom";
import {MediaBox, TabSection} from '../../Module';
import one from '../../../images/pic/Picture Media A.jpg';
import two from '../../../images/pic/Picture Media B.jpg';
import three from '../../../images/pic/Picture Media C.jpg';
import four from '../../../images/pic/Picture Media D.jpg';
import five from '../../../images/pic/Picture Media E.jpg';
import six from '../../../images/pic/Picture Media F.jpg';
import seven from '../../../images/pic/Picture Media G.jpg';
import './InternalMedia.css';


function InternalMedia(props) {

    const array = [
        {
          title:"Dont be Sad",
          src:one
        },
        {
            title:"Da Iran War",
            src:two
        },
        {
            title:"Unity in Islam",
            src:three
        },
        {
            title:"Life in the eyes of Islam",
            src:four
        },
        {
            title:"Study in Islam",
            src:five
        },
        {
            title:"Da IraSuranin War",
            src:six
        },
        {
            title:"Unnaderi ity in Islam",
            src:seven
        },
        {
            title:"Dont be Sad",
            src:one
        },
        {
            title:"Da Iran War",
            src:two
        },
        {
            title:"Unity in Islam",
            src:three
        },
        {
            title:"Life in the eyes of Islam",
            src:four
        },
        {
            title:"Study in Islam",
            src:five
        },
        {
            title:"Da IraSuranin War",
            src:six
        }
    ];

    return (
        <div>
            <TabSection title="Picture Category" hr/>
            <div
                className="d-flex flex-column flex-lg-row
                           flex-lg-wrap w-100 mt-4"
            >
                {array.map((x,i)=>(
                    <MediaBox
                        src={x.src}
                        title={x.title}
                        key={i}
                    />
                ))
                }
            </div>
        </div>
    )
}

const mapStateToProps = states => (
    {
        RtlDir: states.Rtl,
        Single: states.SingleInCategoriesPageTitle,
        SingleSlug: states.SingleInCategoriesPageSlug
    }
);
export default withRouter(connect(mapStateToProps)(InternalMedia));