import React, {useEffect, useState} from 'react';
import {Book, TabSection, Article, Subject} from "../../Module";
import {Scrollbars} from "react-custom-scrollbars";
import axios from "axios";
import {defaultRahUrl, loadingItem} from "../../../config";
import connect from "react-redux/es/connect/connect";
import {withRouter} from "react-router-dom";
import {Empty, Spin} from "antd";
import {__} from "../../../global";


function InternalSubject(props) {
    let [dataOne] = useState([]);
    let [book, SetBook] = useState(null);
    let [article, SetArticle] = useState(null);
    const [endCursor, SetEndCursor] = useState("");
    let [loading, SetLoading] = useState(true);
    let [NextPage, SetNextPage] = useState(null);


    async function getInformation1() {
        SetLoading(true);
        await axios.get(
            defaultRahUrl + `/subjects?per_page=9&end=${endCursor}&parent=${props.Parent}`)
            .then(result => {
                if (result.status === 200) {
                        result.data &&
                        result.data.subjects &&
                        result.data.subjects.data &&
                        result.data.subjects.data.topics &&
                        result.data.subjects.data.topics.nodes &&
                        result.data.subjects.data.topics.nodes.map(x=>{
                            dataOne.push(x);
                        });
                        SetNextPage(
                            result.data &&
                            result.data.subjects &&
                            result.data.subjects.data &&
                            result.data.subjects.data.topics &&
                            result.data.subjects.data.topics.pageInfo &&
                            result.data.subjects.data.topics.pageInfo.hasNextPage
                        );
                        SetEndCursor(
                            result.data &&
                            result.data.subjects &&
                            result.data.subjects.data &&
                            result.data.subjects.data.topics &&
                            result.data.subjects.data.topics.pageInfo &&
                            result.data.subjects.data.topics.pageInfo.endCursor
                        )
                }
            });
        SetLoading(false);
    }

    async function getInformation2() {
        SetLoading(true);
        await axios.get(
            defaultRahUrl + `/subject?per_page_book=7&per_page_article=6&slug=${props.match.params.slug}`
        )
            .then(result => {
                if (result.status === 200) {
                    SetBook(result.data &&
                        result.data.subject &&
                        result.data.subject.data &&
                        result.data.subject.data.books &&
                        result.data.subject.data.books.nodes);
                    SetArticle(result.data &&
                        result.data.subject &&
                        result.data.subject.data &&
                        result.data.subject.data.articles &&
                        result.data.subject.data.articles.nodes);
                }
            });
        SetLoading(false);
    }

    useEffect(() => {
        getInformation1() && getInformation2()
    }, []);
    return (
        <div className="w-100">
            {dataOne && dataOne.length !== 0 ?
                <div className="w100 d-flex flex-column
                justify-content-center align-items-center"
                >
                    <div
                        className="d-flex flex-wrap mt-1 mt-lg-4
                    justify-content-between"
                    >
                        {!loading ? dataOne && dataOne.map((x, i) => (
                            <Subject
                                data={x}
                                key={i}
                            />
                        ))
                        :
                            <div
                                className="w-100 d-flex
                                justify-content-center align-items-center spin"
                            >
                                <Spin
                                    indicator={
                                        <img className="w-25 h-auto" src={loadingItem} alt="loading" />
                                    }
                                />
                            </div>
                        }
                    </div>
                    {NextPage &&
                    <button
                        className="More my-3 my-md-5 font-F-Gothic"
                        onClick={() => getInformation1()}
                    >
                        {__("More")}
                    </button>
                    }
                </div>
                :
                <div className="d-flex flex-column w-100 mt-5">
                    <TabSection
                        title="Books"
                        more={true}
                        hr={true}
                        moreLink={`/Subject/${props.match.params.slug}/Books/`}
                    />
                    <div className="w-100 mt-4">
                        <Scrollbars className="ScrollBooks Scrollbars-Suppress-vertical">
                            {!loading ? book && book.length > 0 ? book.map((x, i) => (
                                <Book
                                    location="Books"
                                    key={i}
                                    data={x}
                                    targetLocation="Books"
                                    popover="Book"
                                    className="Book"
                                />
                            ))
                                :
                                <div
                                    className="w-100 d-flex
                                    justify-content-center
                                    align-items-center spin"
                                >
                                    <Empty/>
                                </div>
                                :
                                <div
                                    className="w-100 d-flex
                                justify-content-center align-items-center spin"
                                >
                                    <Spin
                                        indicator={
                                            <img className="w-25 h-auto" src={loadingItem} alt="loading" />
                                        }
                                    />
                                </div>
                            }
                        </Scrollbars>
                    </div>
                    <div className="w-100 mt-4">
                        <TabSection
                            title="Articles"
                            hr
                            more
                            moreLink={`/Subject/${props.match.params.slug}/Articles/`}
                        />
                        <div
                            className="w-100 d-flex flex-wrap
                               justify-content-between my-4"
                        >
                            {!loading ? article && article.length > 0 ? article.map((x, i) => (
                                <Article
                                    key={i}
                                    link="Articles"
                                    title={x.title && x.title}
                                    slug={x.slug && x.slug}
                                    view={x.articlefields && x.articlefields.view}
                                    rate={x.articlefields && x.articlefields.rate}
                                    author={
                                        x.articlefields &&
                                        x.articlefields.actAuthor &&
                                        x.articlefields.actAuthor[0] &&
                                        x.articlefields.actAuthor[0].title
                                    }
                                />
                            ))
                                :
                                <div
                                    className="w-100 d-flex
                                    justify-content-center align-items-center spin"
                                >
                                    <Empty/>
                                </div>
                                :
                                <div
                                    className="w-100 d-flex
                                    justify-content-center align-items-center spin"
                                >
                                    <Spin
                                        indicator={
                                            <img className="w-25 h-auto" src={loadingItem} alt="loading" />
                                        }
                                    />
                                </div>
                            }
                        </div>
                    </div>
                </div>
            }
        </div>
    )
}

const mapStateToProps = states => (
    {
        Parent: states.Parent
    }
);
export default withRouter(connect(mapStateToProps)(InternalSubject));