import React, {useEffect, useState} from 'react';
import {TabSection, Book} from "../../Module";
import axios from "axios";
import {Button, Empty, Spin} from "antd";
import {useLocation} from "react-router-dom";
import {defaultRahUrl, loadingItem} from "../../../config";
import {__} from "../../../global";
import './Persons.css';


export default function Persons() {
    let [ContentDefault, SetContentDefault] = useState(null);
    const [content, onTabChange] = useState(null);
    const [endCursor, setEndCursor] = useState(null);
    let [tabs, setTabs] = useState(null);
    let [type, setType] = useState(null);
    let [NextPage, SetNextPage] = useState(false);
    let [loading, SetLoading] = useState(true);

    async function getInformation(Type, end) {
        SetLoading(true);
        let array = [];
            await axios.get(
                defaultRahUrl + `/persons?per_page=21&${Type ? `type="${Type}"` : ""}&end=${end ? end : ""}`
            )
                .then(result => {
                    if (result.status === 200) {
                        if (type === Type) {
                            result.data &&
                            result.data.persons &&
                            result.data.persons.data &&
                            result.data.persons.data.persons &&
                            result.data.persons.data.persons.nodes.map(x => {
                                content.push(x);
                            })
                        } else {
                            onTabChange(
                                result.data &&
                                result.data.persons &&
                                result.data.persons.data &&
                                result.data.persons.data.persons &&
                                result.data.persons.data.persons.nodes
                            );
                        }
                        SetNextPage(
                            result.data &&
                            result.data.persons &&
                            result.data.persons.data &&
                            result.data.persons.data.persons &&
                            result.data.persons.data.persons.pageInfo &&
                            result.data.persons.data.persons.pageInfo.hasNextPage
                        );
                        array.push({
                            title: __("All"),
                            mode: ""
                        });
                        result.data.types.map((x) => {
                            return array.push({
                                title: __(x),
                                mode: x
                            })
                        });
                        setEndCursor(
                            result.data &&
                            result.data.persons &&
                            result.data.persons.data &&
                            result.data.persons.data.persons &&
                            result.data.persons.data.persons.pageInfo &&
                            result.data.persons.data.persons.pageInfo.endCursor
                        );
                        setType(Type);
                        setTabs(array);
                    }
                });
        SetLoading(false);
    }

    useEffect(() => {
        getInformation()
    }, []);

    const Location = useLocation();

    return (
        (
            Location.pathname.split("/")[2] === ""
            ||
            Location.pathname.split("/")[2] === undefined
        )
        &&
        <div className="my-5 w-100">
            <div className="w-100">
                <TabSection
                    title={__("Persons")}
                    tabs={tabs && tabs}
                    defaultSelectedButton={ContentDefault ? ContentDefault : tabs && tabs[0].title}
                    onTabChange={(e) =>{e.title !== ContentDefault &&  getInformation(e.mode);SetContentDefault(e.title)}}
                    hr
                    more={false}
                />
            </div>
            <div
                className="w-100 d-flex flex-row flex-wrap
                            justify-content-between mt-3"
            >
                {loading ?
                    <Spin
                        spinning
                        className="w-100 d-flex align-items-center justify-content-center"
                        style={{height: "40vh"}}
                        indicator={
                            <img
                                className="w-25 h-auto"
                                src={loadingItem}
                                alt="loading"
                            />
                        }
                    />
                    :
                    content.length !== 0 ?
                        content.map((x, i) => (
                            <Book
                                key={i}
                                data={x}
                                targetLocation="Persons"
                                popover="Person"
                                className="Book"
                            />
                        ))
                        :
                        <div
                            className="
                                w-100 d-flex
                                justify-content-center
                                align-items-center spin"
                        >
                            <Empty/>
                        </div>
                }
            </div>
            {NextPage &&
            <div
                className="
                my-5
                d-flex
                justify-content-center
                align-items-center"
            >
                <Button
                    className="More font-F-Gothic"
                    onClick={() => getInformation(type, endCursor)}
                >
                    {__("More")}
                </Button>
            </div>
            }
        </div>
    );
}
