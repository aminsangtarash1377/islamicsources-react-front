﻿import React, {useEffect, useState} from 'react';
import {
    Article,
    Book,
    TabSection,
    TextPackagePage
} from "../../Module";
import {Button, Empty, Row, Spin} from "antd";
import connect from "react-redux/es/connect/connect";
import {withRouter} from "react-router-dom";
import axios from "axios";
import {defaultRahUrl, loadingItem} from "../../../config";
import {UrlImagePackage} from "../../../actions";
import {__} from "../../../global";
import "./Package.css";


function Package(props) {
    const [content, onTabChange] = useState(null);
    let [data, setData] = useState("");
    let [BookData] = useState([]);
    let [ArticleData] = useState([]);
    let [MagazineData] = useState([]);
    let [loading, SetLoading] = useState(true);
    const tabs = [
        {
            title: __("Book"),
            mode: "book",
            data: BookData
        },
        {
            title: __("Article"),
            mode: "article",
            data: ArticleData
        },
        {
            title: __("Journal"),
            mode: "journal",
            data: MagazineData
        }
    ];

    async function GetInformation() {
        SetLoading(true);
        await axios.get(`${defaultRahUrl}/package?slug=${props.match.params.slug}`)
            .then(result => {
                setData(
                    result.data &&
                    result.data.package.data.postBy
                );
                props.dispatch(UrlImagePackage(
                    result.data.package.data.postBy.featuredImage &&
                    result.data.package.data.postBy.featuredImage.sourceUrl)
                );
                result.data.package.data.postBy.packagefields &&
                result.data.package.data.postBy.packagefields.books.map(x => {
                    BookData.push(x)
                });
                result.data.package.data.postBy.packagefields &&
                result.data.package.data.postBy.packagefields.articles.map(x => {
                    ArticleData.push(x)
                });
                result.data.package.data.postBy.packagefields &&
                result.data.package.data.postBy.packagefields.magazines.map(x => {
                    MagazineData.push(x)
                });
            });
        tabs && tabs.length > 1 ? onTabChange(tabs[0]) : onTabChange(tabs[0]);
        SetLoading(false);
    }


    useEffect(() => {
        GetInformation();
    }, []);


    return (
        <div className="div-PackagePage d-flex flex-column">
            <TextPackagePage
                data={data && data}
                loading={loading}
            />
            <TabSection
                tabs={tabs}
                title=""
                onTabChange={(e) => {
                    onTabChange(e);
                }}
                defaultSelectedButton={content && content.title && content.title}
                hr
                more={false}
            />
            <Row className="w-100 mt-3">
                {
                    content ?
                        <div
                            className={
                                content.mode !== "article"
                                    ?
                                    "d-flex flex-row mt-3 ParentBook"
                                    :
                                    "d-flex flex-row flex-wrap justify-content-around mt-3"
                            }
                        >
                            {content.mode && content.mode === "book" &&
                            <>
                                {!loading ?
                                    content &&
                                    content.data.length !== 0 ?
                                        content.data.map((x, i) => (
                                            <div
                                                key={i}
                                                className={props.RtlDir ? "ml-4-5 mb-4" : "mr-4-5 mb-4"}
                                            >
                                                <Book
                                                    data={x}
                                                    targetLocation="Books"
                                                    popover="Book"
                                                    className="Book"
                                                />
                                            </div>
                                        ))
                                        :
                                        <div
                                            className="
                                                w-100
                                                d-flex
                                                justify-content-center
                                                align-items-center"
                                        >
                                            <Empty/>
                                        </div>
                                    :
                                    <Spin
                                        spinning
                                        className="w-100 d-flex align-items-center justify-content-center"
                                        style={{height: "40vh"}}
                                        indicator={
                                            <img
                                                className="w-25 h-auto"
                                                src={loadingItem}
                                                alt="loading"
                                            />
                                        }
                                    />
                                }
                            </>
                            }
                            {content && content.mode === "article" &&
                            <>
                                {!loading ?
                                    content &&
                                    content.data.length !== 0 ?
                                        content.data.map((x, i) => (
                                            <Article
                                                key={i}
                                                title={x.title}
                                                slug={x.slug}
                                                rate={
                                                    x.articlefields.rate &&
                                                    x.articlefields.rate
                                                }
                                                view={
                                                    x.articlefields.view &&
                                                    x.articlefields.view
                                                }
                                                author={
                                                    x.articlefields.actAuthor &&
                                                    x.articlefields.actAuthor[0] &&
                                                    x.articlefields.actAuthor[0].title

                                                }
                                            />
                                        ))
                                        :
                                        <div
                                            className="
                                                w-100
                                                d-flex
                                                justify-content-center
                                                align-items-center"
                                        >
                                            <Empty/>
                                        </div>
                                    :
                                    <Spin
                                        spinning
                                        className="w-100 d-flex align-items-center justify-content-center"
                                        style={{height: "40vh"}}
                                        indicator={
                                            <img
                                                className="w-25 h-auto"
                                                src={loadingItem}
                                                alt="loading"
                                            />
                                        }
                                    />
                                }
                            </>
                            }
                            {content.mode && content.mode === "journal" &&
                            <>
                                {!loading ?
                                    content &&
                                    content.data.length !== 0 ?
                                        content.data.map((x, i) => (
                                            <div
                                                key={i}
                                                className={props.RtlDir ? "ml-4-5 mb-4" : "mr-4-5 mb-4"}
                                            >
                                                <Book
                                                    data={x}
                                                    targetLocation="Books"
                                                    popover="Book"
                                                    className="Book"
                                                />
                                            </div>
                                        ))
                                        :
                                        <div
                                            className="
                                                w-100
                                                d-flex
                                                justify-content-center
                                                align-items-center"
                                        >
                                            <Empty/>
                                        </div>
                                    :
                                    <Spin
                                        spinning
                                        className="w-100 d-flex align-items-center justify-content-center"
                                        style={{height: "40vh"}}
                                        indicator={
                                            <img
                                                className="w-25 h-auto"
                                                src={loadingItem}
                                                alt="loading"
                                            />
                                        }
                                    />
                                }
                            </>
                            }
                        </div>
                        :
                        <div
                            className="
                               w-100
                               d-flex
                               justify-content-center
                               align-items-center"
                            style={{minHeight: "40vh"}}
                        >
                            <Empty/>
                        </div>
                }
            </Row>
            {
                content && content.mode && content.mode.length >= 14 &&
                <div className="my-5 d-flex justify-content-center align-items-center">
                    <Button
                        className="More font-F-Gothic"
                    >
                        {__("More")}
                    </Button>
                </div>
            }
        </div>
    );
}

const mapStateToProps = states => (
    {
        RtlDir: states.Rtl
    }
);
export default withRouter(connect(mapStateToProps)(Package));
