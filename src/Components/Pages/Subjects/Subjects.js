import React, {useEffect, useState} from 'react';
import {Subject, TabSection} from "../../Module";
import axios from "axios";
import {Link, useLocation} from "react-router-dom";
import {defaultRahUrl, loadingItem} from "../../../config";
import {Empty, Row, Spin} from "antd";
import {__} from "../../../global";
import './Subjects.css';


export default function Subjects() {
    const [table, setCount] = useState(false);
    const [data, setData] = useState(null);
    let [loading, SetLoading] = useState(false);
    const [endCursor, SetEndCursor] = useState("");
    let [NextPage, SetNextPage] = useState(null);


    async function getInformation() {
        SetLoading(true);
        await axios.get(defaultRahUrl + `/subjects?per_page=&end=${endCursor}`)
            .then(result => {
                if (result.status === 200) {
                    setData(
                        result.data &&
                        result.data.subjects &&
                        result.data.subjects.data.topics &&
                        result.data.subjects.data.topics.nodes);
                    SetNextPage(
                        result.data &&
                        result.data.subjects &&
                        result.data.subjects.data &&
                        result.data.subjects.data.topics &&
                        result.data.subjects.data.topics.pageInfo &&
                        result.data.subjects.data.topics.pageInfo.hasNextPage
                    );
                    SetEndCursor(
                        result.data &&
                        result.data.subjects &&
                        result.data.subjects.data &&
                        result.data.subjects.data.topics &&
                        result.data.subjects.data.topics.pageInfo &&
                        result.data.subjects.data.topics.pageInfo.endCursor
                    )
                }
            });
        SetLoading(false);
    }

    useEffect(() => {
        getInformation()
    }, []);

    const Location = useLocation();
    let tabletr = [__("Title"), __("Book"), __("Article"), __("View"), __("Rate")];
    const calcWidth = 100 / tabletr.length;


    return (
        (
            Location.pathname.split("/")[2] === ""
            ||
            Location.pathname.split("/")[2] === undefined
        ) &&
        <div
            className="w-100 d-flex flex-column
                       justify-content-center align-items-center"
        >
            <div className="w-100 mt-3 mt-lg-5">
                <TabSection
                    title={__("Subjects")}
                    onClickTrue={() => setCount(true)}
                    onClickFalse={() => setCount(false)}
                    hr
                    IconSubject
                />
            </div>
            {!table &&
            <Row
                type="flex"
                justify="space-between"
                className="
                    w-100
                    flex-wrap
                    mt-1 mt-lg-4
                    "
            >
                {!loading ? data && data.length > 0 ? data.map((x, i) => (
                        <Subject
                            data={x}
                            key={i}
                        />
                    ))
                    :
                    <Empty
                        className="
                            pb-5
                            w-100 d-flex
                            flex-column
                            justify-content-center
                            align-items-center"
                        style={{minHeight: "40vh"}}
                    />
                    :
                    <div
                        className="w-100 d-flex
                            justify-content-center
                            align-items-center spin"
                    >
                        <Spin
                            spinning
                            indicator={
                                <img
                                    className="w-25 h-auto"
                                    src={loadingItem}
                                    alt="loading"
                                />
                            }
                        />
                    </div>
                }
            </Row>
            }
            {table &&
            <table className="TableSubject w-100 mt-2">
                <tr className="w-100">
                    {data &&
                    data.length > 0 && tabletr.map((x, i) => (
                        <th
                            key={i}
                            className="font-F-Gothic"
                            style={{width: calcWidth + "%"}}>
                            {x}
                        </th>
                    ))}
                </tr>
                {data &&
                data.length > 0 ? data.map((x, i) => (
                        <Link key={i} to={`/Subjects/${x.name}/`}>
                            <tr className="w-100">
                                <td
                                    style={{width: calcWidth + "%"}}
                                    key={i}
                                    className="font-F-Gothic"
                                >
                                    {x.name}
                                </td>
                                <td
                                    style={{width: calcWidth + "%"}}
                                    key={i}
                                    className="font-F-Gothic"
                                >
                                    {x.booksCount}
                                </td>
                                <td
                                    style={{width: calcWidth + "%"}}
                                    key={i}
                                    className="font-F-Gothic"
                                >
                                    {x.articlesCount}
                                </td>
                                <td
                                    style={{width: calcWidth + "%"}}
                                    key={i}
                                    className="font-F-Gothic">
                                    {x.topicfileds && x.topicfileds.topicView
                                        ?
                                        x.topicfileds.topicView
                                        :
                                        "---"
                                    }
                                </td>
                                <td
                                    style={{width: calcWidth + "%"}}
                                    key={i}
                                    className="font-F-Gothic">
                                    <h3 className="font-F-Gothic RateSubject">
                                        {Math.round(
                                            x.topicfileds && x.topicfileds.topicRate / 100
                                        )
                                        }/5
                                    </h3>
                                </td>
                            </tr>
                        </Link>
                    ))
                    :
                    <Empty
                        className="
                            pb-5
                            w-100 d-flex
                            flex-column
                            justify-content-center
                            align-items-center"
                        style={{minHeight: "40vh"}}
                    />
                }
            </table>
            }
            {NextPage &&
            <button
                className="More my-3 my-md-5 font-F-Gothic"
                onClick={() => getInformation()}
            >
                {__("More")}
            </button>
            }
        </div>
    )
}