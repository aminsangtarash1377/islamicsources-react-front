import React, {useEffect, useState} from 'react';
import {TabSection, Book} from "../../Module";
import axios from "axios";
import {defaultRahUrl, loadingItem} from "../../../config";
import {Empty, Spin} from "antd";
import {__} from "../../../global";


export default function JournalsCategory(props) {
    let [data] = useState([]);
    let [NextPage, SetNextPage] = useState(null);
    let [endCursor, SetEndCursor] = useState("");
    let [loading, SetLoading] = useState(true);


    async function GetInformation() {
        SetLoading(true);
        await axios.get(
            defaultRahUrl +
            `/categoryMag?per_page=21&slug=${props.match.params.slug}&end=${endCursor}`
        )
            .then(result => {
                result.data &&
                result.data.category &&
                result.data.category.data &&
                result.data.category.data.magazines &&
                result.data.category.data.magazines.nodes.map(x => {
                    data.push(x);
                });
                SetNextPage(
                    result.data &&
                    result.data.category &&
                    result.data.category.data &&
                    result.data.category.data.magazines &&
                    result.data.category.data.magazines.pageInfo &&
                    result.data.category.data.magazines.pageInfo.hasNextPage
                );
                SetEndCursor(
                    result.data &&
                    result.data.category &&
                    result.data.category.data &&
                    result.data.category.data.magazines &&
                    result.data.category.data.magazines.pageInfo &&
                    result.data.category.data.magazines.pageInfo.endCursor
                )
            });
        SetLoading(false);
    }

    useEffect(() => {
        GetInformation()
    }, []);

    return (
        <div className="d-flex flex-column ">
            <div className="w-100 mt-5 categoryTabSection">
                <TabSection
                    title={props.location.title ? props.location.title : props.match.params.slug}
                    hr
                />
            </div>
            <div className="d-flex flex-wrap justify-content-between BooksCategory">
                {!loading ? data.length !== 0 ? data.map((x, i) => (
                        <Book
                            data={x}
                            key={i}
                            targetLocation="Books"
                            popover="Book"
                        />
                    ))
                    :
                    <div
                        className="w-100 d-flex
                            justify-content-center
                            align-items-center
                            spin"
                    >
                        <Empty/>
                    </div>
                    :
                    <div
                        className="w-100 d-flex
                            justify-content-center
                            align-items-center spin"
                    >
                        <Spin
                            spinning
                            indicator={
                                <img
                                    className="w-25 h-auto"
                                    src={loadingItem}
                                    alt="loading"
                                />
                            }
                        />
                    </div>
                }
            </div>
            {NextPage &&
            <div
                className="w-100 d-flex justify-content-center
                               align-items-center mt-3 mb-5"
            >
                <button
                    className="More font-F-Gothic"
                    onClick={() => GetInformation()}
                >
                    {__("More")}
                </button>
            </div>
            }
        </div>
    )
}