import React, {useEffect, useState} from 'react';
import {Book, TabSection} from "../../Module";
import "./SingleCategory.css";
import {Link} from "react-router-dom";
import axios from "axios";
import {defaultRahUrl} from "../../../config";
import {__} from "../../../global";

export default function SingleCategory() {
    const [show, setShow] = useState("book");
    let [data] = useState([]);
    let [dataBar, setDataBar] = useState([]);
    const [endCursor, SetEndCursor] = useState("");

    function onClickShow(e) {
        setShow(e.currentTarget.getAttribute("name"));
        if (show === "bars")
            setDataCut();
    }

    function getInformation() {
        axios.get(
            defaultRahUrl + `/category/books?per_page=14&slug=7-roman-va-dastan&end=${endCursor}`
        )
            .then(result => {
                if (result.status === 200) {
                    result.data &&
                    result.data.books &&
                    result.data.books.data &&
                    result.data.books.data.books &&
                    result.data.books.data.books.nodes &&
                    result.data.books.data.books.nodes.forEach(x => {
                        data.push(x);
                    });
                    SetEndCursor(
                        result.data &&
                        result.data.books &&
                        result.data.books.data &&
                        result.data.books.data.books &&
                        result.data.books.data.books.pageInfo &&
                        result.data.books.data.books.pageInfo.endCursor &&
                        result.data.books.data.books.pageInfo.endCursor)
                }
            })
    }

    function setDataCut() {
        let cut = data && data.length % 4 === 0;
        let cutLength = data && data.length - 2;
        if (cut)
            setDataBar(data && data);
        else
            setDataBar(data && data.slice(0, cutLength));
    }

    useEffect(() => {
        getInformation();
        setDataCut();
    }, []);

    let tabletr = ["Title", "Writer", "Year", "Publisher", "Format", "Rate"];

    return (
        <div className="w-100 d-flex flex-column align-items-center">
            <div className="mt-2 w-100">
                <TabSection
                    title="Holy Quran (53)"
                    IconCategory={true}
                    hr={true}
                    onClickShow={onClickShow}
                />
            </div>
            {show === "book" &&
            <div className="w-100 d-flex flex-row flex-wrap justify-content-between">
                {data && data.map((x, i) => (
                    <Book
                        key={i}
                        data={x}
                        popover="Book"
                        targetLocation="Books"
                        className="Book"
                    />
                ))}
            </div>
            }
            {endCursor &&
            <button
                className="More my-3 my-md-5 font-F-Gothic"
                onClick={async () => {
                    await getInformation();
                    setDataCut();
                }}
            >
                {__("More")}
            </button>
            }
        </div>
    )
}