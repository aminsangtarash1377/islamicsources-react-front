import React, {useEffect, useState} from 'react';
import {ResourceStatistics, InputSearch, CustomImage} from '../../Module';
import {Button, Col, Row} from "antd";
import connect from "react-redux/es/connect/connect";
import {withRouter} from "react-router-dom";
import {introUrl} from '../../../config';
import axios from "axios";
import {Scrollbars} from 'react-custom-scrollbars';
import './Language.css';

function TopLanguageLG(props) {
    const {dataLang} = props;

    return (
        <React.Fragment>
            <div className="d-none w-100 d-md-flex">
                <Col
                    xs={22}
                    md={8}
                    lg={8}
                >
                    <Row
                        className="
                            LeftLanguage
                            d-flex
                            flex-row
                            justify-content-center
                            justify-content-md-start
                        "
                    >
                        {props.run.map((x, i) => (
                            <Col
                                key={i}
                                xs={6}
                                md={7}
                                lg={6}
                            >
                                <ResourceStatistics
                                    title={x.name}
                                    number={x.number}
                                />
                            </Col>
                        ))}
                    </Row>
                </Col>
                <Col
                    xs={22}
                    md={8}
                    lg={8}
                    className="my-2 my-md-0"
                >
                    <Row
                        className="w-100 d-flex justify-content-center"
                    >
                        <Col
                            xs={15}
                            md={20}
                            lg={14}
                            className="
                                d-flex
                                flex-column
                                justify-content-center
                                align-items-center
                            "
                        >
                            <img
                                src={dataLang && dataLang.logo}
                                className="IconLanguage w-75"
                                alt="islamic sources"
                            />
                            <p className="SubLogo text-center w-95 mt-3 mb-0">
                                {dataLang && dataLang.description}
                            </p>
                        </Col>
                    </Row>
                </Col>
                <Col
                    xs={22}
                    md={8}
                    lg={8}
                >
                    <Row className="
                        RightLanguage
                        d-flex
                        flex-row
                        justify-content-center
                        justify-content-md-end
                    "
                    >
                        {props.fun.map((x, i) => (
                            <Col
                                key={i}
                                xs={6}
                                md={7}
                                lg={6}
                            >
                                <ResourceStatistics title={x.name} number={x.number}/>
                            </Col>
                        ))}
                    </Row>
                </Col>
            </div>
            <div
                className="
                    d-flex
                    w-100
                    d-md-none
                    flex-column
                    justify-content-center
                    align-items-center
                "
            >
                <Col xs={22} className="my-2 my-md-0">
                    <Row
                        className="w-100 d-flex justify-content-center"
                    >
                        <Col
                            span={15}
                            className="
                                d-flex
                                flex-column
                                justify-content-center
                                align-items-center
                            "
                        >
                            <img
                                src={dataLang && dataLang.logo}
                                className="IconLanguage w-75"
                                alt="islamic sources"
                            />
                            <p className="SubLogo text-center w-95 mt-3 mb-0">
                                {dataLang && dataLang.description}
                            </p>
                        </Col>
                    </Row>
                </Col>
                <Col xs={22}>
                    <Row className="w-100 d-flex justify-content-between">
                        <Col xs={{span: 11, offset: props.RtlDir ? 2 : 0}}>
                            <Col span={12}>
                                <ResourceStatistics
                                    text={props.RtlDir ? 2 : 0}
                                    title="Book"
                                    number="25419"
                                />
                            </Col>
                            <Col span={12}>
                                <ResourceStatistics
                                    text={props.RtlDir ? 2 : 0}
                                    title="Writer"
                                    number="25419"
                                />
                            </Col>
                        </Col>
                        <Col xs={{span: 11, offset: props.RtlDir ? 0 : 2}}>
                            <Col span={12}>
                                <ResourceStatistics
                                    text={props.RtlDir ? 0 : 2}
                                    title="Articel"
                                    number="25419"
                                />
                            </Col>
                            <Col span={12}>
                                <ResourceStatistics
                                    text={props.RtlDir ? 0 : 2}
                                    title="Jurnal"
                                    number="25419"
                                />
                            </Col>
                        </Col>
                    </Row>
                </Col>
            </div>
        </React.Fragment>
    )
}

function Language(props) {
    const [dataLang, setDataLang] = useState();
    useEffect(() => {
        axios.get(introUrl + "/intro")
            .then(result =>
                setDataLang(result.data.data)
            );
    }, []);
    let nun = [
        {
            name: "Book",
            number: 1850
        }, {
            name: "Articel",
            number: 850
        }, {
            name: "Jurnal",
            number: 2020
        }, {
            name: "External",
            number: 2068
        }];
    let run = [
        {
            name: "Book",
            number: 1850
        }, {
            name: "Articel",
            number: 850
        }, {
            name: "Jurnal",
            number: 2020
        }];
    let fun = [
        {
            name: "Title",
            number: 25419
        }, {
            name: "Writer",
            number: 917
        }, {
            name: "Language",
            number: 48
        }];

    function setColor(target, hover) {
        let paths = target.currentTarget.querySelectorAll("path");
        if (hover) {
            for (let i = 0; i < paths.length; i++) {
                paths[i].style.fill = target.currentTarget.getAttribute('data-color');
            }
        } else {
            for (let i = 0; i < paths.length; i++) {
                paths[i].style.fill = "#a8a8a8";
            }
        }
    }

    return (
        <div className="w-100 d-flex flex-column">
            <Row
                className="pt-4 mt-0 mt-md-3 w-100 d-flex flex-column flex-md-row
                          justify-content-center align-items-center"
            >
                <TopLanguageLG
                    dataLang={dataLang}
                    RtlDir={props.RtlDir}
                    run={run}
                    fun={fun}
                    nun={nun}
                />
            </Row>
            <Row className="py-3 my-3 w-100 d-flex justify-content-center">
                <InputSearch className="ParentSearchLanguage"/>
            </Row>
            <Row className="w-100 d-flex justify-content-center">
                <Col
                    xs={22}
                    md={24}
                    lg={24}
                >
                    <Scrollbars
                        autoHide
                        className="App-scroller position-relative"
                        style={{height: "40vh", overflowX: "hidden"}}
                    >
                        <div
                            className="
                                w-100
                                ParentLanguages
                                d-flex
                                flex-row flex-wrap
                                justify-content-between
                                "
                        >
                            {
                                dataLang &&
                                dataLang.languages &&
                                dataLang.languages.map((x, i) => (
                                    <Button
                                        className="Language-Languages"
                                        key={i}
                                    >
                                        {x.post_title}
                                    </Button>
                                ))}
                        </div>
                    </Scrollbars>
                </Col>
            </Row>
            <Row
                className="
                    w-100
                    my-3
                    py-md-3
                    my-md-4
                    d-flex
                    flex-column
                    justify-content-center
                    flex-md-row
                    align-items-center
                "
            >
                <Col
                    xs={22}
                    md={12}
                    lg={12}
                    className="
                        d-flex
                        flex-row
                        justify-content-center
                        justify-content-md-start
                        mb-3
                        mb-md-0
                    "
                >
                    <Row className="w-100 d-flex justify-content-start">
                        <Col
                            xs={24}
                            md={18}
                            lg={13}
                            className="
                                d-flex
                                flex-row
                                justify-content-between
                            "
                        >
                            {dataLang && dataLang.apps.map((d, i) =>
                                <a
                                    key={i}
                                    href={d.url}
                                >
                                    <CustomImage
                                        url={d.image}
                                        className="VirtualIcon cursor-p"
                                        onMouseOver={target => setColor(target, true)}
                                        onMouseLeave={target => setColor(target, false)}
                                        data-color="#a4c639"
                                    />
                                </a>
                            )}
                            <p className="m-0">Android Apps</p>
                            {dataLang && dataLang.social.map((d, i) =>
                                <a
                                    key={i}
                                    href={d.url}
                                >
                                    <CustomImage
                                        url={d.image}
                                        className="VirtualIcon cursor-p"
                                        onMouseOver={target => setColor(target, true)}
                                        onMouseLeave={target => setColor(target, false)}
                                        data-color="#1877f2"
                                    />
                                </a>
                            )}
                        </Col>
                    </Row>
                </Col>
                <Col
                    xs={24}
                    md={12}
                    lg={12}
                    className="
                        d-none
                        d-md-flex
                        justify-content-center
                        justify-content-md-end
                    "
                >
                    <p className="m-0">
                        @All rights reserved for islamic source Designed By Rah Studio
                    </p>
                </Col>
            </Row>
        </div>
    );
}

const mapStateToProps = states => (
    {
        RtlDir: states.Rtl
    }
);
export default withRouter(connect(mapStateToProps)(Language));