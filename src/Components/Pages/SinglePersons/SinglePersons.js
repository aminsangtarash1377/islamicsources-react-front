import React, {useEffect, useState} from 'react';
import {Book, BoxInformation, TabSection, SpinLoading} from "../../Module";
import {Empty, Row} from "antd";
import {Scrollbars} from "react-custom-scrollbars";
import axios from "axios";
import {defaultRahUrl} from "../../../config";
import {__} from "../../../global";
import './SinglePersons.css';


export default function SinglePersons(props) {
    const [books, setBooks] = useState("");
    const [data, setData] = useState(null);
    let [loading, SetLoading] = useState(true);

    async function GetInformation() {
        SetLoading(true);
        await axios.get(defaultRahUrl + `/person?slug=${props.match.params.slug}`)
            .then(result => {
                setData(result.data.person.data.personBy);
                setBooks(result.data.publisherBooks);
            });
        SetLoading(false);
    }

    useEffect(() => {
        GetInformation()
    }, []);

    return (
        <div>
            <BoxInformation
                data={data}
                Logo="SinglePerson"
                loading={loading}
            />
            <Row
                type="flex"
                justify="space-between"
                className="pt-4 flex-column align-items-center mt-2 mt-md-5"
            >
                <TabSection
                    moreLink={`#`}
                    hr
                    more
                    title={__("Author books")}
                    styleHr={{minWidth: "75%"}}
                />
                <Row className="w-100 mt-3">
                    {
                        books ?
                            <Scrollbars
                                className="ScrollBooks Scrollbars-Suppress-vertical"
                            >
                                {
                                    <SpinLoading
                                        loading={books && books.length !== 0}
                                        mode="withoutBox"
                                    >
                                        {
                                            books.map((x, i) => (
                                                <Book
                                                    key={i}
                                                    data={x}
                                                    targetLocation="Books"
                                                    popover="SinglePerson"
                                                    className="Book"
                                                />
                                            ))
                                        }
                                    </SpinLoading>
                                }
                            </Scrollbars>
                            :
                            <Empty
                                className="
                                   w-100 d-flex
                                   flex-column
                                   justify-content-center
                                   align-items-center"
                                style={{minHeight: "40vh"}}
                            />
                    }
                </Row>
            </Row>
        </div>
    );
}
