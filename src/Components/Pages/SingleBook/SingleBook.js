import React, {useEffect, useState} from 'react';
import connect from "react-redux/es/connect/connect";
import {Link, withRouter} from "react-router-dom";
import {Col, Row, Rate, message, Icon} from "antd";
import PropTypes from "prop-types";
import defaultImage from "../../../images/defaultBook.png"
import {BookCollection, SpinLoading} from "../../Module";
import BookMarkFalse from "../../../images/neshan.svg";
import BookMarkTrue from "../../../images/neshan A.svg";
import axios from "axios";
import {defaultRahUrl, defaultWPUrl, ISAccessUserToken, setRateByUser} from "../../../config";
import {__} from "../../../global";
import './SingleBook.css';

InformationItem.propTypes = {
    name: PropTypes.string.isRequired,
    response: PropTypes.string.isRequired
};

function InformationItem(props) {
    return (
        <div className="px-3 px-md-0 d-flex justify-content-start my-1">
            <span
                className="text-bold
                font-color-Information-text
                InformationItemTitle"
            >
                {__(props.name)}:
            </span>
            <span
                className="mx-1
                text-black
                font-color-Information-text
                InformationItemValue"
            >
                {props.response}
            </span>
        </div>
    )
}

function SingleBook(props) {
    const [dataBook, setDataBook] = useState(null);
    const [dataRate, setDataRate] = useState(null);
    let [dataOtherVolumes] = useState([]);
    let [dataSimilarBook] = useState([]);
    let [dataSimilarArticle] = useState([]);
    let [dataSimilarMagazines] = useState([]);
    let [ValueToShow, SetValueToShow] = useState(null);
    let [loading, SetLoading] = useState(true);
    let [loadingRate, SetLoadingRate] = useState(false);
    const [bookMark, SetBookMark] = useState(false);
    const [rateVoteAmount, SetRateVoteAmount] = useState(0);
    const tabs1 = [
        {
            mode: "book",
            data: dataOtherVolumes
        }
    ];

    const tabs = [
        {
            title: __("Book"),
            mode: "book",
            data: dataSimilarBook
        },
        {
            title: __("Article"),
            mode: "article",
            data: dataSimilarArticle
        },
        {
            title: __("Journal"),
            mode: "journal",
            data: dataSimilarMagazines
        }
    ];

    async function getData() {
        await SetLoading(true);
        await axios.get(defaultRahUrl + `/book?slug=${props.match.params.slug}`)
            .then(result => {
                setDataBook(result);
                const rateVote = parseInt(result.data.book.data.book.bookfields.votes);
                SetRateVoteAmount(isNaN(rateVote) ? 0 : rateVote);
                setDataRate(
                    result.data.book &&
                    result.data.book.data &&
                    result.data.book.data.book.bookfields &&
                    result.data.book.data.book.bookfields.rate
                );
                SetValueToShow(result.data.book.data && result.data.book.data.book && result.data.book.data.book.valuesToShow);
                result.data.otherVolumes.data.books.nodes.map((d) => {
                    if (d.bookId !== result.data.book.data.book.bookId)
                        return dataOtherVolumes.push(d);
                });
                result.data.similarActs.data.books.nodes.map((d) => {
                    return dataSimilarBook.push(d);
                });
                result.data.similarActs.data.articles.nodes.map((d) => {
                    return dataSimilarArticle.push(d);
                });
                result.data.similarActs.data.magazines.nodes.map((d) => {
                    return dataSimilarMagazines.push(d);
                });
            });
        await SetLoading(false);
    }

    useEffect(() => {
        getData()
    }, []);

    async function sendRate(e, id) {
        SetLoadingRate(true);
        if (setRateByUser(id)) {
            await axios.post(defaultRahUrl + `/rate?type=post&id=${id}&rate=${e}`)
                .then(result => {
                        if (result.data) {
                            SetRateVoteAmount(rateVoteAmount + 1);
                            setDataRate(result.data.rate);
                            message.success("Done");
                        }
                    }
                )
                .catch(error => {
                    error.response ?
                        message.error(error.response.title) : message.error("Not Done")
                });
        } else {
            message.warning(__('You rate this post once'))
        }
        SetLoadingRate(false)
    }

    async function SetBookMarking(PostId) {
        await axios.post(defaultWPUrl + `/users/BookMarks`,
            {"PostId": PostId},
            {
                headers: {
                    "Content-Type": "application/json",
                    Authorization: ISAccessUserToken
                }
            }
        )
            .then(result => {
                if (result.status === 200) {
                    SetBookMark(result.data.data);
                    message.success(__('Done'));
                }
            })
            .catch(res => {
                if (res.response && res.response.data.message) {
                    message.error(__(res.response.data.message))
                }
            });
    }

    return (
        <SpinLoading mode="withoutBox" loading={loading} className="py-5">
            <React.Fragment>
                <div className="d-flex flex-column">
                    <h3
                        className="
                            text-bold
                            color-Header-responsive
                            d-lg-none
                            d-block"
                    >
                        {
                            dataBook &&
                            dataBook.data.book.data &&
                            dataBook.data.book.data.book &&
                            dataBook.data.book.data.book.title
                        }
                    </h3>
                    <Row
                        className="
                            d-flex
                            flex-row
                            border
                            justify-content-between
                            align-items-start
                            mt-2
                            BookInfoInformation
                "
                    >
                        <Col span={10} lg={6}>
                            <img
                                style={{width: "100%", maxHeight: "100%"}}
                                src={
                                    dataBook
                                    &&
                                    dataBook.data.book.data &&
                                    dataBook.data.book.data.book &&
                                    dataBook.data.book.data.book.featuredImage.mediaDetails.sizes[
                                    dataBook.data.book.data.book.featuredImage.mediaDetails.sizes.length - 1
                                        ].sourceUrl ? dataBook.data.book.data.book.featuredImage.mediaDetails.sizes[
                                        dataBook.data.book.data.book.featuredImage.mediaDetails.sizes.length - 1
                                            ].sourceUrl
                                        :
                                        defaultImage
                                }
                                alt="book Islamic Sources"
                            />
                        </Col>
                        <Col
                            span={14}
                            lg={14}
                            className="InformationBookInfo font-F-Gothic"
                        >
                            <h3 className="text-bold font-color-Information-text d-lg-block d-none">
                                {
                                    dataBook
                                    &&
                                    dataBook.data.book.data &&
                                    dataBook.data.book.data.book &&
                                    dataBook.data.book.data.book.title
                                }
                            </h3>
                            <Row
                                type="flex"
                                className="justify-content-lg-start justify-content-center pt-2 mb-5"
                            >
                                <div className="ParentValueToShow d-flex flex-column flex-wrap">
                                    {ValueToShow && ValueToShow.map((data, i) => {
                                        if (data.type === "post_object") {
                                            if (data.value.length) {
                                                return <InformationItem
                                                    key={i}
                                                    name={data.label}
                                                    response={data.value[0] && data.value[0].post_title}
                                                />
                                            } else {
                                                if (data.value.post_type === "person") {
                                                    return <Link
                                                        to={`/Persons/${data.value.post_name}/`}
                                                        className="LinkedObject"
                                                    >
                                                        <InformationItem
                                                            key={i}
                                                            name={data.label}
                                                            response={data.value.post_title}
                                                        />
                                                    </Link>
                                                } else if (data.value.post_type === "publisher") {
                                                    return <Link
                                                        to={`/Publishers/${data.value.post_name}/`}
                                                        className="LinkedObject"
                                                    >
                                                        <InformationItem
                                                            key={i}
                                                            name={data.label}
                                                            response={data.value.post_title}
                                                        />
                                                    </Link>
                                                }
                                            }
                                        } else if (data.name && data.value) {
                                            return <InformationItem
                                                key={i}
                                                name={data.label}
                                                response={data.value}
                                            />
                                        }
                                    })}
                                </div>
                            </Row>
                            <Row type="flex" align="middle" className="pt-3 d-lg-flex d-none">
                                <SpinLoading mode="loading" loading={loadingRate}>
                                    <Rate
                                        value={!loading && dataRate > 0.5 ? Math.round(dataRate) : 1}
                                        onChange={(e) => sendRate(e, dataBook && dataBook.data.book.data.book.bookId)}
                                        character={
                                            <div
                                                className="position-relative "
                                                style={{
                                                    width: "1rem",
                                                    height: "1rem",
                                                }}
                                            >
                                                <div
                                                    style={{
                                                        width: ".9rem",
                                                        height: ".9rem",
                                                        background: "#b3b3b3",
                                                        borderRadius: "2px"
                                                    }}
                                                    className="RatingIslamicSources position-absolute"
                                                />
                                                <div
                                                    style={{
                                                        width: ".9rem",
                                                        height: ".9rem",
                                                        background: "#b3b3b3",
                                                        transform: "rotate(45deg)",
                                                        borderRadius: "2px"
                                                    }}
                                                    className="RatingIslamicSources position-absolute"
                                                />
                                            </div>
                                        }
                                        allowHalf={false}
                                    />
                                </SpinLoading>
                                <span
                                    className="mx-3 text-black"
                                    style={{fontSize: ".75rem"}}
                                >
                                    {`(${
                                        dataBook
                                        &&
                                        rateVoteAmount + " "} ${(__("vote"))})`}
                        </span>
                                <img
                                    src={bookMark ? BookMarkTrue : BookMarkFalse}
                                    className="mx-2 h-100 cursor-p"
                                    alt=""
                                    onClick={() => SetBookMarking(
                                        dataBook && dataBook.data.book.data.book.bookId
                                    )}
                                    style={{width: ".65rem"}}
                                />
                                <div className="ShareIcon mx-2 d-flex align-items-center cursor-p">
                                    <Icon type="share-alt"/>
                                </div>
                            </Row>
                        </Col>
                        <Col
                            span={24}
                            lg={4}
                            className="pt-lg-5 d-lg-flex d-none align-items-center justify-content-start"
                        >
                            <div className="d-flex flex-column align-items-center">
                                <img
                                    src={dataBook &&
                                    dataBook.data &&
                                    dataBook.data.book &&
                                    dataBook.data.book.data &&
                                    dataBook.data.book.data.book &&
                                    dataBook.data.book.data.book.bookfields &&
                                    dataBook.data.book.data.book.bookfields.actQrCode}
                                    alt="scan book"
                                    width={140}
                                    className="mb-1"
                                />
                                <a
                                    href={`/${
                                    dataBook
                                    &&
                                    dataBook.data.book.data &&
                                    dataBook.data.book.data.book &&
                                    dataBook.data.book.data.book.bookfields.actFile &&
                                    dataBook.data.book.data.book.bookfields.actFile[0] &&
                                    dataBook.data.book.data.book.bookfields.actFile[0].actFileDoc}`
                                    }
                                    download={
                                        dataBook
                                        &&
                                        dataBook.data.book.data &&
                                        dataBook.data.book.data.book &&
                                        dataBook.data.book.data.book.bookfields.actFile &&
                                        dataBook.data.book.data.book.bookfields.actFile[0] &&
                                        dataBook.data.book.data.book.bookfields.actFile[0].actFileDoc
                                    }
                                    className="
                                        btn-Book-study
                                        btn
                                        d-flex
                                        justify-content-center
                                        font-F-Gothic
                                        b-flat
                                        pt-2
                                        px-3-5
                                        mt-3
                                     "
                                >
                                    {__("Download")}
                                </a>
                                <button
                                    className="
                                         btn-Book-study
                                         btn
                                         d-flex
                                         justify-content-center
                                         font-F-Gothic
                                         b-flat
                                         pt-2
                                         px-3-5
                                         mt-3
                                      "
                                >
                                    {__("Online Study")}
                                </button>
                            </div>
                        </Col>
                    </Row>
                    <Row
                        type="flex"
                        align="middle"
                        className="px-2 mt-5 d-lg-none d-flex"
                    >
                        <SpinLoading mode="loading" loading={loadingRate}>
                            <Rate
                                value={
                                    !loading && dataRate > 0.5 ? Math.round(dataRate) : 1
                                }
                                onChange={(e) => sendRate(e, dataBook && dataBook.data.book.data.book.bookId)}
                                character={
                                    <div
                                        className="position-relative "
                                        style={{
                                            width: "1.5rem",
                                            height: "1rem",
                                        }}
                                    >
                                        <div
                                            style={{
                                                width: "1.20rem",
                                                height: "1.20rem",
                                                background: "#b3b3b3",
                                                borderRadius: "2px"
                                            }}
                                            className="RatingIslamicSources position-absolute"
                                        />
                                        <div
                                            style={{
                                                width: "1.20rem",
                                                height: "1.20rem",
                                                background: "#b3b3b3",
                                                transform: "rotate(45deg)",
                                                borderRadius: "2px"
                                            }}
                                            className="RatingIslamicSources position-absolute"
                                        />
                                    </div>
                                }
                                allowHalf={false}
                            />
                        </SpinLoading>
                        <span
                            className="mx-2 text-black"
                            style={{fontSize: "1rem"}}
                        >
                            {`(${
                            dataBook
                            &&
                            rateVoteAmount + " "} ${(__("vote"))})`}
                        </span>
                        <img
                            src={bookMark ? BookMarkTrue : BookMarkFalse}
                            className="mx-2 h-100"
                            alt=""
                            onClick={() => SetBookMarking(
                                dataBook && dataBook.data.book.data.book.bookId
                            )}
                            style={{width: ".75rem"}}
                        />
                        <div className="ShareIcon mx-2 d-flex align-items-center">
                            <Icon type="share-alt"/>
                        </div>
                    </Row>
                    <Row
                        type="flex"
                        justify="space-between"
                        align="middle"
                        className="mt-2 d-lg-none justify-content-between align-items-center D-letter"
                    >
                        <button
                            className="
                            btn-Book-studyInResponsive
                            btn
                            d-flex
                            justify-content-center
                            align-items-center
                            font-F-Gothic
                            b-flat
                            px-3-5
                            mt-3
                            "
                        >
                            <a
                                href={`/${
                                    dataRate ?
                                        dataRate.votes
                                        :
                                        dataBook
                                        &&
                                        dataBook.data.book.data &&
                                        dataBook.data.book.data.book &&
                                        dataBook.data.book.data.book.bookfields.actFile &&
                                        dataBook.data.book.data.book.bookfields.actFile[0] &&
                                        dataBook.data.book.data.book.bookfields.actFile[0].actFileDoc}`
                                }
                                download={
                                    dataBook
                                    &&
                                    dataBook.data.book.data &&
                                    dataBook.data.book.data.book &&
                                    dataBook.data.book.data.book.bookfields.actFile &&
                                    dataBook.data.book.data.book.bookfields.actFile[0] &&
                                    dataBook.data.book.data.book.bookfields.actFile[0].actFileDoc
                                }
                            >
                                {(__("Download"))}
                            </a>
                        </button>
                        <button
                            className="
                            btn-Book-studyInResponsive
                            btn
                            d-flex
                            justify-content-center
                            align-items-center
                            font-F-Gothic
                            b-flat
                            px-3-5
                            mt-3
                            "
                        >
                            {(__("Online Study"))}
                        </button>
                    </Row>
                </div>
                <Row className="my-5 Introduction-text-book">
                    <h2
                        className="font-F-Gothic text-bold"
                    >
                        {(__("Introduction"))}
                    </h2>
                    <div
                        className="font-F-Gothic"
                        dangerouslySetInnerHTML={{
                            __html:
                                dataBook
                                &&
                                dataBook.data.book.data &&
                                dataBook.data.book.data.book &&
                                dataBook.data.book.data.book.content
                        }}
                    />
                </Row>
                <BookCollection
                    title={__("Other Volumes")}
                    hr
                    tabs={tabs1}
                    name="books"
                />
                <BookCollection
                    title={__("Similar titles")}
                    hr
                    tabs={tabs}
                    name="books"
                />
            </React.Fragment>
        </SpinLoading>
    )
}

const mapStateToProps = states => (
    {
        RtlDir: states.Rtl
    }
);
export default withRouter(connect(mapStateToProps)(SingleBook));