import React, {useEffect, useState} from 'react';
import connect from "react-redux/es/connect/connect";
import {withRouter, useLocation} from "react-router-dom";
import {Book, BookCollection, TabSection, CardView, Article, SpinLoading} from "../../Module";
import {defaultRahUrl, loadingItem} from "../../../config";
import {
    DataForSideBar,
    LoadingForSidebar, SingleInBooksPageSlug, SingleInBooksPageTitle,
    SingleInCategoriesPageSlug,
    SingleInCategoriesPageTitle
} from "../../../actions";
import axios from "axios";
import {Spin} from 'antd';
import {__} from "../../../global";

function Categories(props) {
    let [content,SetContent] = useState(null);
    const Location = useLocation();
    const [data, SetData] = useState(null);
    let [loading, SetLoading] = useState(false);
    let [BookData_1] = useState([]);
    let [ArticleData_1] = useState([]);
    let [BookData_2] = useState([]);
    let [ArticleData_2] = useState([]);
    let [BookData_3] = useState([]);
    let [ArticleData_3] = useState([]);
    let [BookData_4] = useState([]);
    let [ArticleData_4] = useState([]);
    let [dataSingleBook, SetDataSingleBook] = useState([]);
    let [dataSingleArticle, SetDataSingleArticle] = useState([]);
    const [show, setShow] = useState("book");

    async function onClickShow(e) {
        console.log(e.mode);
        setShow(e.mode);
        SetContent(e);
    }

    const tabs_1 = [
        {
            title: __("Book"),
            mode: "book",
            data: BookData_1 && BookData_1
        },
        {
            title: __("Article"),
            mode: "article",
            data: ArticleData_1 && ArticleData_1
        }
    ];
    const tabs_2 = [
        {
            title: __("Book"),
            mode: "book",
            data: BookData_2 && BookData_2
        },
        {
            title: __("Article"),
            mode: "article",
            data: ArticleData_2 && ArticleData_2
        }
    ];
    const tabs_3 = [
        {
            title: __("Book"),
            mode: "book",
            data: BookData_3 && BookData_3
        },
        {
            title: __("Article"),
            mode: "article",
            data: ArticleData_3 && ArticleData_3
        }
    ];
    const tabs_4 = [
        {
            title: __("Book"),
            mode: "book",
            data: BookData_4 && BookData_4
        },
        {
            title: __("Article"),
            mode: "article",
            data: ArticleData_4 && ArticleData_4
        }
    ];
    const tabs_Single = [
        {
            title: __("Book"),
            mode: "book",
            data: dataSingleBook.length > 0 && dataSingleBook
        },
        {
            title: __("Article"),
            mode: "article",
            data: dataSingleArticle.length > 0 && dataSingleArticle
        }
    ];

    async function getInformation() {
        await props.dispatch(LoadingForSidebar(true));
        await axios.get(defaultRahUrl + "/categories?per_page_book=6&per_page_article=6&books=1&articles=1")
            .then(result => {
                console.log(result);
                if (result.status === 200) {
                    props.dispatch(DataForSideBar(
                        result.data &&
                        result.data.category &&
                        result.data.category.data &&
                        result.data.category.data.categories &&
                        result.data.category.data.categories.nodes &&
                        result.data.category.data.categories.nodes
                    ));
                    SetData(
                        result.data &&
                        result.data.category &&
                        result.data.category.data &&
                        result.data.category.data.categories &&
                        result.data.category.data.categories.nodes &&
                        result.data.category.data.categories.nodes);
                    result.data &&
                    result.data.category &&
                    result.data.category.data &&
                    result.data.category.data.categories &&
                    result.data.category.data.categories.nodes &&
                    result.data.category.data.categories.nodes.map((x, i) => {
                        if (i === 0) {
                            x.books.data &&
                            x.books.data.books.nodes.map(x => {
                                BookData_1.push(x);
                            });
                            x.articles.data &&
                            x.articles.data.articles.nodes.map(x => {
                                ArticleData_1.push(x);
                            })
                        } else if (i === 1) {
                            x.books.data &&
                            x.books.data.books.nodes.map(x => {
                                BookData_2.push(x);
                            });
                            x.articles.data &&
                            x.articles.data.articles.nodes.map(x => {
                                ArticleData_2.push(x);
                            })
                        } else if (i === 2) {
                            x.books.data &&
                            x.books.data.books.nodes.map(x => {
                                BookData_3.push(x);
                            });
                            x.articles.data &&
                            x.articles.data.articles.nodes.map(x => {
                                ArticleData_3.push(x);
                            })
                        } else if (i === 3) {
                            x.books.data &&
                            x.books.data.books.nodes.map(x => {
                                BookData_4.push(x);
                            });
                            x.articles.data &&
                            x.articles.data.articles.nodes.map(x => {
                                ArticleData_4.push(x);
                            })
                        }
                    })
                }
            });
        await props.dispatch(LoadingForSidebar(false));
    }

    async function getSingleData() {
        console.log(props.SingleSlug);
        SetLoading(true);
        await axios.get(defaultRahUrl +
            `/category?per_page_book=18&per_page_article=12&slug=${props.SingleSlug}`
        )
            .then(result => {
                console.log(result);
                if (result.status === 200) {
                    SetDataSingleBook(
                        result.data &&
                        result.data.category &&
                        result.data.category.data &&
                        result.data.category.data.books &&
                        result.data.category.data.books.nodes
                    );
                    SetDataSingleArticle(
                        result.data &&
                        result.data.category &&
                        result.data.category.data &&
                        result.data.category.data.articles &&
                        result.data.category.data.articles.nodes
                    );
                    SetContent(tabs_Single && tabs_Single[0])
                }
            });
        SetLoading(false);
    }



    function SetTabs(i) {
        if (i === 0) {
            return tabs_1
        } else if (i === 1) {
            return tabs_2
        } else if (i === 2) {
            return tabs_3
        } else if (i === 3) {
            return tabs_4
        }
    }


    useEffect(() => {
            !props.Single && getInformation();
            props.Single && getSingleData();
    }, [props.Single]);

    console.log(props);

    return (
        (
            Location.pathname.split("/")[2] === ""
            ||
            Location.pathname.split("/")[2] === undefined
        ) &&

        <React.Fragment>
            {props.Single !== null
                ?
                !loading ?
                    <div className="w-100 d-flex flex-column align-items-center">
                        <div className="mt-2 w-100">
                            <TabSection
                                title={props.Single}
                                subCategoryLink={true}
                                more
                                tabs={tabs_Single}
                                hr={true}
                                onTabChange={(e) => {
                                    onClickShow(e)
                                }}
                                moreLink={show === "book" ?
                                    `/Category/${props.SingleSlug && props.SingleSlug}/Books/`
                                    :
                                    `/Category/${props.SingleSlug && props.SingleSlug}/Articles/`
                                }
                                defaultSelectedButton={content && content.title}
                            />
                        </div>
                        {show === "book" &&
                        <div
                            className="w-100 d-flex flex-row flex-wrap
                        justify-content-between mt-4"
                        >
                            {dataSingleBook.length > 0 &&
                            dataSingleBook.map((x, i) => (
                                <Book
                                    key={i}
                                    data={x}
                                    popover="Book"
                                    targetLocation="Books"
                                    className="Book"
                                />
                            ))}
                        </div>
                        }
                        {show === "article" &&
                        <div
                            className="w-100 d-flex flex-row flex-wrap
                        justify-content-between mt-4"
                        >
                            {dataSingleArticle.length > 0 &&
                            dataSingleArticle.map((x, i) => (
                                <Article
                                    link="Articles"
                                    key={i}
                                    title={x.title}
                                    slug={x.slug}
                                    rate={x.articlefields && x.articlefields.rate}
                                    view={x.articlefields && x.articlefields.view}
                                    author={
                                        x.articlefields &&
                                        x.articlefields.actAuthor &&
                                        x.articlefields.actAuthor.title &&
                                        x.articlefields.actAuthor.title[0] &&
                                        x.articlefields.actAuthor.title[0]
                                    }
                                />
                            ))}
                        </div>
                        }
                    </div>
                    :
                    <div className="w-100 d-flex
                        justify-content-center align-items-center spin"
                    >
                        <SpinLoading mode="logo"/>
                    </div>
                :
                <div className="py-3">
                    {data ? data.slice(0, 4).map((x, i) => (
                            <BookCollection
                                more
                                title={x.name}
                                tabs={SetTabs(i)}
                                hr={true}
                                moreLink="/"
                            />
                        ))
                        :
                        <div className="w-100 d-flex
                        justify-content-center align-items-center spin"
                        >
                            <SpinLoading mode="logo"/>
                        </div>
                    }
                </div>
            }
        </React.Fragment>
    )
}

const mapStateToProps = states => (
    {
        RtlDir: states.Rtl,
        Single: states.SingleInCategoriesPageTitle,
        SingleSlug: states.SingleInCategoriesPageSlug
    }
);
export default withRouter(connect(mapStateToProps)(Categories));