import React, {useEffect, useState} from 'react';
import {Contact, SpinLoading, TextAbout} from "../../Module";
import axios from "axios";
import {defaultRahUrl} from "../../../config";
import './About.css';


export default function About() {
    let [data, PostData] = useState("");
    let [loading, PostLoading] = useState(true);

    async function InformationAbout() {
        PostLoading(true);
        await axios.get(defaultRahUrl + "/pageAbout")
            .then(result => {
                if (result.status === 200) {
                    PostData(result.data);
                }
            });
        PostLoading(false)
    }

    useEffect( () =>
             InformationAbout(),
        []
    );

    return (
        <div className="min-vh-100 d-flex flex-column align-items-center">
            <SpinLoading
                loading={loading}
                mode="withoutBox"
            >
                <img
                    src={data && data.logo}
                    style={{width: "33vw"}}
                    alt="logo Islamic Sources"
                />
            </SpinLoading>
            <TextAbout
                data={data && data}
                loading={loading}
            />
            <Contact/>
        </div>
    );
}
