import React, {useEffect, useState} from 'react';
import {TabSection, SearchIn, MyList} from "../../Module";
import connect from "react-redux/es/connect/connect";
import {withRouter} from "react-router-dom";
import {SearchFiltersMyLists} from "../../../actions";
import {__} from "../../../global";
import './SearchPage.css';


let dataBuilder = {};

function SearchPage(props) {
    const [content, onTabChange] = useState({title: __("Books")});

    const tabs = [
        {title: __("Books")},
        {title: __("Article")},
        {title: __("Journal")},
        {title: __("Writer")},
        {title: __("Publisher")},
        {title: __("Media")},
        {title: __("Centers")},
    ];

    useEffect(() => {
        dataBuilder[content.title] = {}
    }, [content]);

    function changeMyList(e) {
        dataBuilder[content.title] = e;
        props.dispatch(SearchFiltersMyLists(
            {
                name: content.title,
                data: dataBuilder[content.title]
                , checked: true
            }));
    }

    return (
        <div className="mt-5">
            <TabSection
                title="Search in.."
                tabs={tabs}
                onTabChange={(e) => onTabChange(e)}
                defaultSelectedButton={content && content.title && content.title}
                hr={false}
                moer={false}
                styleSearchPage="AllButtonWidth"
            />
            <SearchIn
                id={content && content.title}
                sendData={(e) => changeMyList(e)}
            />
            <MyList/>
        </div>
    );
}

const mapStateToProps = states => (
    {
        FilterRedux: states.Filter,
        RtlDir: states.Rtl,
        SearchFiltersMyLists: states.SearchFiltersMyLists,
        CheckSelected: states.CheckTypeSelected,
        CheckFormatSelected: states.CheckFormatSelected,
        CheckCreatorSelected: states.CheckCreatorSelected
    }
);
export default withRouter(connect(mapStateToProps)(SearchPage));
