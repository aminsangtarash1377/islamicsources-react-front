import React, {useEffect, useState} from 'react';
import {TabSection, Book} from "../../Module";
import "./BooksCategory.css";
import axios from "axios";
import {defaultRahUrl, loadingItem} from "../../../config";
import {Empty, Spin} from "antd";
import {__} from "../../../global";


export default function BooksSubject(props) {
    let [data] = useState([]);
    let [NextPage, SetNextPage] = useState(null);
    let [endCursor, SetEndCursor] = useState("");
    let [loading, SetLoading] = useState(true);


    async function GetInformation() {
        SetLoading(true);
        await axios.get(
            defaultRahUrl +`/category/books?per_page=21&slug=${props.match.params.slug}&end=${endCursor}`
        )
            .then(result => {
                console.log(result,'ttttt');
                result.data &&
                result.data.books &&
                result.data.books.data &&
                result.data.books.data.books &&
                result.data.books.data.books.nodes.map(x => {
                    data.push(x);
                });
                SetNextPage(
                    result.data &&
                    result.data.books &&
                    result.data.books.data &&
                    result.data.books.data.books &&
                    result.data.books.data.books.pageInfo &&
                    result.data.books.data.books.pageInfo.hasNextPage
                );
                SetEndCursor(
                    result.data &&
                    result.data.books &&
                    result.data.books.data &&
                    result.data.books.data.books &&
                    result.data.books.data.books.pageInfo &&
                    result.data.books.data.books.pageInfo.endCursor
                )
            });
        SetLoading(false);
    }

    useEffect(() => {
        GetInformation()
    }, []);

    return (
        <div className="d-flex flex-column ">
            <div className="w-100 mt-5 categoryTabSection">
                <TabSection
                    title={(__("Books"))}
                    hr
                />
            </div>
            <div className="d-flex flex-wrap justify-content-between BooksCategory">
                {!loading ? data.length !== 0 ? data.map((x, i) => (
                        <Book
                            location="Books"
                            data={x}
                            key={i}
                            targetLocation="Books"
                            popover="Book"
                        />
                    ))
                    :
                    <div
                        className="w-100 d-flex
                        justify-content-center align-items-center spin"
                    >
                        <Empty/>
                    </div>
                    :
                    <div
                        className="w-100 d-flex
                                 justify-content-center
                                 align-items-center spin"
                    >
                        <Spin
                            spinning
                            indicator={
                                <img
                                    className="w-25 h-auto"
                                    src={loadingItem}
                                    alt="loading"
                                />
                            }
                        />
                    </div>
                }
            </div>
            {NextPage &&
            <div
                className="w-100 d-flex justify-content-center
                               align-items-center mt-3 mb-5"
            >
                <button
                    className="More font-F-Gothic"
                    onClick={() => GetInformation()}
                >
                    {__("More")}
                </button>
            </div>
            }
        </div>
    )
}