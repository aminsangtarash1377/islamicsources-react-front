import React, {useEffect, useState} from 'react';
import {
    BookCollection,
    Pictures,
    AuthorOfTheWeek,
    BookOfTheWeekFirst,
    SliderMainPage,
    Media,
} from "../../Module";
import one_P from "../../../images/pic/Picture Media A.jpg";
import two_P from "../../../images/pic/Picture Media B.jpg";
import tree_P from "../../../images/pic/Picture Media C.jpg";
import four_P from "../../../images/pic/Picture Media D.jpg";
import five_P from "../../../images/pic/Picture Media E.jpg";
import six_P from "../../../images/pic/Picture Media F.jpg";
import seven_P from "../../../images/pic/Picture Media G.jpg";
import one_S from "../../../images/sound/PictureSound G.jpg";
import two_S from "../../../images/sound/PictureSound A.jpg";
import tree_S from "../../../images/sound/PictureSound B.jpg";
import four_S from "../../../images/sound/PictureSound C.jpg";
import five_S from "../../../images/sound/PictureSound D.jpg";
import six_S from "../../../images/sound/PictureSound E.jpg";
import seven_S from "../../../images/sound/PictureSound F.jpg";
import one_V from "../../../images/PictureVideo A.png";
import two_V from "../../../images/PictureVideo B.png";
import tree_V from "../../../images/PictureVideo C.png";
import four_V from "../../../images/PictureVideo D.png";
import axios from "axios";
import {defaultRahUrl, loadingItem, PostArticleConverter, PostBookConverter} from "../../../config";
import {DataForSideBar} from "../../../actions";
import connect from "react-redux/es/connect/connect";
import {withRouter} from "react-router-dom";
import {Spin} from "antd";
import {__} from "../../../global";
import './Main.css';

function Main(props) {

    let [Package, SetPackage] = useState(null);
    let [loading, SetLoading] = useState(true);
    let [SubjectTitle, SetSubjectTitle] = useState(null);
    let [Settings, SetSettings] = useState(null);
    let [NewTitles] = useState([
        {
            title: __("Book"),
            mode: "book",
            data: []
        },
        {
            title: __("Article"),
            mode: "article",
            data: []
        },
        {
            title: __("Journal"),
            mode: "journal",
            data: []
        }]);
    let [SubjectBox] = useState([
        {
            title: __("Book"),
            mode: "book",
            data: []
        },
        {
            title: __("Article"),
            mode: "article",
            data: []
        },
        {
            title: __("Journal"),
            mode: "journal",
            data: []
        }]);
    let [HotTitles] = useState([
        {
            title: __("Book"),
            mode: "book",
            data: []
        },
        {
            title: __("Article"),
            mode: "article",
            data: []
        },
        {
            title: __("Journal"),
            mode: "journal",
            data: []
        }]);
    let [authorOfTheWeek, SetAuthorOfTheWeek] = useState(null);
    let [SingleAuthorOfTheWeek, SetSingleAuthorOfTheWeek] = useState(null);
    let [BookOfTheWeek, SetBookOfTheWeek] = useState(null);
    let [SingleBookOfTheWeek, SetSingleBookOfTheWeek] = useState(null);
    let [DataSliderAuthor] = useState([]);
    let [DataSliderBook] = useState([]);
    let [Suggested] = useState([
        {
            title: __("Book"),
            mode: "book",
            data: []
        },
        {
            title: __("Article"),
            mode: "article",
            data: []
        },
        {
            title: __("Journal"),
            mode: "journal",
            data: []
        }]);


    async function getInformation() {
        SetLoading(true);
        await axios.get(defaultRahUrl + `/home`)
            .then(result => {
                if (result.status === 200) {
                    props.dispatch(DataForSideBar(
                        result.data.data.sidebar &&
                        result.data.data.sidebar.data &&
                        result.data.data.sidebar.data.categories &&
                        result.data.data.sidebar.data.categories.nodes
                    ));
                    SetSettings(result.data.data.settings);
                    SetPackage(
                        result.data.data.packages &&
                        result.data.data.packages
                    );
                    result.data.data.new_title &&
                    result.data.data.new_title.books &&
                    result.data.data.new_title.books.map(x => {
                        return NewTitles[0].data.push(
                            PostBookConverter(x)
                        )
                    });
                    result.data.data.new_title &&
                    result.data.data.new_title.article &&
                    result.data.data.new_title.article.map(x => {
                        return NewTitles[1].data.push(
                            PostArticleConverter(x)
                        )
                    });
                    result.data.data.new_title &&
                    result.data.data.new_title.magazine &&
                    result.data.data.new_title.magazine.map(x => {
                        return NewTitles[2].data.push(
                            PostBookConverter(x)
                        )
                    });
                    result.data.data.subject_box &&
                    result.data.data.subject_box.books &&
                    result.data.data.subject_box.books.map(x => {
                        return SubjectBox[0].data.push(
                            PostBookConverter(x)
                        )
                    });
                    result.data.data.subject_box &&
                    result.data.data.subject_box.article &&
                    result.data.data.subject_box.article.map(x => {
                        return SubjectBox[1].data.push(
                            PostArticleConverter(x)
                        )
                    });

                    result.data.data.subject_box &&
                    result.data.data.subject_box.magazine &&
                    result.data.data.subject_box.magazine.map(x => {
                        return SubjectBox[2].data.push(
                            PostBookConverter(x)
                        )
                    });
                    SetSubjectTitle(
                        result.data.data.subject_box &&
                        result.data.data.subject_box.subject
                    );
                    result.data.data.hot_titles &&
                    result.data.data.hot_titles.books &&
                    result.data.data.hot_titles.books.map(x => {
                        return HotTitles[0].data.push(
                            PostBookConverter(x)
                        )
                    });
                    result.data.data.hot_titles &&
                    result.data.data.hot_titles.article &&
                    result.data.data.hot_titles.article.map(x => {
                        return HotTitles[1].data.push(
                            PostArticleConverter(x)
                        )
                    });

                    result.data.data.hot_titles &&
                    result.data.data.hot_titles.magazine &&
                    result.data.data.hot_titles.magazine.map(x => {
                        return HotTitles[2].data.push(
                            PostBookConverter(x)
                        )
                    });
                    SetAuthorOfTheWeek(
                        result.data.data.author_of_the_week &&
                        result.data.data.author_of_the_week
                    );
                    if (result.data.data.author_of_the_week.mode === "multiple") {
                        result.data.data.author_of_the_week.data.map(data => {
                            return DataSliderAuthor.push(
                                {
                                    image: data.attachment,
                                    title: data.post_title,
                                    text: data.post_content,
                                    count: data.meta_fields.rate,
                                    numberOfVisits: data.meta_fields.view
                                }
                            )
                        });
                    } else SetSingleAuthorOfTheWeek(result.data.data.author_of_the_week.data);
                    result.data.data.suggested &&
                    result.data.data.suggested.books &&
                    result.data.data.suggested.books.map(x => {
                        return Suggested[0].data.push(
                            PostBookConverter(x)
                        )
                    });
                    result.data.data.suggested &&
                    result.data.data.suggested.article &&
                    result.data.data.suggested.article.map(x => {
                        return Suggested[1].data.push(
                            PostArticleConverter(x)
                        )
                    });

                    result.data.data.suggested &&
                    result.data.data.suggested.magazine &&
                    result.data.data.suggested.magazine.map(x => {
                        return Suggested[2].data.push(
                            PostBookConverter(x)
                        )
                    });
                    SetBookOfTheWeek(
                        result.data.data.book_of_the_week &&
                        result.data.data.book_of_the_week
                    );
                    if (result.data.data.book_of_the_week.mode === "multiple") {
                        result.data.data.book_of_the_week.data.map(data => {
                            return DataSliderBook.push(
                                {
                                    image: data.attachment,
                                    title: data.post_title,
                                    text: data.post_content,
                                    count: data.meta_fields ? data.meta_fields.rate : 0,
                                    numberOfVisits: data.meta_fields.view
                                }
                            )
                        });
                    } else SetSingleBookOfTheWeek(result.data.data.book_of_the_week.data);
                }
            });
        SetLoading(false);
    }


    const tabs_P = [
        {
            title: "Picture",
            mode: "Picture",
            data: [
                {
                    src: one_P,
                    text: "Don't be Sad"
                },
                {
                    src: two_P,
                    text: "Da Iran War"
                }
                ,
                {
                    src: tree_P,
                    text: "Unity In Islamic"
                },
                {
                    src: four_P,
                    text: "Life In the eyes of Islam"
                },
                {
                    src: five_P,
                    text: "Study In Islam"
                },
                {
                    src: six_P,
                    text: "Lying In Islam"
                },
                {
                    src: seven_P,
                    text: "Exercise In Islam"
                },
            ]
        },
        {
            title: "Sound",
            mode: "Picture",
            data: [
                {
                    src: one_S,
                    text: "Stone in islam"
                },
                {
                    src: two_S,
                    text: "When God Say Hello To Me"
                }
                ,
                {
                    src: tree_S,
                    text: "Satan: the enemy in the blood things"
                },
                {
                    src: four_S,
                    text: "Don ' t be Sad"
                },
                {
                    src: five_S,
                    text: "Don ' t be Sad"
                },
                {
                    src: six_S,
                    text: "Don ' t be Sad"
                },
                {
                    src: seven_S,
                    text: "Don ' t be Sad"
                },
            ]
        },
        {
            title: "Video",
            mode: "Video",
            data: [
                {
                    src: one_V,
                    text: "Stone in islam"
                },
                {
                    src: two_V,
                    text: "When God Say Hello To Me"
                }
                ,
                {
                    src: tree_V,
                    text: "Satan: the enemy in the blood things"
                },
                {
                    src: four_V,
                    text: "Don ' t be Sad"
                },
            ]
        }
    ];

    useEffect(() => {
        getInformation();
    }, []);

    return (
        <div className="w-100">
            {!loading ?
                Settings.three_first_section &&
                <React.Fragment>
                    <BookCollection
                        title={__("New Titles")}
                        name="books"
                        hr
                        more
                        tabs={NewTitles}
                    />
                    <Pictures data={Package && Package}/>
                    {SubjectTitle &&
                    <div className="w-100 mt-3 mt-md-5">
                        <BookCollection
                            title={SubjectTitle}
                            hr={false}
                            more
                            tabs={SubjectBox}
                            styleHejab={{width: "100%"}}
                        />
                    </div>
                    }
                </React.Fragment>
                :
                <div
                    className="
                        w-100 d-flex
                        justify-content-center
                        align-items-center spin"
                >
                    <Spin
                        spinning
                        indicator={
                            <img className="w-25 h-auto" src={loadingItem} alt="loading"/>
                        }
                    />
                </div>
            }
            {!loading ?
                Settings.popular_titles_section &&
                <BookCollection
                    title={__("Popular Titles")}
                    name="books"
                    hr
                    tabs={HotTitles}
                />
                :
                <div
                    className="
                        w-100 d-flex
                        justify-content-center
                        align-items-center spin"
                >
                    <Spin
                        spinning
                        indicator={
                            <img
                                className="w-25 h-auto"
                                src={loadingItem}
                                alt="loading"
                            />
                        }
                    />
                </div>
            }
            {!loading &&
            Settings.author_of_the_week_section &&
            authorOfTheWeek.mode !== "multiple" ?
                <AuthorOfTheWeek
                    data={
                        SingleAuthorOfTheWeek &&
                        SingleAuthorOfTheWeek
                    }
                />
                :
                <SliderMainPage
                    data={DataSliderAuthor}
                    title={__("Authors of The Week")}
                />
            }
            {!loading ?
                Settings.suggested_section &&
                <BookCollection
                    title={__("Suggested")}
                    name="books"
                    hr
                    more
                    tabs={Suggested}
                />
                :
                <div
                    className="
                        w-100 d-flex
                        justify-content-center
                        align-items-center spin"
                >
                    <Spin
                        spinning
                        indicator={
                            <img
                                className="w-25 h-auto"
                                src={loadingItem}
                                alt="loading"
                            />
                        }
                    />
                </div>
            }
            {!loading &&
            Settings.book_of_the_week_section &&
            BookOfTheWeek.mode !== "multiple" ?
                <BookOfTheWeekFirst data={SingleBookOfTheWeek}/>
                :
                <SliderMainPage
                    data={DataSliderBook}
                    title={__("Books of The Week")}
                />
            }
            {!loading &&
            Settings.media_section &&
            <Media
                title={__("Media")}
                name="media"
                hr
                tabs={tabs_P}
                more
            />
            }
        </div>
    );
}

const mapStateToProps = states => (
    {
        RtlDir: states.Rtl,
        Single: states.SingleInHomePageTitle,
        SingleSlug: states.SingleInHomePageSlug
    }
);
export default withRouter(connect(mapStateToProps)(Main));
