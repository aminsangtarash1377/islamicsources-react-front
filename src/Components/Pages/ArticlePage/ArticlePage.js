import React, {useEffect, useState} from 'react';
import {BookCollection, TabSection, Article, CardView} from "../../Module";
import axios from "axios";
import {defaultRahUrl, loadingItem} from "../../../config";
import connect from "react-redux/es/connect/connect";
import {Link, useLocation, withRouter} from "react-router-dom";
import {DataForSideBar, LoadingForSidebar} from "../../../actions";
import {__} from "../../../global";
import {Spin} from "antd";

function ArticlePage(props) {
    const Location = useLocation();
    let [data, SetData] = useState(null);
    let [slug, setSlug] = useState(null);
    let [show, setShow] = useState("book");
    let [dataSingle, SetDataSingle] = useState(null);
    let [endCursor, SetEndCursor] = useState(null);
    let [NextPage, SetNextPage] = useState(null);
    let [BookData] = useState([]);
    let [ArticleData] = useState([]);
    let [MagazineData] = useState([]);
    let [Box, SetBox] = useState(null);
    let [loading, SetLoading] = useState(false);

    async function onClickShow(e) {
        await setShow(e.currentTarget.getAttribute("name"));
    }

    let tabletr = ["Title", "Writer", "Year", "Publisher", "Format", "Rate"];

    async function getInformation() {
        await props.dispatch(LoadingForSidebar(true));
        await axios.get(defaultRahUrl + "/categories?per_page_book=0&per_page_article=6&books=0&articles=1")
            .then(result => {
                    if (result.status === 200) {
                        props.dispatch(DataForSideBar(
                            result.data &&
                            result.data.category &&
                            result.data.category.data &&
                            result.data.category.data.categories &&
                            result.data.category.data.categories.nodes
                        ));
                        SetData(
                            result.data &&
                            result.data.category &&
                            result.data.category.data &&
                            result.data.category.data.categories &&
                            result.data.category.data.categories.nodes);
                        result.data &&
                        result.data.subjectBox &&
                        result.data.subjectBox.books &&
                        result.data.subjectBox.books.data &&
                        result.data.subjectBox.books.data.books &&
                        result.data.subjectBox.books.data.books.nodes &&
                        result.data.subjectBox.books.data.books.nodes.map(x => {
                            BookData.push(x);
                        });
                        result.data &&
                        result.data.subjectBox &&
                        result.data.subjectBox.articles &&
                        result.data.subjectBox.articles.data &&
                        result.data.subjectBox.articles.data.articles &&
                        result.data.subjectBox.articles.data.articles.nodes &&
                        result.data.subjectBox.articles.data.articles.nodes.map(x => {
                            ArticleData.push(x);
                        });
                        result.data &&
                        result.data.subjectBox &&
                        result.data.subjectBox.magazines &&
                        result.data.subjectBox.magazines.data &&
                        result.data.subjectBox.magazines.data.magazines &&
                        result.data.subjectBox.magazines.data.magazines.nodes &&
                        result.data.subjectBox.magazines.data.magazines.nodes.map(x => {
                            MagazineData.push(x);
                        });
                        SetBox(
                            result.data &&
                            result.data.category &&
                            result.data.category
                        )
                    }
                }
            );
        await props.dispatch(LoadingForSidebar(false));
    }

    async function getSingleData() {
        SetLoading(true);
        await axios.get(defaultRahUrl +
            `/category/articles?per_page=12&slug=${props.SingleSlug}&end=${slug === props.SingleSlug ? endCursor : ""}`
        )
            .then(result => {
                if (result.status === 200) {
                    if (slug === props.SingleSlug) {
                        result.data.articles.data.articles.nodes.forEach(x => {
                            dataSingle.push(x);
                        });
                    } else {
                        SetDataSingle(
                            result.data &&
                            result.data.articles &&
                            result.data.articles.data &&
                            result.data.articles.data.articles &&
                            result.data.articles.data.articles.nodes);
                    }
                    SetEndCursor(
                        result.data &&
                        result.data.articles &&
                        result.data.articles.data &&
                        result.data.articles.data.articles &&
                        result.data.articles.data.articles.pageInfo &&
                        result.data.articles.data.articles.pageInfo.endCursor);
                    SetNextPage(
                        result.data &&
                        result.data.articles &&
                        result.data.articles.data &&
                        result.data.articles.data.articles &&
                        result.data.articles.data.articles.pageInfo &&
                        result.data.articles.data.articles.pageInfo.hasNextPage);
                    setSlug(props.SingleSlug);
                }
            });
        SetLoading(false)
    }

    useEffect(() => {
            !props.Single && getInformation();
            props.Single && getSingleData();
        },
        [props.Single]);

    const borderedOne = [
        {
            title: __("Book"),
            mode: "book",
            data: BookData
        },
        {
            title: __("Article"),
            mode: "article",
            data: ArticleData
        },
        {
            title: __("Journal"),
            mode: "book",
            data: MagazineData
        }
    ];
    return (
        (
            Location.pathname.split("/")[2] === ""
            ||
            Location.pathname.split("/")[2] === undefined
        ) &&
        <React.Fragment>
            {props.Single !== null
                ?
                <div className="w-100 d-flex flex-column align-items-center">
                    <div className="my-2 w-100">
                        <TabSection
                            title={props.Single}
                            IconCategory={true}
                            hr
                            onClickShow={onClickShow}
                            moreLink={connect && connect.title}
                        />
                    </div>
                    {show === "book" &&
                    <div className="w-100 d-flex flex-row flex-wrap justify-content-between mt-4">
                        {!loading ? dataSingle && dataSingle.map((x, i) => (
                            <Article
                                key={i}
                                link="Articles"
                                title={x.title && x.title}
                                slug={x.slug && x.slug}
                                rate={x.articlefields &&
                                (x.articlefields.rate === "INF" || x.articlefields.rate === null)
                                    ?
                                    "-"
                                    :
                                    x.articlefields.rate}
                                view={x.articlefields && x.articlefields.view}
                                author={
                                    x.articlefields &&
                                    x.articlefields.actAuthor &&
                                    x.articlefields.actAuthor[0] &&
                                    x.articlefields.actAuthor[0].title
                                }
                            />
                        ))
                            :
                            <div
                                className="w-100 d-flex
                                 justify-content-center
                                 align-items-center spin"
                            >
                                <Spin
                                    spinning
                                    indicator={
                                        <img
                                            className="w-25 h-auto"
                                            src={loadingItem}
                                            alt="loading"
                                        />
                                    }
                                />
                            </div>
                        }
                    </div>
                    }
                    {show === "bars" &&
                    <div className="w-100 d-flex flex-row flex-wrap justify-content-between">
                        {!loading ?
                            dataSingle && dataSingle.map((x, i) => (
                                <CardView
                                    data={x}
                                    key={i}
                                    type="article"
                                    link="Books"
                                />
                            ))
                            :
                            <div
                                className="w-100 d-flex
                                 justify-content-center
                                 align-items-center spin"
                            >
                                <Spin
                                    spinning
                                    indicator={
                                        <img
                                            className="w-25 h-auto"
                                            src={loadingItem}
                                            alt="loading"
                                        />
                                    }
                                />
                            </div>
                        }
                    </div>
                    }
                    {show === "table" &&
                    <table className="TableSubject w-100 mt-2">
                        <tr className="w-100">
                            <th className="font-F-Gothic"
                                style={{width: "20%"}}>
                                {tabletr[0]}
                            </th>
                            <th className="font-F-Gothic"
                                style={{width: "20%"}}>
                                {tabletr[1]}
                            </th>
                            <th className="font-F-Gothic"
                                style={{width: "15%"}}>
                                {tabletr[2]}
                            </th>
                            <th className="font-F-Gothic"
                                style={{width: "18%"}}>
                                {tabletr[3]}
                            </th>
                            <th className="font-F-Gothic"
                                style={{width: "13%"}}>
                                {tabletr[4]}
                            </th>
                            <th className="font-F-Gothic"
                                style={{width: "10%"}}>
                                {tabletr[5]}
                            </th>
                        </tr>
                        {!loading ? dataSingle && dataSingle.map((x, i) => (
                            <Link key={i} to={`/Books/${x.slug}/`}>
                                <tr className="w-100">
                                    <td
                                        style={{width: "20%"}}
                                        key={i}
                                        className="font-F-Gothic">
                                        <h4>{x.title}</h4>
                                    </td>
                                    <td
                                        style={{width: "20%"}}
                                        key={i}
                                        className="font-F-Gothic">
                                        <h4>
                                            {x.bookfields && x.bookfields.actAuthor
                                                ?
                                                x.bookfields.actAuthor
                                                :
                                                "---"
                                            }
                                        </h4>
                                    </td>
                                    <td
                                        style={{width: "15%"}}
                                        key={i}
                                        className="font-F-Gothic">
                                        <h4>
                                            {x.bookfields && x.bookfields.actPublishDate
                                                ?
                                                x.bookfields.actPublishDate
                                                :
                                                "---"
                                            }
                                        </h4>
                                    </td>
                                    <td
                                        style={{width: "18%"}}
                                        key={i}
                                        className="font-F-Gothic">
                                        <h4>
                                            {x.bookfields &&
                                            x.bookfields.actPublisher &&
                                            x.bookfields.actPublisher.title ?
                                                x.bookfields.actPublisher.title : "---"
                                            }
                                        </h4>
                                    </td>
                                    <td
                                        style={{width: "13%"}}
                                        key={i}
                                        className="font-F-Gothic">
                                        <h4>
                                            {x.bookfields && x.bookfields.actFile ? x.bookfields.actFile.map(x => (
                                                x.actFileFormat
                                            )) : "---"}
                                        </h4>
                                    </td>
                                    <td
                                        style={{width: "10%"}}
                                        key={i}
                                        className="font-F-Gothic">
                                        <h4>{x.bookfields && x.bookfields.rate}</h4>
                                    </td>
                                </tr>
                            </Link>
                        ))
                            :
                            <div
                                className="w-100 d-flex
                                 justify-content-center
                                 align-items-center spin"
                            >
                                <Spin
                                    spinning
                                    indicator={
                                        <img
                                            className="w-25 h-auto"
                                            src={loadingItem}
                                            alt="loading"
                                        />
                                    }
                                />
                            </div>
                        }
                    </table>
                    }
                    {NextPage &&
                    <button
                        className="More my-3 my-md-5 font-F-Gothic"
                        onClick={async () => {
                            await getSingleData();
                        }}
                    >
                        More
                    </button>
                    }
                </div>
                :
                <div className="d-flex flex-column">
                    {data ? data.slice(0, 2).map((x, i) => (
                            <React.Fragment key={i}>
                                <TabSection
                                    title={x.name}
                                    hr
                                    moreLink={{pathname: `/Subject/${x.slug}/Articles/`, state: "Articles", title: x.name}}
                                    more
                                    className="my-4"
                                />
                                <div
                                    className="d-flex flex-column flex-lg-row
                                         flex-lg-wrap justify-content-between"
                                >
                                    {x.articles.data && x.articles.data.articles.nodes.map((x, i) => (
                                        <Article
                                            link="Articles"
                                            key={i}
                                            title={x.title}
                                            slug={x.slug && x.slug}
                                            rate={x.articlefields &&
                                            (x.articlefields.rate === "INF" || x.articlefields.rate === null)
                                                ?
                                                "-"
                                                :
                                                x.articlefields.rate}
                                            view={
                                                x.articlefields &&
                                                x.articlefields.view
                                            }
                                            author={
                                                x.articlefields &&
                                                x.articlefields.actAuthor &&
                                                x.articlefields.actAuthor[0] &&
                                                x.articlefields.actAuthor[0].title

                                            }
                                        />
                                    ))}
                                </div>
                            </React.Fragment>
                        ))
                        :
                        <div
                            className="w-100 d-flex
                                 justify-content-center
                                 align-items-center spin"
                        >
                            <Spin
                                spinning
                                indicator={
                                    <img
                                        className="w-25 h-auto"
                                        src={loadingItem}
                                        alt="loading"
                                    />
                                }
                            />
                        </div>
                    }
                    {data &&
                    <BookCollection
                        hr={false}
                        title={Box && Box.subjectName}
                        more
                        tabs={borderedOne}
                        moreLink={''}/>
                    }
                    {data && data.slice(2, 3).map((x, i) => (
                        <React.Fragment key={i}>
                            <TabSection
                                title={x.name}
                                hr={true}
                                moreLink={{pathname: `/Subject/${x.slug}/Articles/`, state: "Articles", title: x.name}}
                                more={true}
                                className="my-4"
                            />
                            <div
                                className="d-flex flex-column flex-lg-row
                        flex-lg-wrap justify-content-between"
                            >
                                {x.articles.data && x.articles.data.articles.nodes.map((x, i) => (
                                    <Article
                                        link="Articles"
                                        key={i}
                                        title={x.title}
                                        slug={x.slug && x.slug}
                                        rate={x.articlefields &&
                                        (x.articlefields.rate === "INF" || x.articlefields.rate === null)
                                            ?
                                            "-"
                                            :
                                            x.articlefields.rate}
                                        view={
                                            x.articlefields &&
                                            x.articlefields.view
                                        }
                                        author={
                                            x.articlefields &&
                                            x.articlefields.actAuthor &&
                                            x.articlefields.actAuthor[0] &&
                                            x.articlefields.actAuthor[0].title
                                        }
                                    />
                                ))}
                            </div>
                        </React.Fragment>
                    ))}
                </div>
            }
        </React.Fragment>
    );
}

const mapStateToProps = states => (
    {
        Single: states.SingleInArticlesPageTitle,
        SingleSlug: states.SingleInArticlesPageSlug
    }
);
export default withRouter(connect(mapStateToProps)(ArticlePage));