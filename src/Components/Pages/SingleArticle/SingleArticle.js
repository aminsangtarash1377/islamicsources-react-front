import React, {useEffect, useState} from 'react';
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";
import PropTypes from "prop-types";
import {BookCollection, SpinLoading} from "../../Module";
import {defaultRahUrl, loadingItem, ISAccessUserToken, defaultWPUrl, setRateByUser} from "../../../config";
import {Col, Rate, Row, Spin, message, Icon} from "antd";
import {__} from "../../../global";
import axios from "axios";
import BookMarkFalse from '../../../images/neshan.svg';
import BookMarkTrue from '../../../images/neshan A.svg';
import './SingleArticle.css';

InformationItem.propTypes = {
    name: PropTypes.string.isRequired,
    response: PropTypes.string.isRequired
};

function InformationItem(props) {
    return (
        <div className="d-flex justify-content-start my-1">
            <span
                className="text-bold font-color-Information-text"
            >
                {props.name}:
            </span>
            <span
                className="mx-1 text-black font-color-Information-text"
            >
                {props.response}
            </span>
        </div>
    )
}

function SingleArticle(props) {
    const [dataBook, setDataBook] = useState(null);
    const [dataRate, setDataRate] = useState(null);
    let [dataSimilarBook] = useState([]);
    let [dataSimilarArticle] = useState([]);
    let [dataSimilarMagazines] = useState([]);
    let [data, SetData] = useState(null);
    let [loading, SetLoading] = useState(true);
    const [bookMark, SetBookMark] = useState(false);
    const [rateLoading, setRateLoading] = useState(false);
    const [rateVoteAmount, SetRateVoteAmount] = useState(0);

    const tabs = [
        {
            title: __("Book"),
            mode: "book",
            data: dataSimilarBook
        },
        {
            title: __("Article"),
            mode: "article",
            data: dataSimilarArticle
        },
        {
            title: __("Journal"),
            mode: "journal",
            data: dataSimilarMagazines
        }
    ];

    async function getData() {
        SetLoading(true);
        await axios.get(defaultRahUrl + `/article?slug=${props.match.params.slug}`)
            .then(result => {
                const rateVote = parseInt(result.data.article.data.article.articlefields.votes);
                SetRateVoteAmount(isNaN(rateVote) ? 0 : rateVote);
                SetBookMark(result.data.article.data.article.bookmarked);
                setDataBook(result);
                SetData(result.data.article.data.article);
                setDataRate(
                    result.data.article.data.article.articlefields &&
                    result.data.article.data.article.articlefields
                );
                result.data.similarActs.data.books.nodes.map((d) => {
                    return dataSimilarBook.push(d);
                });
                result.data.similarActs.data.articles.nodes.map((d) => {
                    return dataSimilarArticle.push(d);
                });
                result.data.similarActs.data.magazines.nodes.map((d) => {
                    return dataSimilarMagazines.push(d);
                });
            });
        SetLoading(false);
    }


    useEffect(() => {
        getData()
    }, []);

    function sendRate(e, id) {
        setRateLoading(true);
        if (setRateByUser(id)) {
            axios.post(defaultRahUrl + `/rate?type=post&id=${id}&rate=${e}`)
                .then(res =>
                    {
                        if (res.status === 200) {
                            SetRateVoteAmount(rateVoteAmount + 1);
                            setDataRate(res.data.rate);
                            message.success("Done");
                        }
                    }
                )
        } else {
            message.warning(__('You rate this post once'))
        }
        setRateLoading(false);
    }

    async function SetBookMarking(PostId) {
        await axios.post(defaultWPUrl + `/users/BookMarks`,
            {"PostId": PostId},
            {
                headers: {
                    "Content-Type": "application/json",
                    Authorization: ISAccessUserToken
                }
            }
        )
            .then(result => {
                if (result.status === 200) {
                    SetBookMark(result.data.data);
                    message.success(__('Done'));
                }
            })
            .catch(res => {
                if (res.response && res.response.data.message) {
                    message.error(__(res.response.data.message))
                }
            });
    }

    /**
     * @return {string}
     */
    function Get_Field(data) {
        if (data.value.ID)
            return data.value.post_title;
        else {
            return data.value
        }
    }


    return (
        <React.Fragment>
            {!loading
                ?
                <div className="d-flex flex-column">
                    <Row
                        className="
                        d-flex
                        border
                        justify-content-between
                        align-items-center
                        mt-2
                        BookInfoInformation
                        InformationArticleInfo
                        "
                    >
                        <Col
                            span={12}
                            lg={18}
                            xs={24}
                            className="font-F-Gothic"
                        >
                            <div className="w-100 d-flex flex-column flex-md-row">
                                <Col span={12} lg={12}>
                                    <h3 className="text-bold">
                                        {
                                            data && data.title
                                        }
                                    </h3>
                                    {
                                        data &&
                                        data.valuesToShow
                                        && data.valuesToShow.map((data, i) => {
                                            if (data.label === "author" || data.label === "source")
                                                return <InformationItem
                                                    key={i}
                                                    name={data.label}
                                                    response={Get_Field(data)}
                                                />
                                        })
                                    }
                                </Col>
                                <Col span={12} lg={12}>
                                    <Row type="flex" align="middle" className="pt-3 d-lg-flex flex-lg-column d-none">
                                        <div className="d-flex">
                                            <span
                                                className="mx-3 text-black"
                                                style={{fontSize: ".75rem"}}
                                            >
                                                {
                                                    data &&
                                                    rateVoteAmount + " " + (__("vote"))
                                                }
                                            </span>
                                            <span
                                                className="mx-3 text-black"
                                                style={{fontSize: ".75rem"}}
                                            >
                                                {
                                                    data &&
                                                    data.articlefields &&
                                                    data.articlefields.view ?
                                                        data.articlefields.view + ` ${(__(`View`))}`
                                                        :
                                                        1 + (__(`view`))
                                                }
                                            </span>
                                        </div>
                                        <div className="d-flex mt-3">
                                            <SpinLoading
                                                loading={rateLoading}
                                                mode="loading"
                                            >
                                                <Rate
                                                    className=""
                                                    value={
                                                        !loading &&
                                                        dataRate !== "INF" &&
                                                        data.articlefields.rate > 0.5 ?
                                                            Math.round(data.articlefields.rate) :
                                                            1
                                                    }
                                                    onChange={(e) => sendRate(e, dataBook && dataBook.data.article.data.article.articleId)}
                                                    character={
                                                        <div
                                                            className="position-relative "
                                                            style={{
                                                                width: "1rem",
                                                                height: "1rem",
                                                            }}
                                                        >
                                                            <div
                                                                style={{
                                                                    width: ".9rem",
                                                                    height: ".9rem",
                                                                    background: "#b3b3b3",
                                                                    borderRadius: "2px"
                                                                }}
                                                                className="RatingIslamicSources position-absolute"
                                                            />
                                                            <div
                                                                style={{
                                                                    width: ".9rem",
                                                                    height: ".9rem",
                                                                    background: "#b3b3b3",
                                                                    transform: "rotate(45deg)",
                                                                    borderRadius: "2px"
                                                                }}
                                                                className="RatingIslamicSources position-absolute"
                                                            />
                                                        </div>
                                                    }
                                                    allowHalf={false}
                                                />
                                            </SpinLoading>
                                            <img
                                                src={bookMark ? BookMarkTrue : BookMarkFalse}
                                                className="mx-4"
                                                alt=""
                                                onClick={() => SetBookMarking(
                                                    data && data.articleId
                                                )}
                                                style={{width: ".65rem"}}
                                            />
                                            <div className="ShareIcon d-flex align-items-center">
                                                <Icon type="share-alt"/>
                                            </div>
                                        </div>
                                    </Row>
                                </Col>
                            </div>
                        </Col>
                        <Col
                            span={24}
                            lg={6}
                            className="d-lg-flex d-none align-items-center justify-content-center"
                        >
                            <div className="d-flex flex-column align-items-center">
                                <img
                                    src={data && data.articlefields &&
                                    data.articlefields.actQrCode}
                                    alt="scan book"
                                    width={140}
                                    className="mb-1"
                                />
                            </div>
                        </Col>
                    </Row>
                    <Row
                        type="flex"
                        align="middle"
                        className="px-2 mt-3 d-lg-none d-flex"
                    >
                        <SpinLoading
                            loading={rateLoading}
                            mode="loading"
                        >
                            <Rate
                                value={
                                    !loading &&
                                    dataRate !== "INF" &&
                                    data.articlefields.rate > 0.5 ?
                                        Math.round(data.articlefields.rate) :
                                        1
                                }
                                onChange={(e) => sendRate(e, dataBook && dataBook.data.article.data.article.articleId)}
                                character={
                                    <div
                                        className="position-relative "
                                        style={{
                                            width: "1.5rem",
                                            height: "1rem",
                                        }}
                                    >
                                        <div
                                            style={{
                                                width: "1.20rem",
                                                height: "1.20rem",
                                                background: "#b3b3b3",
                                                borderRadius: "2px"
                                            }}
                                            className="RatingIslamicSources position-absolute"
                                        />
                                        <div
                                            style={{
                                                width: "1.20rem",
                                                height: "1.20rem",
                                                background: "#b3b3b3",
                                                transform: "rotate(45deg)",
                                                borderRadius: "2px"
                                            }}
                                            className="RatingIslamicSources position-absolute"
                                        />
                                    </div>
                                }
                                allowHalf={false}
                            />
                        </SpinLoading>
                        <span
                            className="mx-3 text-black"
                            style={{fontSize: "1rem"}}
                        >
                                 {
                                     data &&
                                     rateVoteAmount + " " + (__("vote"))
                                 }
                            </span>
                        <img
                            src={bookMark ? BookMarkTrue : BookMarkFalse}
                            className="mx-2 h-100"
                            alt=""
                            onClick={() => SetBookMarking(
                                data && data.articleId
                            )}
                            style={{width: ".75rem"}}
                        />
                        <div className="ShareIcon mx-2 d-flex align-items-center">
                            <Icon type="share-alt"/>
                        </div>
                    </Row>
                </div>
                :
                <div
                    className="w-100 d-flex
                        justify-content-center
                        align-items-center spin"
                >
                    <Spin
                        spinning
                        indicator={
                            <img
                                className="w-25 h-auto"
                                src={loadingItem}
                                alt="loading"
                            />
                        }
                    />
                </div>
            }
            <Row className="my-5 Introduction-text-book">
                <h2
                    className="font-F-Gothic text-bold"
                >
                    {(__("Introduction"))}
                </h2>

                <div
                    className="font-F-Gothic"
                    dangerouslySetInnerHTML={{
                        __html:
                            data && data.content
                    }}
                />
            </Row>
            <BookCollection
                title={__("Similar titles")}
                hr
                tabs={tabs}
                name="books"
            />
        </React.Fragment>
    )
}

const mapStateToProps = states => (
    {
        RtlDir: states.Rtl
    }
);
export default withRouter(connect(mapStateToProps)(SingleArticle));