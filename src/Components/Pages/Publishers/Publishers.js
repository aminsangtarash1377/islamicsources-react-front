import React, {useEffect, useState} from 'react';
import {TabSection, Book} from "../../Module";
import axios from "axios";
import {defaultRahUrl, loadingItem} from "../../../config";
import {useLocation} from "react-router-dom";
import {Spin} from "antd";
import {__} from "../../../global";
import "./Publishers.css";

export default function Publishers() {
    const [data] = useState([]);
    const [endCursor, SetEndCursor] = useState("");
    const [NextPage, SetNextPage] = useState(false);
    const [loading, SetLoading] = useState(false);
    const Location = useLocation();

    async function getInformation() {
        SetLoading(true);
        await axios.get(
            defaultRahUrl + `/publishers?per_page=21&end=${endCursor}`
        )
            .then(result => {
                if (result.status === 200) {
                    result.data &&
                    result.data.publishers &&
                    result.data.publishers.data &&
                    result.data.publishers.data.publishers &&
                    result.data.publishers.data.publishers.nodes.forEach((x) => {
                        data.push(x);
                    });
                    SetNextPage(
                        result.data &&
                        result.data.publishers &&
                        result.data.publishers.data &&
                        result.data.publishers.data.publishers &&
                        result.data.publishers.data.publishers.pageInfo &&
                        result.data.publishers.data.publishers.pageInfo.hasNextPage);
                    SetEndCursor(
                        result.data &&
                        result.data.publishers &&
                        result.data.publishers.data &&
                        result.data.publishers.data.publishers &&
                        result.data.publishers.data.publishers.pageInfo &&
                        result.data.publishers.data.publishers.pageInfo.endCursor);
                }
            });
        SetLoading(false);
    }

    useEffect(() => {
        getInformation()
    }, []);

    return (
        (
            Location.pathname.split("/")[2] === ""
            ||
            Location.pathname.split("/")[2] === undefined
        )
        &&
        <div className="w-100 d-flex flex-column Publishers">
            <div className="w-100 mt-5">
                <TabSection
                    title={__("Publishers")}
                    hr
                />
            </div>
            <div
                className="w-100 d-flex flex-wrap
                justify-content-between BooksPublishers my-4"
            >
                {!loading ? data && data.map((x, i) => (
                    <Book
                        data={x}
                        key={i}
                        targetLocation="Publishers"
                        popover="Person"
                        className="Book"
                    />
                ))
                    :
                    <div
                        className="w-100 d-flex
                            justify-content-center
                            align-items-center spin"
                    >
                        <Spin
                            spinning
                            indicator={
                                <img
                                    className="w-25 h-auto"
                                    src={loadingItem}
                                    alt="loading"
                                />
                            }
                        />
                    </div>
                }
            </div>
            {NextPage &&
            <div className="w-100 d-flex justify-content-center mb-5">
                <button
                    className="More font-F-Gothic"
                    onClick={() => getInformation()}>
                    {__("More")}
                </button>
            </div>
            }
        </div>
    )
}