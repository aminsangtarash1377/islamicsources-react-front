import React from 'react';
import ReactDOM from 'react-dom';
import 'antd';
import {Provider} from 'react-redux';
import App from './App';
import {BrowserRouter as Router} from "react-router-dom";
import {createStore} from "redux";
import reducers from './reducers';
import './css/style.css';
import './css/bootstrap.custom.min.css';
import './css/AntDesignRtl.css';
import 'antd/dist/antd.css';

const store = createStore(reducers);

ReactDOM.render(
    <Provider store={store}>
        <Router>
            <App/>
        </Router>
    </Provider>
    , document.getElementById('root'));

