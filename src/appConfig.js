module.exports = {
    baseUrl : "http://test1.islamic-sources.com/core",
    // baseUrl : "http://localhost/is/index.php",
    introBaseUrl : "http://test1.islamic-sources.com/languages",
    getCookie : function (cookieName) {
        const name = cookieName + "=";
        const decodedCookie = decodeURIComponent(document.cookie);
        const ca = decodedCookie.split(';');
        for (let i = 0; i < ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) === ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) === 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    },
    setCookie : function (cookieName, cookieValue, expiredDays) {
        const d = new Date();
        d.setTime(d.getTime() + (expiredDays*24*60*60*1000));
        const expires = "expires="+ d.toUTCString();
        document.cookie = cookieName + "=" + cookieValue + ";" + expires + ";path=/";
    },
    deleteCookie(cookieName){
        return document.cookie = "" + cookieName + "=; expires=Thu, 01 Jan 1970 00:00:00 UTC;";
    },
};