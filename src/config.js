const {baseUrl, introBaseUrl, getCookie, setCookie} = require('./appConfig');
const loadingGif = require('./images/loading/loading.gif');

const token = getCookie("ISAccessUserToken");
module.exports = {
    rtlMode : true,
    lang: "fa",
    introUrl: introBaseUrl + "/wp-json/rah/v1",
    defaultRahUrl: baseUrl + "/wp-json/rah/v1",
    defaultWPUrl: baseUrl + "/wp-json/wp/v2",
    ISAccessUserToken : token !== "" ? "Bearer " + token : "",
    loadingItem : loadingGif,
    words : [],
    newWords : [],
    PostBookConverter: function (data) {
        return {
            featuredImage: {sourceUrl: data.attachment},
            title: data.post_title,
            slug: data.post_name,
            key: data.ID,
            bookfields: {
                rate: data.meta_fields && data.meta_fields.rate,
                actPublishDate: data.meta_fields && data.meta_fields.act_publish_date,
                view: data.meta_fields && data.meta_fields.view,
                actAuthor: [{title: data.meta_fields && data.meta_fields.act_author &&
                        data.meta_fields.act_author[0] &&
                        data.meta_fields.act_author[0].post_title}]
            }
        }
    },
    /**
     * @return {string}
     */
    StripHTML : function(string){
        string = string.toString();
        return string.replace(/<[^>]*>/g, '');
    },
    PostArticleConverter: function (data) {
        return {
            title: data.post_title,
            slug: data.post_name,
            articlefields: {
                rate: data.meta_fields && isNaN(parseInt(data.meta_fields.rate)) ? "-" : data.meta_fields.rate,
                actAuthor:[{title: data.meta_fields && data.meta_fields.act_author &&
                        data.meta_fields.act_author[0] && data.meta_fields.act_author[0].post_title}],
                view: data.meta_fields && data.meta_fields.view
            }
        }
    },
    getBase64 : function(img, callback) {
        const reader = new FileReader();
        reader.addEventListener('load', () => callback(reader.result));
        reader.readAsDataURL(img);
    },
    setRateByUser : function(PostId) {
        if(typeof PostId === "number"){
            let userRates = getCookie('UserRating');
            if(userRates === "")
                userRates = JSON.stringify([]);
            userRates = JSON.parse(userRates);
            if(userRates.filter(val => val.toString() === PostId.toString()).length < 1){
                userRates.push(PostId);
                setCookie('UserRating', JSON.stringify(userRates), 50);
                return true;
            }
            return false;
        }
    }
};