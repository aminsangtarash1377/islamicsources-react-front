import React, {useEffect, useState} from 'react';
import {
    Header,
    Main,
    Search,
    Footer,
    SearchFilterMenu,
    About,
    ArticlesSubject,
    SingleBook,
    Books,
    Persons,
    SinglePersons,
    Package,
    SidebarCategory,
    BooksSubject,
    Subjects,
    InternalSubject,
    Publishers,
    PublisherInternal,
    Profile,
    Centers,
    ArticlePage,
    SearchPage,
    Journal,
    SingleJournal,
    SingleArticle,
    JournalsCategory,
    PrivateRoute,
    Categories,
    ArticlesCategory,
    BooksCategory,
    InternalMedia,
    MediaPage
} from './Components/Module';
import {Scrollbars} from 'react-custom-scrollbars';
import {Route} from "react-router-dom";
import connect from "react-redux/es/connect/connect";
import {withRouter, useLocation} from "react-router-dom";
import {Authenticated, Rtl, UserDetailPush} from "./actions";
import axios from "axios";
import config, {defaultRahUrl, defaultWPUrl, ISAccessUserToken, loadingItem, newWords, words} from "./config";
import {Icon, Spin} from "antd";


function App(props) {
    const location = useLocation();
    const [Filter, setFilter] = useState(true);
    const [categorySide, setCategoryPosition] = useState(false);
    const [dataThemeOption, setDataThemeOption] = useState(false);

    useEffect(()=> {
        setCategoryPosition(handleDefaultSituation());
    },[location]);

    useEffect(() => {
        axios.get(defaultRahUrl + '/translates')
            .then(async res => {
                if (res.status === 200) {
                    config.words.push(await res.data.data);
                    if (ISAccessUserToken !== "") {
                        await axios.get(`${defaultWPUrl}/users/current`, {
                            headers: {
                                Authorization: ISAccessUserToken,
                                "Content-Type": "application/json"
                            }
                        })
                            .then(async response => {
                                if (response.status === 200) {
                                    await props.dispatch(Authenticated(true));
                                    await props.dispatch(UserDetailPush(response.data.data));
                                } else {
                                    await props.dispatch(Authenticated(false));
                                }
                            })
                            .catch(async error => {
                                await props.dispatch(Authenticated(false));
                            })
                    } else {
                        props.dispatch(Authenticated(false));
                    }
                }
            });

        axios.get(defaultRahUrl + `/themeOptions`)
            .then(res => {
                setDataThemeOption(res.data);
            });

        setTimeout(async function () {
            if (newWords.length > 0) {
                await axios.post(defaultRahUrl + '/translates',
                    {words: newWords},
                    {headers: {Authorization: ISAccessUserToken}})
            }
        }, 5000)
    }, [words]);



    function handleFilterClick() {
        setFilter(!Filter);
    }

    /**
     * @return {boolean}
     */
    function EQLocation(target) {
        return location.pathname === "/" + target;
    }

    function inSomeSituation(firstVariable, SecondVariable) {
        const mediaLG = window.matchMedia("(min-width: 992px)");
        return mediaLG.matches ? (EQLocation("Books")
            ||
            EQLocation("Articles")
            ||
            EQLocation("Journals")
            ||
            EQLocation("Categories"))
            ? firstVariable
            : SecondVariable
            : SecondVariable
    }

    function handleDefaultSituation() {
        const mediaLG = window.matchMedia("(min-width: 992px)");
        return mediaLG.matches ?
            (inSomeSituation(false, true))
            :
            true
    }


    function directionChanger() {
        if (dataThemeOption.rtlMode === "0") {
            config.rtlMode = false;
            props.dispatch(Rtl(false))
        } else {
            config.rtlMode = true;
            props.dispatch(Rtl(true));
        }
    }

    directionChanger();

    return (
        props.authorization === null ?
            <Spin
                spinning
                className="w-100 d-flex align-items-center justify-content-center"
                style={{height: "100vh"}}
                indicator={<img className="w-25 h-auto" src={loadingItem} alt="loading"/>}
            />
            :
            <div
                style={{display: "flex"}}
                className={props.RtlDir ? "dir-rtl" : "dir-ltr"}
            >
                <SidebarCategory
                    location={location.pathname}
                    className={inSomeSituation("position-media-Change", "position-absolute")}
                    sidebarCollapse={inSomeSituation(false, categorySide)}
                    setCategoryPosition={(d)=> setCategoryPosition(d)}
                />
                {
                    location.pathname === "/" ||
                    location.pathname === "/Books/" ||
                    location.pathname === "/Articles/" ||
                    location.pathname === "/Journals/" ||
                    location.pathname === "/Categories/"
                        ?
                        inSomeSituation("",
                            <div
                                onClick={() => setCategoryPosition(!categorySide)}
                                className={
                                    "menu-category-trigger " +
                                    "position-absolute " +
                                    "b-circle " +
                                    "d-flex " +
                                    "align-items-center " +
                                    "justify-content-center " +
                                    "cursor-p " +
                                    (dataThemeOption.rtlMode === "0" ? "ltr-category " : "rtl-category ") +
                                    "menu-opened"
                                }
                            >
                                <Icon type="bars" style={{color: "white"}}/>
                            </div>
                        )
                        : ""
                }
                {
                    location.pathname.split('/')[1] === "Package" &&
                    <div
                        style={
                            {
                                background: props.UrlImage && "url(" + props.UrlImage + ") " +
                                    "no-repeat,linear-gradient(0deg, white 20%, transparent",
                                height: "75%",
                                overflow: "hidden"
                            }
                        }
                        className="w-100 position-absolute img-back-package"
                    />
                }
                <Scrollbars
                    autoHide
                    className={inSomeSituation("App-scroller App-scroller-smaller position-relative",
                        "App-scroller position-relative")}
                    style={{height: "100vh", overflowX: "hidden"}}
                >
                    <div
                        className={inSomeSituation("padding-app-parent position-relative rem",
                            "cat-change-pos position-relative")}
                    >
                        {EQLocation("language") ? "" :
                            <Header data={dataThemeOption} loc={location.pathname}/>
                        }

                        <Search loc={location.pathname} click={(e) => handleFilterClick(e)}/>

                        <Route path="/" exact component={Main}/>
                        {/*<Route path="/language/" component={Language}/>*/}
                        <Route path="/Books/" component={Books}/>
                        <Route path="/Subject/:slug/Articles/" component={ArticlesSubject}/>
                        <Route path="/Subject/:slug/Books/" component={BooksSubject}/>
                        <Route path="/Subject/:slug/Journals/" component={JournalsCategory}/>
                        <Route path="/About/" component={About}/>
                        <Route path="/Persons/" component={Persons}/>
                        <Route path="/Books/:slug/" component={SingleBook}/>
                        <Route path="/Articles/:slug" component={SingleArticle}/>
                        <Route path="/Journals/:slug/" component={SingleJournal}/>
                        <Route path="/Persons/:slug/" component={SinglePersons}/>
                        <Route path="/Package/:slug" component={Package}/>
                        <Route path="/Subjects/" component={Subjects}/>
                        <Route path="/Subjects/:slug/" component={InternalSubject}/>
                        <Route path="/Publishers/" component={Publishers}/>
                        <Route path="/Publishers/:slug/" component={PublisherInternal}/>
                        <PrivateRoute path="/profile/" component={Profile}/>
                        <Route path="/Centers/" component={Centers}/>
                        <Route path="/Articles/" component={ArticlePage}/>
                        <Route path="/SearchPage/" component={SearchPage}/>
                        <Route path="/Journals/" component={Journal}/>
                        <Route path="/Categories/" component={Categories}/>
                        <Route path="/Category/:slug/Articles/" component={ArticlesCategory}/>
                        <Route path="/Category/:slug/Books/" component={BooksCategory}/>
                        <Route path="/internalMedia/" component={InternalMedia}/>
                        <Route path="/mediaPage/" component={MediaPage}/>

                        <SearchFilterMenu
                            onFilter={(e) => handleFilterClick(e)}
                            filter={Filter}
                            top={location.pathname.split('/')[1] === "language" ? "0" : "4.25rem"}
                        />
                    </div>
                    {location.pathname.split('/')[1] === "language" ? "" :
                        <Footer
                            style={{paddingRight: inSomeSituation("27rem", "")}}
                            data={dataThemeOption && dataThemeOption}
                        />
                    }
                </Scrollbars>
            </div>
    );
}

const mapStateToProps = states => (
    {
        RtlDir: states.Rtl,
        UrlImage: states.UrlImagePackage,
        authorization: states.authorization
    }
);
export default withRouter(connect(mapStateToProps)(App));