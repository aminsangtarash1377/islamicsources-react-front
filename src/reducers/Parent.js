

export const Parent =(state = 12 , action)=>{
    if (action.type === "PARENT") {
        return action.data;
    } else {
        return state;
    }
};
