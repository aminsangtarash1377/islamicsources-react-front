import React from "react";
import {NewSelectInput} from '../Components/Module'
import './reducer.css'

let checks = {};

export const CheckTypeSelected = (state = null,action)=>{
    if (action.type === "TYPE") {
        checks[action.name] = action;
        return {
            array : checks,
            value : Object.values(checks).map((d,i) =>
                d.checked &&
                <div
                    key={i}
                    className="InputCheckBoxCheckedClass d-flex align-items-center my-1"
                >
                    <NewSelectInput type={action.type} name={d.name} checked={d.checked}/>
                    <label htmlFor={d.name}>{d.name}</label>
                </div>

            )
        }
    } else {
        return state;
    }
};


let checksFormat = {};

export const CheckFormatSelected = (state = null,action)=>{
    if (action.type === "FORMAT") {
        checksFormat[action.name] = action;
        return {
            array : checksFormat,
            value : Object.values(checksFormat).map((d,i) =>
                d.checked &&
                <div
                    key={i}
                    className="InputCheckBoxCheckedClass d-flex align-items-center my-1"
                >
                    <NewSelectInput type={action.type} name={d.name} checked={d.checked}/>
                    <label htmlFor={d.name}>{d.name}</label>
                </div>

            )
        }
    } else {
        return state;
    }
};

let checksCreator = {};

export const CheckCreatorSelected = (state = null,action)=>{
    if (action.type === "CREATOR") {
        checksCreator[action.name] = action;
        return {
            array : checksCreator,
            value : Object.values(checksCreator).map((d,i) =>
                d.checked &&
                <div
                    key={i}
                    className="InputCheckBoxCheckedClass d-flex align-items-center my-1"
                >
                    <NewSelectInput type={action.type} name={d.name} checked={d.checked}/>
                    <label htmlFor={d.name}>{d.name}</label>
                </div>

            )
        }
    } else {
        return state;
    }
};
