

export const SingleInBooksPageSlug =(state = null , action)=>{
    if (action.type === "SINGLEINBOOKSPAGESLUG") {
        return action.data;
    } else {
        return state;
    }
};