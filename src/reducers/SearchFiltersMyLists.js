import React from "react";
import './reducer.css'
import {Menu} from "antd";

const { SubMenu } = Menu;

let checks = {};

export const SearchFiltersMyLists = (state = null,action)=>{
    if (action.type === "LISTS") {
        checks[action.name] = action;
        console.log();
        return {
            array : checks,
            value : Object.values(checks).map((d,i) =>
                d.checked && Object.keys(action.data).length > 0 &&
                <div key={i} className="myList-box m-3">
                    <span>{d.name}</span>
                    <Menu
                        mode="inline"
                        className="menuMyList"
                    >
                        {Object.keys(d.data).map((data , i) =>
                            Object.keys(data).length > 0 && Object.values(d.data[data]).length > 0 &&
                            <SubMenu
                                key={i}
                                title={
                                    <span>
                                        <span>{data}</span>
                                    </span>
                                }
                            >
                                {Object.values(d.data[data]).map((Q,index)=>
                                    Q.checked &&
                                    <Menu.Item key={index}>
                                        <div
                                            className="InputCheckBoxCheckedClass d-flex align-items-center my-1"
                                        >
                                            <div >{Q.name}</div>
                                        </div>
                                    </Menu.Item>
                                )}
                            </SubMenu>
                        )}
                    </Menu>
                </div>
            )
        }
    } else {
        return state;
    }
};
