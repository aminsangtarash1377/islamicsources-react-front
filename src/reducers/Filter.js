

export const Filter =(state = false , action)=>{
    if (action.type === "FILTER") {
        return action.text;
    } else {
        return state;
    }
};