

export const SingleInBooksPageTitle = (state = null , action)=>{
    if (action.type === "SINGLEINBOOKSPAGETITLE") {
        return action.data;
    } else {
        return state;
    }
};
