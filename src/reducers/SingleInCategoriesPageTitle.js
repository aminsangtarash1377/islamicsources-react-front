

export const SingleInCategoriesPageTitle =(state = null , action)=>{
    if (action.type === "SINGLEINCATEGORIESPAGETITLE") {
        return action.data;
    } else {
        return state;
    }
};