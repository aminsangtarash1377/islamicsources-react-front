

export const SingleInHomePageSlug =(state = null , action)=>{
    if (action.type === "SINGLEINHOMEPAGESLUG") {
        return action.data;
    } else {
        return state;
    }
};