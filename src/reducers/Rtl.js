

export const Rtl =(state = true , action)=>{
    if (action.type === "RTL") {
        return action.data;
    } else {
        return state;
    }
};