

export const SingleInJournalPageTitle =(state = null , action)=>{
    if (action.type === "SINGLEINJOURNALPAGETITLE") {
        return action.data;
    } else {
        return state;
    }
};