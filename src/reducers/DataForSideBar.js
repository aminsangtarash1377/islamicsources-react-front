

export const DataForSideBar =(state = null , action)=>{
    if (action.type === "DATAFORSIDEBAR") {
        return action.data;
    } else {
        return state;
    }
};