

export const SingleInArticlesPageTitle = (state = null , action)=>{
    if (action.type === "SINGLEINARTICLESPAGETITLE") {
        return action.data;
    } else {
        return state;
    }
};