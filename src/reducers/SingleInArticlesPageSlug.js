

export const SingleInArticlesPageSlug =(state = null , action)=>{
    if (action.type === "SINGLEINARTICLESPAGESLUG") {
        return action.data;
    } else {
        return state;
    }
};