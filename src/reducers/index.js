import {combineReducers} from "redux";
import {Filter} from './Filter';
import {Rtl} from './Rtl';
import {SingleInBooksPageTitle} from './SingleInBooksPageTitle';
import {SingleInBooksPageSlug} from "./SingleInBooksPageSlug";
import {SingleInArticlesPageSlug} from "./SingleInArticlesPageSlug";
import {SingleInArticlesPageTitle} from "./SingleInArticlesPageTitle";
import {SingleInJournalPageTitle} from "./SingleInJurnalPageTitle";
import {SingleInJournalPageSlug} from "./SingleInJurnalPageSlug";
import {SingleInHomePageSlug} from "./SingleInHomePageSlug";
import {SingleInHomePageTitle} from "./SingleInHomePageTitle";
import {DataForSideBar} from "./DataForSideBar";
import {SearchFiltersMyLists} from "./SearchFiltersMyLists";
import {LoadingForSidebar} from "./LoadingForSidebar";
import {UrlImagePackage} from "./UrlImagePackage";
import {SingleInCategoriesPageTitle} from "./SingleInCategoriesPageTitle"
import {SingleInCategoriesPageSlug} from "./SingleInCategoriesPageSlug"
import {
    CheckTypeSelected,
    CheckFormatSelected,
    CheckCreatorSelected
} from './checkFilterSelected';
import {Parent} from "./Parent";
import {authorization, userDetails, privateLoading} from "./Authorization";


export default combineReducers({
    Filter,
    Rtl,
    CheckTypeSelected,
    CheckFormatSelected,
    CheckCreatorSelected,
    Parent,
    SingleInBooksPageTitle,
    SingleInBooksPageSlug,
    SingleInArticlesPageSlug,
    SingleInArticlesPageTitle,
    SingleInJournalPageTitle,
    SingleInJournalPageSlug,
    DataForSideBar,
    SingleInHomePageTitle,
    SingleInHomePageSlug,
    SearchFiltersMyLists,
    UrlImagePackage,
    authorization,
    userDetails,
    privateLoading,
    LoadingForSidebar,
    SingleInCategoriesPageTitle,
    SingleInCategoriesPageSlug,
})