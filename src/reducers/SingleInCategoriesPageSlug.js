

export const SingleInCategoriesPageSlug =(state = null , action)=>{
    if (action.type === "SINGLEINCATEGORIESPAGESLUG") {
        return action.data;
    } else {
        return state;
    }
};