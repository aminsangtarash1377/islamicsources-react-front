

export const LoadingForSidebar =(state = false , action)=>{
    if (action.type === "LOADINGFORSIDEBAR") {
        return action.data;
    } else {
        return state;
    }
};