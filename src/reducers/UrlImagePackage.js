

export const UrlImagePackage =(state = null , action)=>{
    if (action.type === "URLIMAGEPACKAGE") {
        return action.data;
    } else {
        return state;
    }
};