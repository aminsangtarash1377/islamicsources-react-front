

export const SingleInHomePageTitle =(state = null , action)=>{
    if (action.type === "SINGLEINHOMEPAGETITLE") {
        return action.data;
    } else {
        return state;
    }
};