export const authorization = (isAuthenticated = null, action) => {
    switch (action.type) {
        case "AUTHENTICAT":(
            isAuthenticated = action.value
        );

        default:
            return isAuthenticated;
    }
};

export const userDetails = (details = [], action) => {
    switch (action.type) {
        case "USERDETAILSPUSH":(
            details = action.value
        );

        default:
            return details;
    }
};

export const privateLoading = (loading = true, action) => {
    switch (action.type) {
        case "STOPLOADING":(
            loading = action.value
        );

        default:
            return loading;
    }
};