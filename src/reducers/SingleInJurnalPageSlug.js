

export const SingleInJournalPageSlug =(state = null , action)=>{
    if (action.type === "SINGLEINJOURNALPAGESLUG") {
        return action.data;
    } else {
        return state;
    }
};